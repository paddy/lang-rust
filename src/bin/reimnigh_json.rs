use serde::Serialize;
use serde_json::{Map, Value};
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::process;
use std::sync::Arc;

use languages::{LanguageData, DictWord, TranslationContext, TranslationSet};

use languages::DialectSet;
use languages::irish::categories::{AgreementInfo, Number, Person, Tense};
use languages::irish::external::IrishWord;
use languages::irish::pronoun::{Pronoun, PronounForm};
use languages::irish::verb::IrishVerb;
use languages::irish::verbalparticle::VerbalParticle;
use languages::irish::np::NP;
use languages::irish::vp::VP;
use languages::irish::Dialect;

const TENSES: &[(&str, Tense)] = &[
    ("past", Tense::Past),
    ("present", Tense::Present),
    ("habitualpresent", Tense::HabitualPresent),
    ("future", Tense::Future),
    ("conditional", Tense::Conditional),
    ("pasthabitual", Tense::HabitualPast),
    ("pastsubjunctive", Tense::PastSubjunctive),
    ("presentsubjunctive", Tense::PresentSubjunctive),
    ("imperative", Tense::Imperative),
];
const PERSONS: &[(&str, Person, Number)] = &[
    ("first_singular", Person::First, Number::Singular),
    ("second_singular", Person::Second, Number::Singular),
    ("third_singular", Person::Third, Number::Singular),
    ("first_plural", Person::First, Number::Plural),
    ("second_plural", Person::Second, Number::Plural),
    ("third_plural", Person::Third, Number::Plural),
    ("relative", Person::Relative, Number::Singular),
    ("autonomous", Person::Autonomous, Number::Singular),
];
const DIALECTS: &[(&str, Dialect, bool)] = &[
    ("caighdean", Dialect::SchoolCO, false),
    ("connacht", Dialect::ConnachtD, true),
    ("ulster", Dialect::UlsterD, true),
    ("munster", Dialect::MunsterD, true),
];
const DIALECTS_VN_VA: &[(&str, Dialect, bool)] = &[
    ("caighdean", Dialect::SchoolCO, false),
    ("connacht", Dialect::ConnachtD, true),
    ("munster", Dialect::MunsterD, true),
    ("ulster", Dialect::UlsterD, true),
];

fn create_se_si() -> Arc<Pronoun> {
    let mut forms: HashMap<String, Vec<PronounForm>> = HashMap::new();
    let form = vec![PronounForm {
        text: "sé/sí".to_owned(),
        dialects: DialectSet::all(),
    }];
    forms.insert("subj".to_owned(), form.clone());
    forms.insert("subj-emph".to_owned(), form);

    Arc::new(Pronoun::new(Person::Third, Number::Singular, None, forms).unwrap())
}

fn get_pronoun(data: &LanguageData, i: AgreementInfo) -> Option<Arc<Pronoun>> {
    let pronoun = if i.number == Number::Singular {
        match i.person {
            Person::First => Some("mé"),
            Person::Second => Some("tú"),
            Person::Third => return Some(create_se_si()),
            Person::Autonomous | Person::Relative => None,
        }
    } else {
        match i.person {
            Person::First => Some("muid"),
            Person::Second => Some("sibh"),
            Person::Third => Some("siad"),
            Person::Autonomous | Person::Relative => None,
        }
    };

    pronoun.map(|pronoun| {
        match &*data.get_word(pronoun).unwrap() {
            DictWord::Ga(IrishWord::IrishPronoun(p)) => p.clone(),
            _ => panic!("{} is not a pronoun", pronoun),
        }
    })
}

fn generate_person(ctx: &TranslationContext, verb: &Arc<IrishVerb>, t: Tense, p: Person, n: Number) -> Value {
    let data = &ctx.ld;
    let i = AgreementInfo { person: p, number: n, ..Default::default() };

    let mut vp = VP::new(verb.clone(), t, false).sentence(false);
    if let Some(pronoun) = get_pronoun(data, i) {
        let np = NP::from_pronoun(pronoun, false);
        vp = vp.subject(np);
    } else {
        vp = vp.agreement(i);
    }

    if i.person == Person::Relative {
        let p = VerbalParticle::from_str(data, "a").unwrap();
        assert!(!p.dependent_form);
        vp = vp.particle(p);
    }
    match t {
        Tense::PresentSubjunctive => {
            vp = vp.particle(VerbalParticle::from_str(data, "go").unwrap());
        },
        Tense::PastSubjunctive => {
            vp = vp.particle(VerbalParticle::from_str(data, "dá").unwrap());
        },
        _ => (),
    };
    let positive = vp.generate(ctx);

    vp = vp.particle(VerbalParticle::from_str(data, "an").unwrap());
    let question = vp.generate(ctx);

    vp = vp.particle(VerbalParticle::from_str(data, "ní").unwrap());
    let negative = vp.generate(ctx);

    let mut result = Map::new();
    for d in DIALECTS {
        //println!("{:?} {} {:?} {:?} {:?}", verb.lemma().get(0), d.0, t, p, n);
        if p == Person::Relative && (t == Tense::Imperative || t == Tense::PresentSubjunctive || t == Tense::PastSubjunctive) {
            result.insert(format!("{}_positive", d.0), Value::String("-".to_owned()));
        } else {
            result.insert(
                format!("{}_positive", d.0),
                Value::String(format!("{}", positive.best(d.1, d.2).unwrap()))
            );
        }

        if p != Person::Relative && t.has_dependent_forms() && t != Tense::PastSubjunctive {
            result.insert(
                format!("{}_question", d.0),
                Value::String(format!("{}", question.best(d.1, d.2).unwrap()))
            );
            result.insert(
                format!("{}_negative", d.0),
                Value::String(format!("{}", negative.best(d.1, d.2).unwrap()))
            );
        } else {
            result.insert(format!("{}_question", d.0), Value::String("".to_owned()));
            result.insert(format!("{}_negative", d.0), Value::String("".to_owned()));
        }
    }
    Value::Object(result)
}

fn generate_tense(ctx: &TranslationContext, verb: &Arc<IrishVerb>, t: Tense) -> Value {
    let mut result = Map::new();
    for p in PERSONS {
        result.insert(p.0.to_owned(), generate_person(ctx, verb, t, p.1, p.2));
    }
    Value::Object(result)
}

fn generate_vn_va(ts: &dyn TranslationSet) -> Value {
    let mut result = Map::new();
    for d in DIALECTS_VN_VA {
        let form = match ts.best(&d.1, d.2) {
            Some(t) => format!("{}", t),
            None => "-".to_owned(),
        };
        result.insert(format!("{}", d.0), Value::String(form));
    }
    Value::Object(result)
}

fn add_verb(ctx: &TranslationContext, words: &mut Vec<Value>, lemma: &str, verb: &Arc<IrishVerb>) {
    let mut result = Map::new();
    result.insert("verb".to_owned(), Value::String(lemma.to_owned()));
    result.insert("verbalnoun".to_owned(), generate_vn_va(&verb.vn()));
    result.insert("verbaladjective".to_owned(), generate_vn_va(&verb.va()));
    for t in TENSES {
        result.insert(t.0.to_owned(), generate_tense(ctx, verb, t.1));
    }
    words.push(Value::Object(result));
}

fn main() {
    let file = File::open("content.json").unwrap();
    let reader = BufReader::new(file);
    let data: LanguageData = serde_json::from_reader(reader).unwrap_or_else(|e| {
        eprintln!("{}", e);
        process::exit(1);
    });
    let ctx = TranslationContext::new(data);

    let mut words = Vec::new();

    for lemma in &["abair", "bain", "beir", "bí", "ceannaigh", "cluin", "cuir", "déan", "deighil", "fág", "faigh", "fan", "feic", "foghlaim", "freagair", "ith", "imir", "inis", "léigh", "maraigh", "ól", "oscail", "rith", "sábháil", "tabhair", "tar", "téigh", "tóg", "tosaigh"] {
        let word = ctx.ld.get_word(&lemma).unwrap_or_else(|e| {
            eprintln!("{}", e);
            process::exit(1);
        });

        match &*word {
            DictWord::Ga(IrishWord::IrishVerb(verb)) => add_verb(&ctx, &mut words, lemma, verb),
            _ => ()
        }
    }

    let json = Value::Array(words);

    let formatter = serde_json::ser::PrettyFormatter::with_indent(b"    ");
    let buf = Vec::new();
    let mut ser = serde_json::Serializer::with_formatter(buf, formatter);
    json.serialize(&mut ser).unwrap();
    println!("{}", String::from_utf8(ser.into_inner()).unwrap())
}
