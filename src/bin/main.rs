use std::fs::File;
use std::io::BufReader;
use std::sync::Arc;

use languages::{self, LanguageData, TranslationContext};
use learning::{Course, CourseConfig, ExerciseStatus};

fn main() {
    let file = File::open("content.json").unwrap();
    let reader = BufReader::new(file);
    let data: Result<LanguageData, _> = serde_json::from_reader(reader);
    let data = match data {
        Ok(data) => data,
        Err(e) => {
            println!("{}", e);
            return;
        },
    };
    let ctx = Arc::new(TranslationContext::new(data.clone()));

    /*
    for (lemma, word) in data.words.iter() {
        println!("--- {} ---", lemma);
        match &**word {
            DictWord::De(GermanWord::GermanNoun(noun)) => noun.print(),
            DictWord::De(GermanWord::GermanPronoun(pronoun)) => pronoun.base.print(),
            DictWord::De(GermanWord::GermanVerb(verb)) => verb.print(),
            DictWord::Ga(IrishWord::IrishVerb(_verb)) => (),
            _ => ()
        }
    }
    */

    println!("\n\n=== Sentence ===");
    /*
    let source_json: Value = serde_json::from_str(r#"
        {
            "concept": "haben",
            "subject": {
                "concept": "Hund",
                "definite": "DEFINITE"
            },
            "object": {
                "concept": "Ball",
                "number": "PLURAL",
                "definite": "DEFINITE"
            }
        }
    "#).unwrap();
    */
    /*
    let source_json: Value = serde_json::from_str(r#"
        {
            "concept": "is",
            "subject": {
                "concept": "Hund",
                "definite": "DEFINITE"
            },
            "predicate": {
                "concept": "Ball"
            }
        }
    "#).unwrap();

    let json = data.apply_concepts_lang::<German>(source_json.clone()).unwrap();
    for json in json.iter() {
        println!("\n{}", &json);

        let json_data = match serde_json::from_str::<ConstituentJson>(&json.to_string()) {
            Ok(data) => data,
            Err(e) => {
                println!("{}", e);
                return;
            }
        };

        let sentence = GermanVP::from_json(&data, &json_data).unwrap();
        for t in sentence.generate() {
            println!("- {}", t);
        }
    }

    let json = data.apply_concepts_lang::<Irish>(source_json).unwrap();
    for json in json.iter() {
        println!("\n{}", &json);

        let json_data = match serde_json::from_str(&json.to_string()) {
            Ok(x) => x,
            Err(e) => {
                println!("{}", e);
                return;
            }
        };

        let sentence = IrishCopP::into(&json_data, &data).unwrap();
        for t in sentence.generate(&ctx) {
            if t.dialects.contains_all() {
                println!("- {}", t);
            } else {
                println!("- {}  [{}]", t, t.dialects.to_string());
            }
            for hint in t.dialect_hints() {
                println!("  # {}", hint);
            }
        }
    }
    */

    println!("\n\n=== Concept ===");
    /*let concept = data.get_concept("Hund").unwrap();
    for t in concept.lemma(&data).unwrap() {
        println!("- {}", t);
    }

    println!();

    let concept = data.get_concept("haben").unwrap();
    for t in concept.lemma(&data).unwrap() {
        println!("- {}", t);
    }

    println!();*/

    for concept in ctx.ld.concepts() {
        if !concept.in_dictionary() {
            continue;
        }
        for t in concept.lemma(&ctx).unwrap() {
            println!("- {}", t);
        }
        println!();
    }

    println!("\n\n=== Phrase ===");
    for phrase in ctx.ld.phrases() {
        for (i, v) in phrase.variants().iter().enumerate() {
            for t in v.translations(&ctx).unwrap() {
                println!("- [{}/{:?}] {}", i, t.language(), t);
                for hint in t.dialect_hints() {
                    println!("  # {}", hint);
                }
                let words = t.words();
                if !words.is_empty() {
                    print!("  @ ");
                    for word in words {
                        print!("{} ", word);
                    }
                    println!();
                }
                let tags = t.tags();
                if !tags.is_empty() {
                    print!("  > ");
                    for tag in tags {
                        print!("{} ", tag);
                    }
                    println!();
                }
            }
        }
        println!();
    }

    println!("=== Lessons ===");
    for l in ctx.ld.lessons() {
        println!("* [{:?} -> {:?}] {}", l.source_lang, l.target_lang, l.name);
    }

    println!("\n=== Courses ===");
    for c in ctx.ld.courses() {
        println!("* [{:?} -> {:?}] {}", c.source_lang, c.target_lang, c.name);
        for s in &c.skills {
            println!("  - {}/{} [{}]", s.section, s.name, s.priority);
        }
    }


    let ld_course = ctx.ld.courses().next().unwrap();
    println!("\n=== {} ===", ld_course.name);
    let mut course = Course::new(&ctx, CourseConfig {
        course: ld_course.clone(),
        source_dialect: Box::new(languages::german::Dialect::DeSued),
        target_dialect: Box::new(languages::irish::Dialect::UlsterCO),
    }).unwrap();

    course.progress.practised_word(ctx.ld.word_by_id("bean").unwrap(), 100, 1.0);
    course.progress.practised_word(ctx.ld.word_by_id("beag").unwrap(), 50, 1.0);
    course.progress.practised_word(ctx.ld.word_by_id("mór").unwrap(), 20, 1.0);
    course.progress.practised_tag("noun-def-nom-sg-f", 10, 1.0);
    course.progress.practised_tag("art-nom-sg-f", 20, 1.0);
    course.progress.practised_tag("adj-nom-sg-f", 100, 1.0);

    println!("Known sentences:");
    let mut first_ex = None;
    for ex in course.available() {
        if first_ex.is_none() && ex.status != ExerciseStatus::Unknown {
            first_ex = Some(ex.clone());
        }
        match ex.status {
            ExerciseStatus::Known => println!("- {} [{}]", ex.variant.get_question().question, ex.priority),
            ExerciseStatus::NewlyAdded => println!("+ {} [{}]", ex.variant.get_question().question, ex.priority),
            ExerciseStatus::Unknown => (),
        }
    }
    let first_ex = first_ex.unwrap();

    println!("\nKnown sentences after adding the following skill:");
    for s in &ld_course.skills {
        let mut total = 0;
        let mut current = 0;
        let mut new = 0;

        let avail = course.available_with_skill(&ctx, s);
        for ex in &avail {
            total += 1;
            match ex.status {
                ExerciseStatus::Known => {
                    current += 1;
                    new += 1;
                }
                ExerciseStatus::NewlyAdded => {
                    new += 1;
                }
                ExerciseStatus::Unknown => (),
            }
        }

        println!("- {}/{} [cur {} new {} total {}]", s.section, s.name, current, new, total);
        for ex in &avail {
            if ex.status == ExerciseStatus::NewlyAdded {
                println!("  + {} [{}]", ex.variant.get_question().question, ex.priority);
            }
        }
    }

    println!("\nChecking answer:");
    let res = first_ex.variant.check_answer(&mut course, "an bhean");
    println!("* an bhean => correct={} best={:?} hints={:?}", res.correct, res.best_correction, res.hints);

    let res = first_ex.variant.check_answer(&mut course, "an bean");
    println!("* an bean => correct={} best={:?} hints={:?}", res.correct, res.best_correction, res.hints);

    println!("\nNew scores:");
    for ex in course.available() {
        match ex.status {
            ExerciseStatus::Known => println!("- {} [{}]", ex.variant.get_question().question, ex.priority),
            ExerciseStatus::NewlyAdded => println!("+ {} [{}]", ex.variant.get_question().question, ex.priority),
            ExerciseStatus::Unknown => (),
        }
    }

    /*
    println!("\n\n### bí ###");
    match &*data.get_word("bí").unwrap() {
        DictWord::Ga(IrishWord::IrishVerb(verb)) => {
            let ts = verb.inflect(
                languages::irish::categories::Tense::Present,
                languages::irish::categories::AgreementInfo {
                    person: languages::irish::categories::Person::Third,
                    number: languages::irish::categories::Number::Plural,
                    gender: languages::irish::categories::Gender::Masc,
                    analytic_verb: false,
                    dat_mutation: languages::irish::Mutation::None,
                },
                &None
            );
            println!("* {}", ts.best(languages::irish::Dialect::UlsterD, true).unwrap());
            for t in ts {
                println!("- {} {:?} (* {:?})", t, t.dialects, t.preferred_in_dialects);
            }
        }
        _ => {
            println!("Not an Irish word: ");
            return;
      }
    };
    */
}
