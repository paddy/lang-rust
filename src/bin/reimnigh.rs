use std::fs::File;
use std::io::BufReader;
use std::process;
use std::sync::Arc;

use languages::{LanguageData, DictWord, Word, TranslationContext, TranslationSet};

use languages::irish::categories::{AgreementInfo, Number, Person, Tense};
use languages::irish::external::IrishWord;
use languages::irish::pronoun::Pronoun;
use languages::irish::verb::IrishVerb;
use languages::irish::verbalparticle::VerbalParticle;
use languages::irish::np::NP;
use languages::irish::vp::VP;
use languages::irish::Dialect;

fn get_pronoun(data: &LanguageData, i: AgreementInfo) -> Option<Arc<Pronoun>> {
    let pronoun = if i.number == Number::Singular {
        match i.person {
            Person::First => Some("mé"),
            Person::Second => Some("tú"),
            Person::Third => Some("sé"),
            Person::Autonomous | Person::Relative => None,
        }
    } else {
        match i.person {
            Person::First => Some("muid"),
            Person::Second => Some("sibh"),
            Person::Third => Some("siad"),
            Person::Autonomous | Person::Relative => None,
        }
    };

    pronoun.map(|pronoun| {
        match &*data.get_word(pronoun).unwrap() {
            DictWord::Ga(IrishWord::IrishPronoun(p)) => p.clone(),
            _ => panic!("{} is not a pronoun", pronoun),
        }
    })
}

fn get_particle(data: &LanguageData, lemma: &str) -> Arc<VerbalParticle> {
    match &*data.get_word(lemma).unwrap() {
        DictWord::Ga(IrishWord::IrishVerbalParticle(p)) => p.clone(),
        _ => panic!("{} is not a verbal particle", lemma),
    }
}

const DIALECTS: &[(Dialect, bool)] = &[
    (Dialect::SchoolCO, false),
    (Dialect::MunsterD, true),
    (Dialect::ConnachtD, true),
    (Dialect::UlsterD, true),
];

fn print_irish_verb_row(ts: &dyn TranslationSet) {
    for d in DIALECTS {
        match ts.best(&d.0, d.1) {
            Some(t) => print!("{:20} ", t),
            None => print!("{:20} ", "-"),
        }
    }
    println!("");
}

fn print_irish_verb_form(
    ctx: &TranslationContext,
    verb: &Arc<IrishVerb>,
    mut id: String,
    tense: Tense,
    i: AgreementInfo
) {
    let particles: &[_] = match tense {
        Tense::Imperative => &[None],
        _ => if i.person == Person::Relative { &[None] } else { &[None, Some("ní"), Some("an")] },
    };

    for maybe_p in particles {
        let mut vp = VP::new(verb.clone(), tense, false).sentence(false);
        if let Some(p) = maybe_p {
            vp = vp.particle(get_particle(&ctx.ld, p));
        };

        if let Some(pronoun) = get_pronoun(&ctx.ld, i) {
            let np = NP::from_pronoun(pronoun, false);
            vp = vp.subject(np);
        } else {
            vp = vp.agreement(i);
        }

        print!("{:20} ", id);
        id.clear();

        print_irish_verb_row(&vp.generate(ctx));
    }

    if particles.len() > 1 {
        println!("");
    }
}

fn print_irish_verb_inflection(ctx: &TranslationContext, verb: &Arc<IrishVerb>) {
    println!("=== {} ===", verb.lemma().join("/"));
    println!("{:20} {:20} {:20} {:20} {:20}", "", "Standard", "Munster", "Connacht", "Ulster");
    for tense in Tense::values() {
        for number in Number::values() {
            for person in Person::values(true) {
                let id = format!("{}{} {}:", person.id(), number.id(), tense.id());
                let i = AgreementInfo { person, number, ..Default::default() };
                print_irish_verb_form(&ctx, verb, id, tense, i);
            }
        }
        for person in Person::values(false) {
            if person == Person::Relative && tense == Tense::Imperative {
                continue;
            }
            let id = format!("{} {}:", person.id(), tense.id());
            let i = AgreementInfo { person, number: Number::Singular, ..Default::default() };
            print_irish_verb_form(&ctx, verb, id, tense, i);
        }
        println!("");
        println!("");
    }

    print!("{:20} ", "verbal noun");
    print_irish_verb_row(&verb.vn());
    print!("{:20} ", "verbal adjective");
    print_irish_verb_row(&verb.va());
}

fn main() {
    let file = File::open("content.json").unwrap();
    let reader = BufReader::new(file);
    let data: LanguageData = serde_json::from_reader(reader).unwrap_or_else(|e| {
        eprintln!("{}", e);
        process::exit(1);
    });
    let ctx = TranslationContext::new(data);

    let mut args = std::env::args();
    args.next();
    for lemma in args {
        let word = ctx.ld.get_word(&lemma).unwrap_or_else(|e| {
            eprintln!("{}", e);
            process::exit(1);
        });

        match &*word {
            DictWord::Ga(IrishWord::IrishVerb(verb)) => print_irish_verb_inflection(&ctx, verb),
            _ => ()
        }
    }
}
