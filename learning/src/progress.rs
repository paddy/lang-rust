use crate::ArcPtrHash;
use languages::DictWord;

use std::cmp::{max, min};
use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use std::time::SystemTime;

pub enum Status {
    New,
    Known,
}

pub struct Item {
    pub date_added: SystemTime,
    pub score: u32,
    pub status: Status,
}

impl Default for Item {
    fn default() -> Self {
        Self {
            date_added: SystemTime::now(),
            score: 0,
            status: Status::New,
        }
    }
}

impl Item {
    fn target_score(&self) -> f32 {
        let since_added = self.date_added.elapsed().unwrap_or_default();
        let days = 1.0 + (since_added.as_secs() as f32 / (3600.0 * 24.0));
        (20.0 * days) + ((50.0 * days) / (1.0 + days))
    }

    fn max_score(&self) -> u32 {
        (1.2 * self.target_score()) as u32
    }

    pub fn priority(&self) -> f32 {
        2.0 - (self.score as f32 / self.target_score())
    }

    pub fn practised(&mut self, score: i32, factor: f32) {
        let new_score = (self.score as f32 * factor) as i64 + i64::from(score);
        let new_score_u32 = u32::try_from(max(0, new_score)).unwrap_or(u32::MAX);
        self.score = min(new_score_u32, self.max_score());
        if score > 0 {
            self.status = Status::Known;
        }
    }
}

pub struct Progress {
    pub words: HashMap<ArcPtrHash<DictWord>, Item>,
    pub tags: HashMap<String, Item>,
}

impl Progress {
    pub fn new() -> Self {
        Self {
            words: HashMap::new(),
            tags: HashMap::new(),
        }
    }

    pub fn practised_word(&mut self, word: Arc<DictWord>, score: i32, factor: f32) {
        self.words.entry(ArcPtrHash(word)).or_default().practised(score, factor)
    }

    pub fn practised_tag(&mut self, tag: &str, score: i32, factor: f32) {
        let entry = match self.tags.get_mut(tag) {
            Some(entry) => entry,
            None => self.tags.entry(tag.to_owned()).or_default(),
        };
        entry.practised(score, factor)
    }

    pub fn update_score(
        &mut self,
        correct_words: Vec<Arc<DictWord>>,
        wrong_words: Vec<Arc<DictWord>>,
        correct_tags: &HashSet<String>,
        wrong_tags: &HashSet<String>,
    ) {
        for word in correct_words {
            self.practised_word(word, 10, 1.0);
        }
        for word in wrong_words {
            self.practised_word(word, -10, 0.8);
        }

        for tag in correct_tags {
            self.practised_tag(tag, 10, 1.0);
        }
        for tag in wrong_tags {
            self.practised_tag(tag, -10, 0.8);
        }
    }
}
