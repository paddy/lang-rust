use anyhow::{anyhow, Result};
use crate::{ArcPtrHash, Course, CourseConfig};
use difflib::sequencematcher::SequenceMatcher;
use languages::{AnyDialect, DictWord, PhraseVariant, Translation, TranslationContext};
use std::collections::HashSet;
use std::sync::Arc;

pub struct ExerciseQuestion {
    pub instruction: Option<String>,
    pub question: Arc<dyn Translation>,
    pub audio: Option<String>,
}

pub struct ExerciseResult {
    /// The answer given by the user, analysed by comparison with generated answers
    pub given: Arc<dyn Translation>,
    pub correct: bool,
    pub closest_correction: Option<Arc<dyn Translation>>,
    pub best_correction: Option<Arc<dyn Translation>>,
    pub hints: Vec<String>,
}

pub trait Exercise {
    fn variants(&self) -> Box<dyn Iterator<Item = Arc<dyn ExerciseVariant>> + '_>;
}

pub trait ExerciseVariant {
    fn get_question(&self) -> ExerciseQuestion;
    fn check_answer(&self, course: &mut Course, answer: &str) -> ExerciseResult;
    fn tags(&self) -> Box<dyn Iterator<Item = &String> + '_>;
    fn words(&self) -> Box<dyn Iterator<Item = &ArcPtrHash<DictWord>> + '_>;
}

/// Removes punctuation and converts to lower case
fn normalise(s: &str) -> String {
    let mut res = String::with_capacity(s.len());
    for c in s.chars() {
        if !c.is_ascii_punctuation() {
            res.extend(c.to_lowercase());
        }
    }
    res
}

/// Checks if both strings are the same when ignoring case and punctuation
fn check_answer_str(correct: &str, given: &str) -> bool {
    let correct = normalise(correct);
    let given = normalise(given);

    correct == given
}

fn check_translation_answer(
    a_trans: &Vec<Arc<dyn Translation>>,
    a_dialect: &dyn AnyDialect,
    dialect_hints: bool,
    answer: &str,
) -> ExerciseResult {
    let mut closest_correction_score = -1.0;
    let mut closest_correction = None;
    let mut best_correction_score = -1.0;
    let mut best_correction = None;
    let mut correct = false;
    let mut given = None;
    let mut hints = Vec::new();

    for t in a_trans {
        let text = t.text();
        println!("{} != {}", text, answer);
        if check_answer_str(&text, answer) {
            hints = if dialect_hints && !t.has_dialect(a_dialect) {
                t.dialect_hints()
            } else {
                Vec::new()
            };

            // TODO Update scores (or return information for the update)
            correct = true;
            given = Some(t.clone());
        }

        let mut s = SequenceMatcher::new::<str>(&text, answer);
        let score = s.ratio();

        if score > closest_correction_score {
            closest_correction = Some(t.clone());
            closest_correction_score = score;
        }

        if dialect_hints {
            let dialect_score = score + 1.0;
            if t.has_dialect(a_dialect) && dialect_score > best_correction_score {
                best_correction = Some(t.clone());
                best_correction_score = dialect_score;
            }
        }
        // TODO Rate uniqueness like Translation.best_translation()
    }

    if best_correction.is_none() {
        best_correction = closest_correction.clone();
    }
    if check_answer_str(&best_correction.as_ref().unwrap().text(), answer) {
        best_correction = None;
    }

    // TODO Port TranslationComparison for `given`
    // TODO Update scores (or return information for the update)
    ExerciseResult {
        given: given.unwrap_or_else(|| a_dialect.str_translation(answer).into()),
        correct,
        best_correction,
        closest_correction,
        hints,
    }
}

struct PhraseTranslationVariant {
    question: Arc<dyn Translation>,
    answers: Arc<Vec<Arc<dyn Translation>>>,
    answer_is_target: bool,
    tags: HashSet<String>,
    words: HashSet<ArcPtrHash<DictWord>>,
}

impl ExerciseVariant for PhraseTranslationVariant {
    fn get_question(&self) -> ExerciseQuestion {
        ExerciseQuestion {
            instruction: None,
            question: self.question.clone(),
            audio: None,
        }
    }

    fn tags(&self) -> Box<dyn Iterator<Item = &String> + '_> {
        Box::new(self.tags.iter())
    }

    fn words(&self) -> Box<dyn Iterator<Item = &ArcPtrHash<DictWord>> + '_> {
        Box::new(self.words.iter())
    }

    fn check_answer(&self, course: &mut Course, answer: &str) -> ExerciseResult {
        let res = check_translation_answer(
            &*self.answers,
            &*course.config.source_dialect,
            self.answer_is_target,
            answer
        );

        // FIXME Updates for incorrect answers
        if res.correct {
            let res_given_tags;
            let (words, tags) = if self.answer_is_target {
                let words = res
                    .given
                    .words()
                    .iter()
                    .map(|w| course.ctx.ld.word_by_id(w).unwrap())
                    .collect();
                res_given_tags = res.given.tags();
                (words, &res_given_tags)
            } else {
                (self.words.iter().map(|p| (**p).clone()).collect(), &self.tags)
            };

            course.progress.update_score(
                words,
                Vec::new(),
                tags,
                &HashSet::new(),
            );
        }
        res
    }

}

pub struct PhraseTranslationExercise {
    variants: Vec<Arc<PhraseTranslationVariant>>,
    answer_is_target: bool,
}

impl PhraseTranslationExercise {
    pub fn new_from_target(
        ctx: &TranslationContext,
        phrase: &PhraseVariant,
        config: &CourseConfig,
    ) -> Result<Self> {
        let mut variants = Vec::new();
        let mut a_trans = Vec::new();
        let answer_is_target = false;

        for t in phrase.translations_lang(&ctx, config.course.source_lang)? {
            a_trans.push(t.into());
        }
        let a_trans = Arc::new(a_trans);

        for t in phrase.translations_lang(&ctx, config.course.target_lang)? {
            // TODO If the question is ambiguous, store alternatively allowed answers language per
            // question variant. If there are unambiguous translations, don't include any ambiguous
            // ones.
            let words = t.words().iter().map(|w| ArcPtrHash(ctx.ld.word_by_id(w).unwrap())).collect();
            variants.push(Arc::new(PhraseTranslationVariant {
                answers: a_trans.clone(),
                answer_is_target,
                tags: t.tags(),
                words,
                question: t.into(),
            }));
        }

        Ok(Self {
            variants,
            answer_is_target,
        })
    }

    pub fn new_to_target(
        ctx: &TranslationContext,
        phrase: &PhraseVariant,
        config: &CourseConfig,
    ) -> Result<Self> {
        let mut a_trans: Vec<Arc<dyn Translation>> = Vec::new();
        let mut variants = Vec::new();
        let answer_is_target = true;

        let q_trans = phrase
            .best_translation(&ctx, &*config.source_dialect, false)
            .ok_or_else(|| anyhow!("Phrase doesn't have a valid source language variant"))?;

        for t in phrase.translations_lang(&ctx, config.course.target_lang)? {
            a_trans.push(t.into());
        }
        let a_trans = Arc::new(a_trans);

        for t in &*a_trans {
            let words = t.words().iter().map(|w| ArcPtrHash(ctx.ld.word_by_id(w).unwrap())).collect();
            variants.push(Arc::new(PhraseTranslationVariant {
                question: q_trans.clone(),
                answers: a_trans.clone(),
                answer_is_target,
                tags: t.tags(),
                words,
            }));
        }

        Ok(Self {
            variants,
            answer_is_target,
        })
    }
}

impl Exercise for PhraseTranslationExercise {
    fn variants(&self) -> Box<dyn Iterator<Item = Arc<dyn ExerciseVariant>> + '_> {
        Box::new(self.variants.iter().map(|x| x.clone() as _))
    }
}
