use anyhow::Result;
use crate::{ArcPtrHash, Exercise, ExerciseVariant, Progress};
use crate::exercise::PhraseTranslationExercise;
use languages::{AnyDialect, DictWord, Skill, SkillType, TranslationContext};
use std::collections::HashSet;
use std::sync::Arc;

pub struct CourseConfig {
    pub course: Arc<languages::Course>,
    pub source_dialect: Box<dyn AnyDialect>,
    pub target_dialect: Box<dyn AnyDialect>,
}

#[derive(Clone, PartialEq)]
pub enum ExerciseStatus {
    Unknown,
    NewlyAdded,
    Known,
}

#[derive(Clone)]
pub struct Available {
    pub exercise: Arc<dyn Exercise>,
    pub variant: Arc<dyn ExerciseVariant>,
    pub status: ExerciseStatus,
    pub priority: f32,
}

pub struct Course {
    pub ctx: Arc<TranslationContext>,
    pub config: CourseConfig,
    pub progress: Progress,
    pub tags: HashSet<String>,
    exercises: Vec<Arc<dyn Exercise>>,
}

impl Course {
    pub fn new(ctx: &Arc<TranslationContext>, config: CourseConfig) -> Result<Self> {
        // TODO Validate config
        let mut exercises = Vec::new();
        let mut tags = HashSet::<String>::new();

        for phrase in ctx.ld.phrases() {
            if !phrase.has_language(config.course.target_lang) {
                continue;
            }

            // v is one variant of a phrase using the same structure, but different words
            // (different values for variables/parameters)
            for v in phrase.variants() {
                let ex = PhraseTranslationExercise::new_to_target(ctx, &v, &config)?;
                exercises.push(Arc::new(ex) as _);

                let ex = PhraseTranslationExercise::new_from_target(ctx, &v, &config)?;
                // ex_v is one way to say the same thing (dialectal differences etc.)
                for ex_v in ex.variants() {
                    tags.extend(ex_v.tags().map(|x| x.to_owned()));
                }
                exercises.push(Arc::new(ex) as _);
            }
        }

        Ok(Self {
            ctx: ctx.clone(),
            config,
            exercises,
            tags,
            progress: Progress::new(),
        })
    }

    fn available_with_new(
        &self,
        new_words: HashSet<ArcPtrHash<DictWord>>,
        new_tags: HashSet<String>,
        grammar_skill: Option<bool>,
    ) -> Vec<Available> {
        let mut res = Vec::new();

        let known_words: HashSet<_> = self.progress.words.keys().cloned().collect();
        let known_words_with_new = &known_words | &new_words;

        let known_tags: HashSet<_> = self.progress.tags.keys().cloned().collect();
        let known_tags_with_new = &known_tags | &new_tags;

        for ex in &self.exercises {
            let mut ex_res: Option<Available> = None;

            for v in ex.variants() {
                // TODO Filter by CourseConfig (right dialect)
                let mut priority = 0.0;

                // Tags that aren't taught in any skill are always considered known
                let tags: HashSet<_> = v.tags().filter(|x| self.tags.contains(*x)).cloned().collect();
                let words: HashSet<_> = v.words().cloned().collect();

                // Consider only phrases that contain something relevant for the skill.
                // Select only phrases that use at least one word or one grammar tag
                // from the skill. However, if it's a grammar skill, practising a
                // random word used in an example doesn't really contribute to the
                // skill, so consider only grammar tags there.
                let used_new_words = &words & &new_words;
                let used_new_tags = &tags & &new_tags;

                match grammar_skill {
                    Some(true) => {
                        if used_new_tags.is_empty() {
                            continue;
                        }
                    }
                    Some(false) => {
                        if used_new_tags.is_empty() && used_new_words.is_empty() {
                            continue;
                        }
                    }
                    _ => ()
                }

                priority += used_new_tags.len() as f32;
                priority += used_new_words.len() as f32 / words.len() as f32;

                let mut max_word_priority = 0.0;
                for w in &words {
                    if let Some(item) = self.progress.words.get(w) {
                        max_word_priority = f32::max(max_word_priority, item.priority());
                    }
                }
                priority += max_word_priority;

                let status = if tags.is_subset(&known_tags) && words.is_subset(&known_words) {
                    ExerciseStatus::Known
                } else if tags.is_subset(&known_tags_with_new) && words.is_subset(&known_words_with_new) {
                    ExerciseStatus::NewlyAdded
                } else {
                    priority = 0.0;
                    ExerciseStatus::Unknown
                };

                let v_res = Available {
                    exercise: ex.clone(),
                    variant: v.clone(),
                    status,
                    priority,
                };

                if let Some(old_ex_res) = &ex_res {
                    if old_ex_res.status == ExerciseStatus::Unknown || v_res.priority > old_ex_res.priority {
                        ex_res = Some(v_res);
                    }
                } else {
                    ex_res = Some(v_res);
                }
            }

            if let Some(ex_res) = ex_res {
                res.push(ex_res);
            }
        }
        res
    }

    pub fn available_with_skill(&self, ctx: &TranslationContext, s: &Skill) -> Vec<Available> {
        let words = s.words.iter().map(|w| ArcPtrHash(ctx.ld.word_by_id(&w.word).unwrap()));

        self.available_with_new(
            HashSet::from_iter(words),
            HashSet::from_iter(s.tags.iter().cloned()),
            Some(s.r#type == SkillType::Grammar)
        )
    }

    pub fn available(&self) -> Vec<Available> {
        self.available_with_new(HashSet::new(), HashSet::new(), None)
    }
}
