use std::hash::{Hash, Hasher};
use std::ops::Deref;
use std::sync::Arc;

mod course;
mod exercise;
mod progress;

pub use course::{Course, CourseConfig, ExerciseStatus};
pub use exercise::{Exercise, ExerciseVariant};
pub use progress::Progress;

pub struct ArcPtrHash<T>(Arc<T>);

impl<T> Deref for ArcPtrHash<T> {
    type Target = Arc<T>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> Clone for ArcPtrHash<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T> Hash for ArcPtrHash<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let inner: &T = &*self.0;
        (inner as *const T).hash(state);
    }
}

impl<T> PartialEq for ArcPtrHash<T> {
    fn eq(&self, other: &Self) -> bool {
        let inner: &T = &*self.0;
        let other_inner: &T = &*other.0;
        (inner as *const T) == (other_inner as *const T)
    }
}

impl<T> Eq for ArcPtrHash<T> {}
