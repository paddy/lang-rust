#!/usr/bin/env python3

import json
import sys

if len(sys.argv) == 2:
    filename = sys.argv[1]
else:
    filename = 'content.json'

with open(filename) as f:
    data = f.read()

json_dict = json.loads(data)

words = []
for w in json_dict['words']:
    if w['type'] in ('IrishVerb', 'IrishVerbalParticle', 'IrishPronoun'):
        words.append(w)

json_dict = {
    'words': words,
    'concepts': [],
    'phrases': [],
    'lessons': [],
    'courses': [],
}

if True:
    print(json.dumps(json_dict, separators=(',',':'), ensure_ascii=False))
else:
    print(json.dumps(json_dict, indent=4, ensure_ascii=False))
