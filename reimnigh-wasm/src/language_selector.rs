use crate::LANGUAGE_LOADER;
use i18n_embed::LanguageLoader;
use yew::prelude::*;

static LANGUAGES: &[(&str, &str)] = &[
    ("ga", "Gaeilge"),
    ("en", "English"),
    ("de", "Deutsch"),
];

pub struct LanguageSelector {
    props: LanguageSelectorProperties,
}

#[derive(Properties, Clone)]
pub struct LanguageSelectorProperties {
    pub switch_lang: Callback<String>,
}

impl Component for LanguageSelector {
    type Message = ();
    type Properties = LanguageSelectorProperties;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        let current = LANGUAGE_LOADER.current_language();
        let current_id = current.language.as_str();

        let languages: Vec<_> = LANGUAGES.iter().map(|l| {
            if current_id == l.0 {
                html! {
                    <li><strong>{&l.1}</strong></li>
                }
            } else {
                let cb = self.props.switch_lang.reform(|_| l.0.to_string());
                html! {
                    <li><a href="#" onclick={cb}>{&l.1}</a></li>
                }
            }
        }).collect();

        html! {
            <nav class="language_selector">
                <ul>
                    {languages}
                </ul>
            </nav>
        }
    }
}
