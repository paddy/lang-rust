use crate::{fl, State};
use crate::words::Words;
use languages::{DictWord, DialectSet, LanguageData, TranslationContext, TranslationSet};
use languages::irish::{
    Dialect,
    categories::{AgreementInfo, Number, Person, Tense},
    external::IrishWord,
    pronoun::{Pronoun, PronounForm},
    verb::IrishVerb,
    verbalparticle::VerbalParticle,
    np::NP,
    vp::VP,
};
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, Mutex};
use yew::prelude::*;
use yew::utils::document;

const DIALECTS: &[(&str, Dialect, bool)] = &[
    ("", Dialect::SchoolCO, false),
    ("munster", Dialect::MunsterD, true),
    ("connacht", Dialect::ConnachtD, true),
    ("ulster", Dialect::UlsterD, true),
];

fn dialect_to_str(d: Dialect) -> String {
    match d {
        Dialect::SchoolCO => fl!{"dialect_caighdean"},
        Dialect::MunsterD => fl!{"dialect_munster"},
        Dialect::ConnachtD => fl!{"dialect_connacht"},
        Dialect::UlsterD => fl!{"dialect_ulster"},
        _ => String::new(),
    }
}

fn tense_to_str(t: Tense) -> String {
    match t {
        Tense::Present => fl!{"tense_pres"},
        Tense::HabitualPresent => fl!{"tense_hab_pres"},
        Tense::Past => fl!{"tense_past"},
        Tense::Future => fl!{"tense_fut"},
        Tense::Conditional => fl!{"tense_cond"},
        Tense::HabitualPast => fl!{"tense_hab_past"},
        Tense::Imperative => fl!{"tense_imp"},
        Tense::PresentSubjunctive => fl!{"tense_pres_subj"},
        Tense::PastSubjunctive => fl!{"tense_past_subj"},
    }
}

fn create_se_si() -> Arc<Pronoun> {
    let mut forms: HashMap<String, Vec<PronounForm>> = HashMap::new();
    let form = vec![PronounForm {
        text: "sé/sí".to_owned(),
        dialects: DialectSet::all(),
    }];
    forms.insert("subj".to_owned(), form.clone());
    forms.insert("subj-emph".to_owned(), form);

    Arc::new(Pronoun::new(Person::Third, Number::Singular, None, forms).unwrap())
}

fn get_pronoun(data: &LanguageData, i: AgreementInfo) -> Option<Arc<Pronoun>> {
    let pronoun = if i.number == Number::Singular {
        match i.person {
            Person::First => Some("mé"),
            Person::Second => Some("tú"),
            Person::Third => return Some(create_se_si()),
            Person::Autonomous | Person::Relative => None,
        }
    } else {
        match i.person {
            Person::First => Some("muid"),
            Person::Second => Some("sibh"),
            Person::Third => Some("siad"),
            Person::Autonomous | Person::Relative => None,
        }
    };

    pronoun.map(|pronoun| {
        match &*data.get_word(pronoun).unwrap() {
            DictWord::Ga(IrishWord::IrishPronoun(p)) => p.clone(),
            _ => panic!("{} is not a pronoun", pronoun),
        }
    })
}

fn get_particle(data: &LanguageData, lemma: &str) -> Arc<VerbalParticle> {
    match &*data.get_word(lemma).unwrap() {
        DictWord::Ga(IrishWord::IrishVerbalParticle(p)) => p.clone(),
        _ => panic!("{} is not a verbal particle", lemma),
    }
}

fn generate_person(
    ctx: &TranslationContext,
    dialects: &HashSet<Dialect>,
    verb: &Arc<IrishVerb>,
    t: Tense,
    p: Person,
    n: Number,
) -> Html {
    let i = AgreementInfo { person: p, number: n, analytic_verb: false, ..Default::default() };
    let data = &ctx.ld;

    let mut vp = VP::new(verb.clone(), t, false).sentence(false);
    if let Some(pronoun) = get_pronoun(data, i) {
        let np = NP::from_pronoun(pronoun, false);
        vp = vp.subject(np);
    } else {
        vp = vp.agreement(i);
    }

    if i.person == Person::Relative {
        let p = get_particle(data, "a");
        assert!(!p.dependent_form);
        vp = vp.particle(p);
    }
    match t {
        Tense::PresentSubjunctive => {
            vp = vp.particle(get_particle(data, "go"));
        },
        Tense::PastSubjunctive => {
            vp = vp.particle(get_particle(data, "dá"));
        },
        _ => (),
    };
    let positive = vp.generate(ctx);

    vp = vp.particle(get_particle(data, "an"));
    let question = vp.generate(ctx);

    vp = vp.particle(get_particle(data, "ní"));
    let negative = vp.generate(ctx);

    let mut result = Vec::new();

    for d in DIALECTS {
        if !dialects.contains(&d.1) {
            continue;
        }

        if p == Person::Relative && (t == Tense::Imperative || t == Tense::PresentSubjunctive || t == Tense::PastSubjunctive) {
            result.push(html! {
                <div class={d.0}>{"-"}</div>
            });
        } else {
            result.push(html! {
                <div class={d.0}>{positive.best(d.1, d.2).unwrap()}</div>
            });
        }

        if p != Person::Relative && t.has_dependent_forms() {
            result.push(html! {
                <div class={d.0}>{negative.best(d.1, d.2).unwrap()}</div>
            });
            result.push(html! {
                <div class={d.0}>{question.best(d.1, d.2).unwrap()}</div>
            });
        }
    }

    html! {
        <td>
            {result}
        </td>
    }
}

fn generate_tense(
    ctx: &TranslationContext,
    dialects: &HashSet<Dialect>,
    verb: &Arc<IrishVerb>,
    t: Tense,
) -> Html {
    if t == Tense::HabitualPresent && !verb.has_habitual_present() {
        return html!{};
    }

    let mut forms = Vec::new();
    for n in Number::values() {
        for p in Person::values(true) {
            forms.push(generate_person(ctx, dialects, verb, t, p, n));
        }
    }
    for p in Person::values(false) {
        forms.push(generate_person(ctx, dialects, verb, t, p, Number::Singular));
    }

    html! {
        <tr>
            <td>{tense_to_str(t)}</td>
            {forms}
        </tr>
    }
}

fn generate_vn_va(ts: &dyn TranslationSet, dialects: &HashSet<Dialect>) -> Html {
    let mut result = Vec::new();
    for d in DIALECTS {
        if !dialects.contains(&d.1) {
            continue;
        }
        let form = match ts.best(&d.1, d.2) {
            Some(t) => format!("{}", t),
            None => "-".to_owned(),
        };
        result.push(html! {
            <div class={d.0}>{form}</div>
        });
    }

    html! {
        <td colspan="8">
            {result}
        </td>
    }
}

fn display_verb(ctx: &TranslationContext, dialects: &HashSet<Dialect>, verb: &Arc<IrishVerb>) -> Html {
    let tenses: Vec<Html> = Tense::values()
        .map(|t| generate_tense(ctx, dialects, verb, t))
        .collect();

    html! {
        <table>
            <tr>
                <th>{fl!{"tense_mood"}}</th>
                <th>{fl!{"infl_1sg"}}</th>
                <th>{fl!{"infl_2sg"}}</th>
                <th>{fl!{"infl_3sg"}}</th>
                <th>{fl!{"infl_1pl"}}</th>
                <th>{fl!{"infl_2pl"}}</th>
                <th>{fl!{"infl_3pl"}}</th>
                <th>{fl!{"infl_aut"}}</th>
                <th>{fl!{"infl_rel"}}</th>
            </tr>
            {tenses}
            <tr>
                <td>{fl!{"verbal_noun"}}</td>
                {generate_vn_va(&verb.vn(), dialects)}
            </tr>
            <tr>
                <td>{fl!{"verbal_adj"}}</td>
                {generate_vn_va(&verb.va(), dialects)}
            </tr>
        </table>
    }
}

#[derive(Properties, Clone)]
pub struct WordProps {
    pub state: Arc<Mutex<State>>,
    pub lemma: String,
}

pub struct Word {
    props: WordProps,
    link: ComponentLink<Self>,
}

pub enum Msg {
    ToggleDialect(Dialect),
}

impl Component for Word {
    type Message = Msg;
    type Properties = WordProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { props, link }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ToggleDialect(d) => {
                let mut state = self.props.state.lock().unwrap();
                if state.dialects.contains(&d) {
                    state.dialects.remove(&d);
                } else {
                    state.dialects.insert(d);
                }
                true
            },
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        // FIXME Update is required if the interface language has changed
        /*if self.props.lemma == props.lemma {
            return false;
        }*/

        self.props = props;
        true
    }

    fn view(&self) -> Html {
        let state = self.props.state.lock().unwrap();
        // FIXME Get TranslationContext from state instead of LanguageData
        let language_data = state.data.as_ref().unwrap().clone();
        let ctx = TranslationContext::new((*language_data).clone());
        let lemma = &urlencoding::decode(&self.props.lemma).unwrap();
        let word = if let Ok(w) = language_data.get_word(&lemma) {
            w
        } else {
            return html! {
                <>
                    {"This is an unknown word: "}
                    {&self.props.lemma}
                </>
            };
        };

        document().set_title(&format!("{} - Réimnigh", lemma));

        let output = match &*word {
            DictWord::Ga(IrishWord::IrishVerb(verb)) => display_verb(&ctx, &state.dialects, &verb),
            _ => html! {
                {"This is not an Irish verb"}
            }
        };

        let mut dialect_selector = Vec::new();
        for d in DIALECTS {
            let cb = self.link.callback(|_ev| Msg::ToggleDialect(d.1));

            let class = if state.dialects.contains(&d.1) {
                "btn btn-default button-active"
            } else {
                "btn btn-default"
            };

            dialect_selector.push(html! {
                <button class={class} onclick=cb>{dialect_to_str(d.1)}</button>
            });
        }

        html! {
            <>
                <div class="btn-group">
                    {dialect_selector}
                </div>
                <div>
                    <Words data=language_data default=lemma.as_ref() />
                </div>
                <div class="container">
                    {output}
                    <p>{fl!("reimnigh_conjugation_notes")}</p>
                </div>
            </>
        }
    }
}


