use crate::words::Words;
use crate::{fl, State};
use std::sync::{Arc, Mutex};
use yew::prelude::*;
use yew::utils::document;

pub struct Index {
    props: IndexProps,
}

#[derive(Properties, Clone)]
pub struct IndexProps {
    pub state: Arc<Mutex<State>>,
}

impl Component for Index {
    type Message = ();
    type Properties = IndexProps;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        let language = self.props.state.lock().unwrap().data.as_ref().unwrap().clone();

        document().set_title("Réimnigh");

        html! {
            <>
                <h2>{fl!("welcome_to_reimnigh")}</h2>
                <div>
                    <Words data=language />
                </div>
            </>
        }
    }
}
