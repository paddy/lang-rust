#![recursion_limit = "256"]

use anyhow::Error;
use crate::language::FetchResponse;
use crate::language_selector::LanguageSelector;
use crate::pages::index::Index;
use crate::pages::word::Word;
use i18n_embed::{WebLanguageRequester, fluent::{
    FluentLanguageLoader, fluent_language_loader
}};
use languages::LanguageData;
use languages::irish::Dialect;
use lazy_static::lazy_static;
use rust_embed::RustEmbed;
use std::collections::HashSet;
use std::sync::Arc;
use std::sync::Mutex;
use unic_langid::LanguageIdentifier;
use wasm_bindgen::prelude::*;
use yew_router::prelude::*;
use yew::format::Json;
use yew::prelude::*;
use yew::services::fetch::FetchTask;

mod words;
mod language;
mod language_selector;
mod pages;

#[derive(RustEmbed)]
#[folder = "i18n"]
struct Localizations;

lazy_static! {
    static ref LANGUAGE_LOADER: FluentLanguageLoader = {
        fluent_language_loader!()
    };
}

#[macro_export]
macro_rules! fl {
    ($message_id:literal) => {{
        i18n_embed_fl::fl!($crate::LANGUAGE_LOADER, $message_id)
    }};

    ($message_id:literal, $($args:expr),*) => {{
        i18n_embed_fl::fl!($crate::LANGUAGE_LOADER, $message_id, $($args), *)
    }};
}

#[derive(Switch, Clone, PartialEq)]
enum Route {
    #[to = "/word/{id}"]
    Word(String),
    #[to="/"]
    Index,
}

pub struct State {
    data: Option<Arc<LanguageData>>,
    dialects: HashSet<Dialect>,
}

pub enum Msg {
    LoadData,
    LoadDataSuccess(LanguageData),
    LoadDataError(Error),
    SwitchLang(String),
}

struct TestApp {
    link: ComponentLink<Self>,
    task: Option<FetchTask>,
    error: Option<Error>,
    state: Arc<Mutex<State>>,
}

fn switch(state: Arc<Mutex<State>>, route: Route) -> Html {
    match route {
        Route::Word(id) => html! { <Word state=state lemma=id /> },
        Route::Index => html! { <Index state=state /> },
    }
}

impl Component for TestApp {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let requested_languages = WebLanguageRequester::requested_languages();
        i18n_embed::select(
            &*LANGUAGE_LOADER,
            &Localizations,
            &requested_languages
        ).unwrap();

        link.send_message(Msg::LoadData);

        let mut dialects = HashSet::new();
        dialects.insert(Dialect::SchoolCO);

        Self {
            link,
            task: None,
            error: None,
            state: Arc::new(Mutex::new(State {
                data: None,
                dialects,
            })),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::LoadData => {
                let handler = self.link
                    .callback(move |response: FetchResponse<LanguageData>| {
                        let (_, Json(data)) = response.into_parts();
                        match data {
                            Ok(data) => Msg::LoadDataSuccess(data),
                            Err(err) => Msg::LoadDataError(err),
                        }
                    });
                self.task = Some(language::load_data(handler));
                true
            },
            Msg::LoadDataSuccess(data) => {
                self.task = None;
                self.state.lock().unwrap().data = Some(Arc::new(data));
                true
            },
            Msg::LoadDataError(err) => {
                self.task = None;
                self.error = Some(err);
                true
            },
            Msg::SwitchLang(lang) => {
                let lang: LanguageIdentifier = lang.parse().unwrap();
                i18n_embed::select(&*LANGUAGE_LOADER, &Localizations, &[lang])
                    .unwrap();
                true
            },
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        let content = if let Some(e) = &self.error {
            html! { <div>{e}</div> }
        } else if self.state.lock().unwrap().data.is_some() {
            let state = self.state.clone();
            let render = Router::render(
                move |route| switch(state.clone(), route)
            );
            html! {
                <Router<Route, ()> render=render/>
            }
        } else {
            html! {
                <div>{fl!("loading")}</div>
            }
        };

        let switch_lang =  self.link.callback(
            |lang: String| Msg::SwitchLang(lang)
        );
        html! {
            <>
                <LanguageSelector switch_lang=switch_lang />
                <h1>{"réimnigh"}</h1>
                {content}
                <footer>
                    <ul>
                        <li>
                            <a href="http://www.celtic-languages.org">
                                {"celtic-languages.org"}
                            </a>
                        </li>
                        <li>
                            <a href="https://www.celtic-languages.org/Celtic_Languages:Privacy_policy">
                                {fl!{"privacy_policy"}}
                            </a>
                        </li>
                        <li>
                            <a href="https://www.celtic-languages.org/Celtic_Languages:About">
                                {"Impressum"}
                            </a>
                        </li>
                    </ul>
                </footer>
            </>
        }
    }
}

#[wasm_bindgen(start)]
pub fn run_app() {
    console_error_panic_hook::set_once();
    App::<TestApp>::new().mount_to_body();
}
