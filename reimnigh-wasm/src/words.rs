use crate::{fl, Route};
use languages::{LanguageData, DictWord, Word};
use languages::irish::{
    Dialect,
    categories::{AgreementInfo, Number, Person, Tense},
    external::IrishWord,
};
use std::sync::Arc;
use yew::prelude::*;
use yew_router::agent::RouteRequest;
use yew_router::prelude::*;

pub struct Words {
    props: WordsProps,
}

#[derive(Properties, Clone, PartialEq)]
pub struct WordsProps {
    pub data: Arc<LanguageData>,
    #[prop_or_default]
    pub default: String,
}

fn str_normalise(src: &str) -> String {
    let mut res = String::with_capacity(src.len());
    for c in src.chars() {
        res.push(match c {
            'á' => 'a',
            'é' => 'e',
            'í' => 'i',
            'ó' => 'o',
            'ú' => 'u',
            _ => c,
        });
    }
    res
}

impl Component for Words {
    type Message = ();
    type Properties = WordsProps;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {
            props,
        }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        // FIXME Update is required if the interface language has changed
        /*if self.props == props {
            return false;
        }*/

        self.props = props;
        true
    }

    fn view(&self) -> Html {
        let language = &self.props.data;
        let mut words: Vec<_> = language.words()
            .filter_map(|word| {
                match &**word {
                    DictWord::Ga(IrishWord::IrishVerb(verb)) => {
                        let info = AgreementInfo {
                            person: Person::Second,
                            number: Number::Singular,
                            analytic_verb: false,
                            ..Default::default()
                        };
                        let ts = verb.inflect(Tense::Imperative, info, &None);
                        let lemma = ts.best(Dialect::SchoolCO, false).unwrap().to_string();
                        let normalised_lemma = str_normalise(&lemma);
                        Some((lemma, normalised_lemma, verb))
                    },
                    _ => None,
                }
            })
            .collect();

        words.sort_by(|a, b| a.1.cmp(&b.1));

        let mut options: Vec<Html> = words
            .iter()
            .map(|(lemma, _, verb)| {
                let selected = verb.lemma().contains(&self.props.default);
                html! {
                    <option selected={selected}>
                        {&lemma}
                    </option>
                }
            }).collect();

        let onchange = Callback::from(|ev| {
            if let ChangeData::Select(select) = ev {
                let mut router: RouteAgentDispatcher<()> = RouteAgentDispatcher::new();
                let route = yew_router::route::Route::from(Route::Word(select.value()));
                router.send(RouteRequest::ChangeRoute(route));
            }
        });

        if self.props.default.is_empty() {
            options.insert(0, html! {
                <option selected=true disabled=true>{fl!{"choose_verb"}}</option>
            });
        };

        html! {
            <select class="btn btn-primary" onchange={onchange}>
                {options}
            </select>
        }
    }
}
