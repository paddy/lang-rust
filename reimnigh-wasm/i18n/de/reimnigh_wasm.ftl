loading = Lade...
nav_german = Deutsch
nav_english = Englisch
nav_irish = Irisch
tense_pres = Präsens
tense_hab_pres = Habituelles Präsens
tense_past = Präteritum
tense_fut = Futur
tense_cond = Konditional
tense_hab_past = Imperfekt
tense_imp = Imperativ
tense_pres_subj = Konjunktiv Präsens
tense_past_subj = Konjunktiv Präteritum
verbal_noun = Verbalnomen
verbal_adj = Verbaladjektiv
tense_mood = Tempus/Modus
infl_1sg = 1. Person Singular
infl_2sg = 2. Person Singular
infl_3sg = 3. Person Singular
infl_1pl = 1. Person Plural
infl_2pl = 2. Person Plural
infl_3pl = 3. Person Plural
infl_aut = Autonome Form
infl_rel = Relativform
choose_verb = Verb auswählen
dialect_caighdean = Caighdeán
dialect_munster = Munster
dialect_connacht = Connacht
dialect_ulster = Ulster
welcome_to_reimnigh = Willkommen auf Réimnigh, dem Konjugator für irische Verben. Bitte wähle ein Verb aus.
reimnigh_conjugation_notes = Hinweis: Die Relativformen werden nur für direkte Relativsätze angezeigt. Nur die positiven Formen des Imperativs und Konjunktivs werden benutzt. Beide Konjunktivformen sind in der Alltagssprache beinahe vollständig außer Gebrauch. Für jede Dialektgruppe ist nur eine Form angegeben, je nach Subdialekt werden ggf. andere oder zusätzliche Varianten benutzt.
privacy_policy = Datenschutz
