pub mod english;
pub mod german;
pub mod irish;

mod concept;
mod constituent;
mod course;
mod dialect;
mod inflection;
mod languagedata;
mod lesson;
mod phrase;
mod strext;
mod translation;
mod translationcontext;

pub use concept::{ConceptDefinition, LanguageCode};
pub use constituent::{ConstituentJson, ConstituentJsonSpecific};
pub use course::{Course, Skill, SkillType};
pub use dialect::{Dialect, DialectSet};
pub use inflection::Inflection;
pub use languagedata::{Audio, AudioJson, DictWord, LanguageData, Word};
pub use lesson::Lesson;
pub use phrase::{Phrase, PhraseVariant};
pub use translationcontext::TranslationContext;

use std::any::Any;
use std::collections::HashSet;
use std::fmt::Debug;
use std::fmt::Display;
use std::sync::Arc;

pub trait Language: 'static {
    type AgreementInfo: Copy + Default;
    type Dialect: Dialect<Self> + for<'a> serde::Deserialize<'a>;
    const CODE: LanguageCode;
}

pub trait AnyDialect {
    fn language(&self) -> LanguageCode;
    fn name(&self) -> &str;
    fn str_translation(&self, text: &str) -> Box<dyn Translation>;
    fn as_any(&self) -> &dyn Any;
}

pub trait TranslationPart {
    fn text(&self) -> &str;
    fn word(&self) -> Option<&str>;
}

pub trait Translation: Display + Debug {
    fn language(&self) -> LanguageCode;
    fn text(&self) -> String;
    fn parts<'a>(&'a self) -> Box<dyn Iterator<Item = &'a dyn TranslationPart> + 'a>;
    fn has_dialect(&self, dialect: &dyn AnyDialect) -> bool;
    fn dialect_hints(&self) -> Vec<String>;
    fn tags(&self) -> HashSet<String>;
    fn words(&self) -> HashSet<String>;
    fn boxed_clone(&self) -> Box<dyn Translation>;
    fn as_any(&self) -> &dyn Any;
}

pub trait TranslationSet {
    fn language(&self) -> LanguageCode;
    fn translations<'a>(&'a self) -> Box<dyn Iterator<Item = Arc<dyn Translation>> + 'a>;
    fn best(&self, dialect: &dyn AnyDialect, most_unique: bool) -> Option<Arc<dyn Translation>>;
    fn append(&mut self, other: &dyn TranslationSet);
    fn as_any(&self) -> &dyn Any;
}

#[derive(Debug)]
pub enum ParseError {
    MissingFeature(String),
    InvalidKey(String),
    InvalidType(String, String),
    InvalidValue(String, String),
    ExpectedWordType(String),
    WordNotFound(String),
    ConceptNotFound(String),
    ParameterNotFound(String),
    NameAlreadyUsed(String),
}

impl std::error::Error for ParseError {}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::MissingFeature(feat) => write!(f, "Missing feature '{}'", feat),
            Self::InvalidKey(key) => write!(f, "Invalid key '{}'", key),
            Self::InvalidType(k, expected_type) => write!(f, "Invalid value type for key '{}', expected {}", k, expected_type),
            Self::InvalidValue(k, v) => write!(f, "Invalid value '{}' for key '{}'", v, k),
            Self::ExpectedWordType(typ) => write!(f, "Expected {}", typ),
            Self::WordNotFound(word) => write!(f, "Word not found: '{}'", word),
            Self::ConceptNotFound(concept) => write!(f, "Concept not found: '{}'", concept),
            Self::ParameterNotFound(param) => write!(f, "Parameter not found: '{}'", param),
            Self::NameAlreadyUsed(name) => write!(f, "Name already used: '{}'", name),
        }
    }
}
