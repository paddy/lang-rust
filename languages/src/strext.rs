use std::borrow::Borrow;
use std::ops::Range;

pub trait StrExt {
    fn ends_with_multi(&self, suffixes: &[&str]) -> bool;
    fn last_vowel_group(&self) -> Option<Range<usize>>;
    fn count_syllables<F>(&self, is_vowel: F) -> u8
    where
        F: Fn(char) -> bool;
}

impl<T: Borrow<str>> StrExt for T {
    fn ends_with_multi(&self, suffixes: &[&str]) -> bool {
        for suff in suffixes {
            if self.borrow().ends_with(suff) {
                return true;
            }
        }
        false
    }

    fn last_vowel_group(&self) -> Option<Range<usize>> {
        fn is_vowel(c: char) -> bool {
            "aeiouäöüAEIOUÄÖÜ".contains(c)
        }

        let mut i = self.borrow().chars().count() - 1;
        let mut it = self.borrow().chars().rev();

        while !is_vowel(it.next()?) {
            i -= 1;
        }
        let last_vowel = i;

        while is_vowel(it.next().unwrap()) {
            i -= 1;
        }
        let first_vowel = i;

        Some(first_vowel..last_vowel + 1)
    }

    fn count_syllables<F>(&self, is_vowel: F) -> u8
    where
        F: Fn(char) -> bool
    {
        let mut count = 0;
        let mut was_vowel = false;

        for letter in self.borrow().chars() {
            let vowel = is_vowel(letter);
            if !was_vowel && vowel {
                was_vowel = true;
                count += 1;
            } else if was_vowel && !vowel {
                was_vowel = false;
            }
        }

        count
    }
}
