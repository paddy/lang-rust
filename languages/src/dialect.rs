use crate::Language;
use std::collections::HashSet;
use std::fmt::Debug;
use std::hash::Hash;
use std::ops::{BitAnd, BitAndAssign, SubAssign};

pub trait Dialect<L: Language + ?Sized>: Clone + Copy + PartialEq + Eq + Hash + Debug {
    fn all() -> HashSet<L::Dialect>;
    fn name(&self) -> &str;
    fn subdialects(&self) -> HashSet<L::Dialect>;
    fn has_unnamed_subdialects(&self) -> bool {
        false
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct DialectSet<L: Language + ?Sized> {
    set: HashSet<L::Dialect>,
}

impl<L: Language> DialectSet<L> {
    pub fn empty () -> Self {
        Self {
            set: HashSet::new(),
        }
    }

    pub fn all() -> Self {
        Self::from(L::Dialect::all())
    }

    pub fn len(&self) -> usize {
        self.set.len()
    }

    pub fn is_empty(&self) -> bool {
        self.set.is_empty()
    }

    pub fn contains_all(&self) -> bool {
        self.set == L::Dialect::all()
    }

    pub fn contains(&self, dialect: L::Dialect) -> bool {
        self.set.contains(&dialect)
    }

    pub fn hash_set(&self) -> &HashSet<L::Dialect> {
        &self.set
    }

    fn propagate_subdialects(&mut self) {
        let mut s = self.set.iter().flat_map(|d| d.subdialects().into_iter()).collect::<HashSet<L::Dialect>>();
        while !self.set.is_superset(&s) {
            self.set.extend(s);
            s = self.set.iter().flat_map(|d| d.subdialects().into_iter()).collect::<HashSet<L::Dialect>>();
        }
    }

    fn collapse_subdialects(&self) -> HashSet<L::Dialect> {
        let all = L::Dialect::all();
        let mut dialects = self.set.clone();
        let mut changed = true;

        while changed {
            changed = false;
            for d in &all {
                if d.has_unnamed_subdialects() && !dialects.contains(&d) {
                    continue;
                }

                let subdialects = &d.subdialects();
                if subdialects.is_empty() {
                    continue;
                }

                // Compare with `self.set` instead of `dialects` because we may already have
                // removed some subdialect that was originally part of the set.
                if !dialects.is_disjoint(&subdialects) && self.set.is_superset(subdialects) {
                    dialects = &dialects - subdialects;
                    dialects.insert(*d);
                    changed = true;
                }
            }
        }

        dialects
    }
}

impl<L: Language> ToString for DialectSet<L> {
    fn to_string(&self) -> String {
        let mut result = String::new();
        let subdialect_set = self.collapse_subdialects();
        let mut subdialects: Vec<&str> = subdialect_set
            .iter()
            .map(|d| d.name())
            .collect();
        let len  = subdialects.len();

        subdialects.sort();

        for (i, d) in subdialects.iter().enumerate() {
            result.push_str(d);
            if i + 2 < len {
                result.push_str(", ");
            } else if i + 2 == len {
                result.push_str(" and ");
            }
        }

        result
    }
}

impl<L: Language> Clone for DialectSet<L> {
    fn clone(&self) -> Self {
        Self {
            set: self.set.clone(),
        }
    }
}

impl<L: Language> From<&[L::Dialect]> for DialectSet<L> {
    fn from(dialects: &[L::Dialect]) -> Self {
        if dialects.is_empty() {
            return Self::all();
        }

        let mut result = Self {
            set: dialects.into_iter().map(|d| *d).collect()
        };
        result.propagate_subdialects();
        result
    }
}

impl<L: Language> From<HashSet<L::Dialect>> for DialectSet<L> {
    fn from(set: HashSet<L::Dialect>) -> Self {
        if set.is_empty() {
            return Self::all();
        }

        let mut result = Self {
            set
        };
        result.propagate_subdialects();
        result
    }
}

impl<L: Language> BitAnd<&DialectSet<L>> for &DialectSet<L> {
    type Output = DialectSet<L>;

    fn bitand(self, rhs: &DialectSet<L>) -> DialectSet<L> {
        DialectSet {
            set: &self.set & &rhs.set
        }
    }
}

impl<L: Language> BitAndAssign<&DialectSet<L>> for DialectSet<L> {
    fn bitand_assign(&mut self, rhs: &DialectSet<L>) {
        self.set = &self.set & &rhs.set;
    }
}

impl<L: Language> SubAssign<&DialectSet<L>> for DialectSet<L> {
    fn sub_assign(&mut self, rhs: &DialectSet<L>) {
        self.set = &self.set - &rhs.set;
    }
}

impl<'a, L: Language> IntoIterator for &'a DialectSet<L> {
    type Item = &'a L::Dialect;
    type IntoIter = std::collections::hash_set::Iter<'a, L::Dialect>;

    fn into_iter(self) -> Self::IntoIter {
        self.set.iter()
    }
}
