use crate::ParseError;
use crate::{ConstituentJson, ConstituentJsonSpecific, DialectSet, DictWord, LanguageData};
use crate::translation::{IterOrNone, Translation, TranslationGenerator, TranslationSet};
use serde::Deserialize;
use std::sync::Arc;
use super::{German, GermanWord};
use super::categories::{AgreementInfo, Case, Gender, Number, Person, Tense};
use super::adjective::AP;
use super::np::NP;
use super::verb::Verb;

pub trait Predicative {
    fn inflect(&self) -> TranslationSet<German>;

    fn try_set_neg(&mut self, _neg: bool) -> bool {
        false
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields, rename_all="UPPERCASE")]
pub enum VPType {
    Statement,
    Infinitive,
}

impl VPType {
    fn is_sentence(&self) -> bool {
        *self == Self::Statement
    }
}

pub struct VP {
    verb: Arc<Verb>,
    subject: Option<NP>,
    object: Option<NP>,
    predicative: Option<Box<dyn Predicative>>,
    comp: Option<Box<VP>>,
    neg: bool,
    zu: bool,
}

impl VP {
    // FIXME Move out of language core
    pub fn from_json(l: &LanguageData, json: &ConstituentJson) -> Result<Self, ParseError> {
        let json = match &json.specific {
            ConstituentJsonSpecific::GermanVP(x) => x,
            _ => return Err(ParseError::InvalidValue("type".to_string(), "VP".to_string())),
        };

        let mut neg = json.neg;

        let verb = match l.get_word(&json.verb)?.as_ref() {
            DictWord::De(GermanWord::GermanVerb(verb)) => verb.clone(),
            _ => return Err(ParseError::ExpectedWordType("verb".to_string())),
        };

        let subject = json.subject.as_ref().map(|subject| {
            NP::from_json(l, &subject, &AgreementInfo {
                person: Person::Third,
                number: Number::Singular,
                case: Case::Nominative,
                gender: Gender::Masc,
            })
        }).transpose()?;

        let predicative = json.predicative.as_ref().map(|predicative| {
            let mut pred: Box<dyn Predicative> = match &predicative.specific {
                ConstituentJsonSpecific::GermanNP(_) => {
                    Box::new(NP::from_json(l, &predicative, &AgreementInfo {
                        person: Person::Third,
                        number: Number::Singular,
                        case: Case::Nominative,
                        gender: Gender::Masc,
                    })?)
                }
                ConstituentJsonSpecific::GermanAP(_) => {
                    Box::new(AP::from_json(l, &predicative)?)
                }
                _ => return Err(ParseError::InvalidValue("type".to_string(), "NP".to_string())),
            };
            if neg && pred.try_set_neg(true) {
                neg = false;
            }
            Ok(pred)
        }).transpose()?;

        let object = json.object.as_ref().map(|object| {
            let mut obj = NP::from_json(l, &object, &AgreementInfo {
                person: Person::Third,
                number: Number::Singular,
                case: Case::Accusative,
                gender: Gender::Masc,
            })?;
            if neg && obj.try_set_neg(true) {
                neg = false;
            }
            Ok(obj)
        }).transpose()?;

        let comp = json.comp.as_ref().map(|comp| {
            let mut comp_vp = Box::new(VP::from_json(l, &comp)?);
            if verb.comp_with_zu() {
                comp_vp.zu = true;
            }
            Ok(comp_vp)
        }).transpose()?;

        Ok(Self {
            verb,
            subject,
            predicative,
            object,
            comp,
            neg,
            zu: false,
        })
    }
}

impl TranslationGenerator<German> for VP {
    fn generate(&self) -> TranslationSet<German> {
        let subject = self.subject.as_ref().map(|x| x.generate());
        let object = self.object.as_ref().map(|x| x.generate());
        let predicative = self.predicative.as_ref().map(|x| x.inflect());
        let comp = self.comp.as_ref().map(|x| x.generate());

        let vptype = match subject {
            Some(_) => VPType::Statement,
            None => VPType::Infinitive,
        };
        let v2 = vptype == VPType::Statement;

        let mut res = TranslationSet::new();
        for s in subject.into_iter_or_none() {
            let agreement = s.as_ref().map(|s| s.agreement).unwrap_or_default();

            let verb = match vptype {
                VPType::Statement => self.verb.inflect(Tense::Present, agreement, v2),
                VPType::Infinitive => self.verb.infinitive(self.zu),
            };
            for v in verb {
                let mut parts: Vec<&dyn TranslationGenerator<German>> = Vec::new();
                let neg;
                let punctuation;

                if let Some(s) = &s {
                    parts.push(s);
                }
                if v2 {
                    parts.push(&v);
                }
                if let Some(o) = &object {
                    parts.push(o);
                }
                if self.neg {
                    /* TODO Make "nicht" a proper word */
                    neg = Arc::new(Translation::from_string_unreferenced(
                        "nicht",
                        AgreementInfo::default(),
                    ));
                    parts.push(&neg);
                }
                if let Some(p) = &predicative {
                    parts.push(p);
                }
                if !v2 {
                    parts.push(&v);
                }
                if let Some(c) = &comp {
                    parts.push(c);
                }

                if self.zu && vptype == VPType::Infinitive && parts.len() > 1 {
                    punctuation = Arc::new(Translation::from_string_contraction(
                        ",",
                        None,
                        true,
                        false,
                        AgreementInfo::default(),
                        &DialectSet::all(),
                        |_| unreachable!(),
                    ));
                    parts.insert(0, &punctuation);
                } else if vptype == VPType::Statement {
                    punctuation = Arc::new(Translation::from_string_contraction(
                        ".",
                        None,
                        true,
                        false,
                        AgreementInfo::default(),
                        &DialectSet::all(),
                        |_| unreachable!(),
                    ));
                    parts.push(&punctuation);
                }


                res.append(Translation::combine_full(
                    &parts,
                    v.agreement,
                    vptype.is_sentence()
                ));
            }
        }
        res
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct VPJson {
    verb: String,
    subject: Option<Arc<ConstituentJson>>,
    object: Option<Arc<ConstituentJson>>,
    predicative: Option<Arc<ConstituentJson>>,
    comp: Option<Arc<ConstituentJson>>,
    #[serde(default)]
    neg: bool,
}
