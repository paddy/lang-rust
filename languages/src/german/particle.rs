use crate::{ParseError, Word};
use crate::{DialectSet, DictWord, LanguageCode, LanguageData};
use crate::translation::{Translation, TranslationGenerator, TranslationSet};
use serde::Deserialize;
use std::sync::Arc;
use super::{German, GermanWord};
use super::categories::AgreementInfo;

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct Particle {
    word: String,
}

impl Word for Particle {
    fn id(&self) -> String {
        self.word.clone()
    }

    fn lemma(&self) -> Vec<String> {
        vec![self.word.clone()]
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::De
    }
}

pub struct Par {
    particle: Arc<Particle>,
}

impl Par {
    // FIXME Move out of language core
    pub fn from_json(l: &LanguageData, json: &ParJson) -> Result<Self, ParseError> {
        let particle = match l.get_word(&json.particle)?.as_ref() {
            DictWord::De(GermanWord::GermanParticle(particle)) => particle.clone(),
            _ => return Err(ParseError::ExpectedWordType("particle".to_string())),
        };

        Ok(Self {
            particle,
        })
    }
}

impl TranslationGenerator<German> for Par {
    fn generate(&self) -> TranslationSet<German> {
        Translation::from_dialect_form(
            &self.particle.word,
            self.particle.id(),
            AgreementInfo::default(),
            &DialectSet::all(),
        ).into()
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct ParJson {
    particle: String,
}
