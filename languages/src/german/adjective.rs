use crate::{ParseError, Word};
use crate::{
    ConstituentJson,
    ConstituentJsonSpecific,
    DialectSet,
    DictWord,
    Inflection,
    LanguageCode,
    LanguageData,
};
use crate::translation::{Translation, TranslationSet};
use serde::Deserialize;
use serde_with::{serde_as, OneOrMany};
use std::collections::HashMap;
use std::sync::Arc;
use super::{German, GermanWord};
use super::categories::{AgreementInfo, Case, DeclensionType, Gender, Number};
use super::vp::Predicative;

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct AdjectiveJson {
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<_>>")]
    forms: HashMap<String, Vec<String>>,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case", try_from="AdjectiveJson")]
pub struct Adjective {
    id: String,
    infl: Inflection<String>,
}

impl Adjective {
    fn form_id(c: Case, n: Number, g: Gender, dt: DeclensionType) -> String {
        if n == Number::Singular {
            format!("{}-{}-{}-{}", c.id(), n.id(), g.id(), dt.id())
        } else {
            format!("{}-{}-{}", c.id(), n.id(), dt.id())
        }
    }

    fn guess_form(
        c: Case,
        n: Number,
        g: Gender,
        dt: DeclensionType,
        mut form: String,
    ) -> Vec<String> {
        let endings = match n {
            Number::Singular => match g {
                Gender::Masc => match dt {
                    DeclensionType::Strong => &[ "er", "en", "em", "en" ],
                    DeclensionType::Weak   => &[ "e", "en", "en", "en" ],
                    DeclensionType::Mixed  => &[ "er", "en", "en", "en" ],
                }
                Gender::Fem => match dt {
                    DeclensionType::Strong => &[ "e", "er", "er", "e" ],
                    DeclensionType::Weak   => &[ "e", "en", "en", "e" ],
                    DeclensionType::Mixed  => &[ "e", "en", "en", "e" ],
                }
                Gender::Neut => match dt {
                    DeclensionType::Strong => &[ "es", "en", "em", "es" ],
                    DeclensionType::Weak   => &[ "e", "en", "en", "e" ],
                    DeclensionType::Mixed  => &[ "es", "en", "en", "es" ],
                }
            }
            Number::Plural => match dt {
                DeclensionType::Strong => &[ "e", "er", "en", "e" ],
                DeclensionType::Weak   => &[ "en", "en", "en", "en" ],
                DeclensionType::Mixed  => &[ "en", "en", "en", "en" ],
            }
        };

        form.push_str(endings[c as usize]);
        vec![form]
    }

    fn inflect(&self, i: AgreementInfo, dt: DeclensionType) -> TranslationSet<German> {
        let id = Self::form_id(i.case, i.number, i.gender, dt);
        self.infl
            .iter(&id)
            .map(|x| {
                Translation::from_dialect_form(&x, self.id(), i, &DialectSet::all())
            })
            .collect()
    }

    fn predicative(&self) -> TranslationSet<German> {
        self.infl
            .iter("root")
            .map(|x| {
                Translation::from_dialect_form(&x, self.id(), AgreementInfo::default(), &DialectSet::all())
            })
            .collect()
    }
}

impl TryFrom<AdjectiveJson> for Adjective {
    type Error = ParseError;

    fn try_from(mut json: AdjectiveJson) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("root".to_string(), &mut json.forms)?;

        for n in Number::values() {
            let genders = match n {
                Number::Singular => Gender::values().collect(),
                Number::Plural => vec![Gender::Masc],
            };
            for g in genders {
                for dt in DeclensionType::values() {
                    for c in Case::values() {
                        let id = Self::form_id(c, n, g, dt);
                        infl.add_from(id, &mut json.forms, "root", |form| {
                            Self::guess_form(c, n, g, dt, form)
                        });
                    }
                }
            }
        }

        infl.verify(json.forms)?;

        Ok(Self {
            id: infl.get_id_form("root")?.clone(),
            infl,
        })
    }
}


impl Word for Adjective {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("root")
            .map(|x| x.clone())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::De
    }
}

pub struct AP {
    adjective: Arc<Adjective>,
}

impl AP {
    pub fn from_json(l: &LanguageData, json: &ConstituentJson) -> Result<Self, ParseError> {
        let json = match &json.specific {
            ConstituentJsonSpecific::GermanAP(x) => x,
            _ => return Err(ParseError::InvalidValue("type".to_string(), "GermanAP".to_string())),
        };

        let adjective = match l.get_word(&json.adjective)?.as_ref() {
            DictWord::De(GermanWord::GermanAdjective(adjective)) => adjective.clone(),
            _ => return Err(ParseError::ExpectedWordType("adjective".to_string())),
        };

        Ok(Self {
            adjective,
        })
    }

    pub fn generate(&self, i: AgreementInfo, dt: DeclensionType) -> TranslationSet<German> {
        self.adjective.inflect(i, dt)
    }

    pub fn predicative(&self) -> TranslationSet<German> {
        self.adjective.predicative()
    }
}

impl Predicative for AP {
    fn inflect(&self) -> TranslationSet<German> {
        self.predicative()
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct APJson {
    adjective: String,
}
