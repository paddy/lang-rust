use crate::{ParseError, Word};
use crate::{DialectSet, Inflection, LanguageCode};
use crate::translation::{Translation, TranslationSet};
use serde::Deserialize;
use serde_with::{serde_as, OneOrMany};
use std::collections::HashMap;
use super::German;
use super::categories::{AgreementInfo, Case, Gender, Number, Person};
use super::np::NPHead;

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct PronounJson {
    person: Person,
    number: Number,
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<_>>")]
    forms: HashMap<String, Vec<String>>,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(try_from="PronounJson")]
pub struct Pronoun {
    id: String,
    infl: Inflection<String>,
    person: Person,
    // TODO Don't access in NP, use AgreementInfo instead
    pub number: Number,
}

impl Pronoun {
    fn form_id(c: Case, n: Number) -> String {
        format!("{}-{}", c.id(), n.id())
    }
}

impl TryFrom<PronounJson> for Pronoun {
    type Error = ParseError;

    fn try_from(mut json: PronounJson) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        match json.number {
            Number::Singular => {
                infl.add("sg".to_string(), &mut json.forms)?;
                for c in Case::values() {
                    let id = Self::form_id(c, Number::Singular);
                    infl.add_or_copy(id, &mut json.forms, "sg");
                }
            }
            Number::Plural => {
                infl.add("pl".to_string(), &mut json.forms)?;
                for c in Case::values() {
                    let id = Self::form_id(c, Number::Plural);
                    infl.add_or_copy(id, &mut json.forms, "pl");
                }
            }
        }

        infl.verify(json.forms)?;

        Ok(Self {
            id: infl.get_id_form("sg")?.clone(),
            infl,
            person: json.person,
            number: json.number,
        })
    }
}

impl NPHead for Pronoun {
    fn inflect(&self, c: Case, n: Number) -> TranslationSet<German> {
        let id = Self::form_id(c, n);
        self.infl
            .iter(&id)
            .map(|x| {
                let agreement = AgreementInfo {
                    person: self.person,
                    number: n,
                    case: c,
                    gender: Gender::Masc,
                };
                Translation::from_dialect_form(&x, self.id(), agreement, &DialectSet::all())
            })
            .collect()
    }
}

impl Word for Pronoun {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter(self.number.id())
            .map(|x| x.clone())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::De
    }
}
