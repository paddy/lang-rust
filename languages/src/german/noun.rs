use crate::{ParseError, Word};
use crate::{DialectSet, Inflection, LanguageCode};
use crate::translation::{Translation, TranslationSet};
use crate::strext::StrExt;
use serde::Deserialize;
use serde_with::{serde_as, OneOrMany};
use std::collections::HashMap;
use super::German;
use super::categories::{AgreementInfo, Case, Gender, Number, Person};
use super::np::NPHead;

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum NounFormJson {
    Short(String),
    Full {
        form: String,
        gender: Option<Gender>,
    },
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(from="NounFormJson")]
pub struct NounForm {
    text: String,
    gender: Gender,
}

impl From<NounFormJson> for NounForm {
    fn from(noun: NounFormJson) -> Self {
        match noun {
            NounFormJson::Short(text) => Self {
                text,
                gender: Gender::Masc,
            },
            NounFormJson::Full { form, gender } => Self {
                text: form,
                gender: gender.unwrap_or(Gender::Masc),
            }
        }
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct NounJson {
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<_>>")]
    forms: HashMap<String, Vec<NounForm>>,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(try_from="NounJson")]
pub struct Noun {
    id: String,
    infl: Inflection<NounForm>,
}

impl Noun {
    fn form_id(c: Case, n: Number) -> String {
        format!("{}-{}", c.id(), n.id())
    }

    fn guess_pl(mut form: NounForm) -> Vec<NounForm> {
       if form.gender == Gender::Fem {
            if form.text.ends_with("in") {
                form.text.push_str("nen");
            } else if form.text.ends_with("e") {
                form.text.push_str("n");
            } else {
                form.text.push_str("en");
            }
        } else if form.text.ends_with_multi(&["a", "o", "u"]) {
            form.text.push_str("s");
        } else if form.text.ends_with_multi(&["chen", "lein", "el", "er"]) {
            // Leave the text unchanged
        } else {
            // FIXME plural_umlaut(sg.text) + 'e'
            form.text.push_str("e");
        };

        vec![form]
    }

    fn guess_sg_case(c: Case, mut form: NounForm) -> Vec<NounForm> {
        if c == Case::Genitive {
            if form.gender != Gender::Fem {
                if form.text.ends_with_multi(&["s", "z"]) {
                    form.text.push_str("es");
                } else {
                    form.text.push_str("s");
                }
            }
        }
        vec![form]
    }

    fn guess_pl_case(c: Case, mut form: NounForm) -> Vec<NounForm> {
        if c == Case::Dative {
            if !form.text.ends_with("n") {
                form.text.push_str("n");
            }
        }
        vec![form]
    }
}

impl NPHead for Noun {
    fn inflect(&self, c: Case, n: Number) -> TranslationSet<German> {
        let id = Self::form_id(c, n);
        self.infl
            .iter(&id)
            .map(|x| {
                let agreement = AgreementInfo {
                    person: Person::Third,
                    number: n,
                    case: c,
                    gender: x.gender,
                };
                Translation::from_dialect_form(&x.text, self.id(), agreement, &DialectSet::all())
            })
            .collect()
    }
}

impl TryFrom<NounJson> for Noun {
    type Error = ParseError;

    fn try_from(mut json: NounJson) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("sg".to_string(), &mut json.forms)?;
        infl.add_from("pl".to_string(), &mut json.forms, "sg", Self::guess_pl);

        for c in Case::values() {
            let id = Self::form_id(c, Number::Singular);
            infl.add_from(id, &mut json.forms, "sg", |form| Self::guess_sg_case(c, form));

            let id = Self::form_id(c, Number::Plural);
            infl.add_from(id, &mut json.forms, "pl", |form| Self::guess_pl_case(c, form));
        }

        infl.verify(json.forms)?;

        Ok(Self {
            id: infl.get_id_form("sg")?.text.clone(),
            infl,
        })
    }
}

impl Word for Noun {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("sg")
            .map(|x| x.text.clone())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::De
    }
}
