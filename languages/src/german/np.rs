use crate::ParseError;
use crate::{ConstituentJson, ConstituentJsonSpecific, DictWord, LanguageData};
use crate::translation::{Translation, TranslationGenerator, TranslationSet};
use serde::Deserialize;
use std::sync::Arc;
use super::{German, GermanWord};
use super::adjective::AP;
use super::categories::{AgreementInfo, Case, DeclensionType, Definiteness, Gender, Number};
use super::vp::Predicative;

pub trait NPHead {
    fn inflect(&self, c: Case, n: Number) -> TranslationSet<German>;
}

pub struct NP {
    noun: Arc<dyn NPHead>,
    number: Number,
    case: Case,
    definite: Definiteness,
    neg: bool,
    attributes: Vec<AP>,
}

impl NP {
    // FIXME Move out of language core
    pub fn from_json(
        l: &LanguageData,
        json: &ConstituentJson,
        i: &AgreementInfo,
    ) -> Result<Self, ParseError> {
        let json = match &json.specific {
            ConstituentJsonSpecific::GermanNP(x) => x,
            _ => return Err(ParseError::InvalidValue("type".to_string(), "NP".to_string())),
        };

        let mut number = json.number;
        let head: Arc<dyn NPHead> = match l.get_word(&json.noun)?.as_ref() {
            DictWord::De(GermanWord::GermanNoun(noun)) => noun.clone(),
            DictWord::De(GermanWord::GermanPronoun(pronoun)) => {
                number = Some(pronoun.number);
                pronoun.clone()
            }
            _ => return Err(ParseError::ExpectedWordType("noun".to_string())),
        };

        let mut attributes = Vec::new();
        for a in &json.attributes {
            attributes.push(AP::from_json(l, &a)?);
        }

        Ok(Self {
            noun: head,
            number: number.unwrap_or(Number::Singular),
            case: json.case.unwrap_or(i.case),
            definite: json.definite.unwrap_or(Definiteness::Bare),
            neg: false,
            attributes,
        })
    }

    fn generate_article(
        &self,
        case: Case,
        number: Number,
        gender: Gender,
        definite: Definiteness
    ) -> TranslationSet<German> {
        let forms = match definite {
            Definiteness::Definite => match number {
                Number::Singular => match case {
                    Case::Nominative => &["der", "die", "das"],
                    Case::Genitive   => &["des", "der", "des"],
                    Case::Dative     => &["dem", "der", "dem"],
                    Case::Accusative => &["den", "die", "das"],
                }
                Number::Plural => match case {
                    Case::Nominative => &["die", "die", "die"],
                    Case::Genitive   => &["der", "der", "der"],
                    Case::Dative     => &["den", "den", "den"],
                    Case::Accusative => &["die", "die", "die"],
                }
            }
            Definiteness::Indefinite => {
                assert!(number == Number::Singular);
                match case {
                    Case::Nominative => &["ein",   "eine",  "ein"],
                    Case::Genitive   => &["eines", "einer", "eines"],
                    Case::Dative     => &["einem", "einer", "einem"],
                    Case::Accusative => &["einen", "eine",  "ein"],
                }
            }
            Definiteness::Bare => unreachable!(),
        };

        let mut form = forms[gender as usize];

        let k_form;
        if self.neg {
            k_form = "k".to_owned() + form;
            form = &k_form;
        }

        Translation::from_string_unreferenced(&form, AgreementInfo::default()).into()
    }

    pub fn try_set_neg(&mut self, neg: bool) -> bool {
        if neg && self.definite != Definiteness::Indefinite {
            false
        } else {
            self.neg = neg;
            true
        }
    }
}

impl TranslationGenerator<German> for NP {
    fn generate(&self) -> TranslationSet<German> {
        let mut ts = TranslationSet::new();

        for noun in self.noun.inflect(self.case, self.number) {
            let mut parts: Vec<&dyn TranslationGenerator<German>> = Vec::new();

            // TODO if (not isinstance(self.noun, GermanPronoun)
            let (need_article, dt) = match self.definite {
                Definiteness::Definite => (true, DeclensionType::Weak),
                Definiteness::Indefinite => {
                    (noun.agreement.number == Number::Singular, DeclensionType::Mixed)
                }
                Definiteness::Bare => (false, DeclensionType::Strong),
            };

            let article;
            if need_article {
                article = self.generate_article(
                    noun.agreement.case,
                    noun.agreement.number,
                    noun.agreement.gender,
                    self.definite
                );
                parts.push(&article);
            }

            let attributes: Vec<_> = self.attributes.iter().map(|x| {
                x.generate(noun.agreement, dt)
            }).collect();

            for a in &attributes {
                parts.push(a);
            }

            parts.push(&noun);
            ts.append(Translation::combine(&parts, noun.agreement));
        }

        ts
    }
}

impl Predicative for NP {
    fn inflect(&self) -> TranslationSet<German> {
        self.generate()
    }

    fn try_set_neg(&mut self, neg: bool) -> bool {
        NP::try_set_neg(self, neg)
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct NPJson {
    noun: String,
    number: Option<Number>,
    case: Option<Case>,
    definite: Option<Definiteness>,
    #[serde(default)]
    attributes: Vec<ConstituentJson>,
}
