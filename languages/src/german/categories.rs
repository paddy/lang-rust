use serde::{Deserialize, Serialize};

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Number {
    Singular,
    Plural,
}
impl Number {
    pub fn id(&self) -> &str {
        match self {
            Self::Singular => "sg",
            Self::Plural => "pl",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        vec![Self::Singular, Self::Plural].into_iter()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Gender{
    Masc,
    Fem,
    Neut,
}

impl Gender {
    pub fn id(&self) -> &str {
        match self {
            Self::Masc => "m",
            Self::Fem => "f",
            Self::Neut=> "n",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        vec![Self::Masc, Self::Fem, Self::Neut].into_iter()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Case {
    Nominative,
    Genitive,
    Dative,
    Accusative,
}

impl Case {
    pub fn id(&self) -> &str {
        match self {
            Self::Nominative => "nom",
            Self::Genitive => "gen",
            Self::Dative => "dat",
            Self::Accusative => "acc",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        vec![Self::Nominative, Self::Genitive, Self::Dative, Self::Accusative].into_iter()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Definiteness {
    Bare,
    Indefinite,
    Definite,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum DeclensionType {
    Strong,
    Weak,
    Mixed,
}

impl DeclensionType {
    pub fn id(&self) -> &str {
        match self {
            Self::Strong => "str",
            Self::Weak => "weak",
            Self::Mixed => "mixed",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        vec![Self::Strong, Self::Weak, Self::Mixed].into_iter()
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Person {
    First,
    Second,
    Third,
}

impl Person {
    pub fn id(&self) -> &str {
        match self {
            Self::First => "1",
            Self::Second => "2",
            Self::Third => "3",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        vec![Self::First, Self::Second, Self::Third].into_iter()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Tense {
    Present,
    Preterite,
}

impl Tense {
    pub fn id(&self) -> &str {
        match self {
            Self::Present => "pres",
            Self::Preterite => "pt",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        vec![Self::Present, Self::Preterite].into_iter()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct AgreementInfo {
    pub person: Person,
    pub number: Number,
    pub case: Case,
    pub gender: Gender,
}

impl Default for AgreementInfo {
    fn default() -> Self {
        Self {
            person: Person::Third,
            number: Number::Singular,
            case: Case::Nominative,
            gender: Gender::Masc,
        }
    }
}
