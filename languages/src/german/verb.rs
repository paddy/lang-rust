use crate::{ParseError, Word};
use crate::{DialectSet, Inflection, LanguageCode};
use crate::translation::{Translation, TranslationSet};
use crate::strext::StrExt;
use serde::Deserialize;
use serde_with::{serde_as, OneOrMany};
use std::collections::HashMap;
use super::German;
use super::categories::{AgreementInfo, Gender, Number, Person, Tense};

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum VerbFormJson {
    Short(String),
    Full {
        form: String,
        // FIXME Verbs shouldn't have gender
        gender: Option<Gender>,
    },
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(from="VerbFormJson", deny_unknown_fields)]
pub struct VerbForm {
    prefix: Option<String>,
    text: String,
    gender: Gender,
}

impl VerbForm {
    fn get_text(&self, separate_prefix: bool, zu: bool) -> String {
        if let Some(p) = &self.prefix {
            if zu {
                format!("{}zu{}", p, self.text)
            } else if separate_prefix {
                format!("{} {}", self.text, p)
            } else {
                format!("{}{}", p, self.text)
            }
        } else if zu {
            format!("zu {}", self.text)
        } else {
            self.text.clone()
        }
    }
}

impl From<VerbFormJson> for VerbForm {
    fn from(verb: VerbFormJson) -> Self {
        let (text, gender) = match verb {
            VerbFormJson::Short(text) => (text, Gender::Masc),
            VerbFormJson::Full { form, gender } => (form, gender.unwrap_or(Gender::Masc)),
        };

        let (prefix, text) = match text.split_once('|') {
            None => (None, text),
            Some((p, r)) => (Some(p.to_owned()), r.to_owned()),
        };

        Self { prefix, text, gender }
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct VerbJson {
    #[serde(default)]
    strong: bool,
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<_>>")]
    forms: HashMap<String, Vec<VerbForm>>,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(try_from="VerbJson")]
pub struct Verb {
    id: String,
    strong: bool,
    infl: Inflection<VerbForm>,
}

impl Verb {
    fn form_id(t: Tense, n: Number, p: Person) -> String {
        format!("{}-{}{}", t.id(), p.id(), n.id())
    }

    fn guess_root(mut form: VerbForm) -> Vec<VerbForm> {
        if form.text.ends_with("en") {
            form.text.truncate(form.text.len() - 2);
        } else if form.text.ends_with_multi(&["ern", "eln"]) {
            form.text.truncate(form.text.len() - 1);
        }
        vec![form]
    }

    fn guess_past(mut form: VerbForm) -> Vec<VerbForm> {
        form.text.push_str("te");
        vec![form]
    }

    fn guess_past_pt(mut form: VerbForm, strong: bool) -> Vec<VerbForm> {
        form.text.insert_str(0, "ge");
        if strong {
            form.text.push_str("en");
        } else {
            form.text.push_str("t");
        }
        vec![form]
    }

    fn present_umlaut(mut text: String) -> String {
        if let Some(vowels) = text.last_vowel_group() {
            match &text[vowels.clone()] {
                "a" => text.replace_range(vowels, "ä"),
                "e" => text.replace_range(vowels, "i"),
                "au" => text.replace_range(vowels, "äu"),
                _ => {}
            }
        }
        text
    }

    fn guess_form(ending: &str, umlaut: bool, mut form: VerbForm) -> Vec<VerbForm> {
        if umlaut {
            form.text = Self::present_umlaut(form.text);
        }

        if form.text.ends_with("e") && ending.starts_with("e") {
            form.text.push_str(&ending[1..]);
        } else if form.text.ends_with_multi(&["s", "ß"]) && ending.starts_with("s") {
            form.text.push_str(&ending[1..]);
        } else {
            form.text.push_str(ending);
        }

        vec![form]
    }

    pub fn comp_with_zu(&self) -> bool {
        true
    }

    pub fn inflect(
        &self,
        t: Tense,
        i: AgreementInfo,
        separate_prefix: bool,
    ) -> TranslationSet<German> {
        let id = Self::form_id(t, i.number, i.person);
        self.infl
            .iter(&id)
            .map(|x| Translation::from_dialect_form(
                &x.get_text(separate_prefix, false),
                self.id(),
                i,
                &DialectSet::all()
            ))
            .collect()
    }

    pub fn infinitive(&self, zu: bool) -> TranslationSet<German> {
        self.infl
            .iter("inf")
            .map(|x| Translation::from_dialect_form(
                &x.get_text(false, zu),
                self.id(),
                AgreementInfo::default(),
                &DialectSet::all()
            ))
            .collect()
    }
}

impl TryFrom<VerbJson> for Verb {
    type Error = ParseError;

    fn try_from(mut json: VerbJson) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("inf".to_string(), &mut json.forms)?;
        infl.add_from("root", &mut json.forms, "inf", Self::guess_root);
        infl.add_from("pres", &mut json.forms, "root", |form| vec![form]);
        infl.add_from("pt", &mut json.forms, "root", Self::guess_past);
        infl.add_from("part-pt", &mut json.forms, "root",
            |form| Self::guess_past_pt(form, json.strong));

        for t in Tense::values() {
            let endings = match t {
                Tense::Present =>  [ "e", "st", "t", "en", "t", "en" ],
                Tense::Preterite => [ "", "st", "", "en", "t", "en" ],
            };

            for n in Number::values() {
                for p in Person::values() {
                    let id = Self::form_id(t, n, p);
                    let umlaut = json.strong && ["pres-2sg", "pres-3sg"].contains(&id.as_str());

                    infl.add_from(
                        id,
                        &mut json.forms,
                        t.id(),
                        |form| Self::guess_form(
                            endings[3 * (n as usize) + (p as usize)],
                            umlaut,
                            form
                        )
                    );
                }
            }
        }

        infl.verify(json.forms)?;

        Ok(Self {
            id: infl.get_id_form("inf")?.get_text(false, false),
            strong: json.strong,
            infl,
        })
    }
}

impl Word for Verb {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("inf")
            .map(|x| x.get_text(false, false))
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::De
    }
}
