use super::LanguageData;

pub struct TranslationContext {
    pub ld: Box<LanguageData>,
}

impl TranslationContext {
    pub fn new(ld: LanguageData) -> Self {
        Self {
            ld: Box::new(ld),
        }
    }
}
