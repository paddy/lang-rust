use serde::Deserialize;
use serde_json::Value;
use std::sync::Arc;

use crate::{self as languages, Language, TranslationContext};
use crate::translation::{Translation, TranslationGenerator, TranslationSet};

use super::german::{
    VP as GermanVP,
    VPJson as GermanVPJson,
    NP as GermanNP,
    NPJson as GermanNPJson,
    Par as GermanPar,
    ParJson as GermanParJson,
    AP as GermanAP,
    APJson as GermanAPJson,
};
use super::irish::categories::{Case as IrishCase};
use super::irish::external::{
    AP as IrishAP,
    CopP as IrishCopP,
    NP as IrishNP,
    Par as IrishPar,
    PP as IrishPP,
    VP as IrishVP,
};

// TODO Replace all the Values with real implementations
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(tag="type")]
pub enum ConstituentJsonSpecific {
    GermanVP(GermanVPJson),
    GermanNP(GermanNPJson),
    GermanAP(GermanAPJson),
    GermanPar(GermanParJson),
    IrishCopP(IrishCopP),
    IrishVP(IrishVP),
    IrishNP(IrishNP),
    IrishPP(IrishPP),
    IrishAP(IrishAP),
    IrishPar(IrishPar),
    EnglishNP(Value),
    EnglishVP(Value),
    EnglishPar(Value),
}

impl ConstituentJson {
    pub fn generate_post<L: Language>(
        post_vec: &Vec<ConstituentJson>,
        ctx: &TranslationContext,
        lemma: bool
    ) -> Vec<TranslationSet<L>> {
        let mut post_ts_vec = Vec::new();
        for post in post_vec {
            let mut post_ts = TranslationSet::new();
            for t in post.do_generate(ctx, lemma).translations() {
                post_ts.append(t.as_any().downcast_ref::<Translation<L>>().unwrap().clone().into());
            }
            post_ts_vec.push(post_ts);
        }

        post_ts_vec
    }

    fn do_generate_post<L: Language>(
        &self,
        ctx: &TranslationContext,
        lemma: bool
    ) -> Vec<TranslationSet<L>> {
        Self::generate_post(&self.post, ctx, lemma)
    }

    fn do_generate_irish(&self, ctx: &TranslationContext, lemma: bool) -> Box<dyn languages::TranslationSet> {
        use ConstituentJsonSpecific as Spec;

        let ld = &ctx.ld;
        let mut ts = TranslationSet::new();
        let mut parts: Vec<&dyn TranslationGenerator<_>> = Vec::new();
        let mut add_post = true;

        let result = match &self.specific {
            Spec::IrishCopP(_) => IrishCopP::into(self, ld).unwrap().generate(ctx),
            Spec::IrishNP(_) => IrishNP::into(self, ld, IrishCase::Nominative).unwrap().generate(ctx),
            Spec::IrishPP(_) => IrishPP::into(self, ld).unwrap().generate(ctx),
            Spec::IrishVP(_) => {
                add_post = false;
                let mut vp = IrishVP::into(self, ld).unwrap();
                if lemma {
                    vp = vp.dictionary();
                }
                vp.generate(ctx)
            }
            Spec::IrishAP(_) => IrishAP::into(self, ld).unwrap().generate(),
            Spec::IrishPar(_) => IrishPar::into(self, ld).unwrap().generate(),
            _ => panic!("Non-Irish constituent"),
        };
        parts.push(&result);

        let post_ts_vec;
        if add_post {
            post_ts_vec = self.do_generate_post(ctx, lemma);
            for post_ts in post_ts_vec.iter() {
                parts.push(post_ts);
            }
        }

        ts.append(Translation::combine(&parts, Default::default()));
        Box::new(ts)
    }

    fn do_generate_german(&self, ctx: &TranslationContext, lemma: bool) -> Box<dyn languages::TranslationSet> {
        use ConstituentJsonSpecific as Spec;

        let ld = &ctx.ld;
        let mut ts = TranslationSet::new();
        let mut parts: Vec<&dyn TranslationGenerator<_>> = Vec::new();

        let result = match &self.specific {
            Spec::GermanNP(_) => GermanNP::from_json(ld, self, &Default::default()).unwrap().generate(),
            Spec::GermanVP(_) => GermanVP::from_json(ld, self).unwrap().generate(),
            Spec::GermanAP(_) => GermanAP::from_json(ld, self).unwrap().predicative(),
            Spec::GermanPar(x) => GermanPar::from_json(ld, x).unwrap().generate(),
            _ => panic!("Non-German constituent"),
        };
        parts.push(&result);

        let post_ts_vec = self.do_generate_post(ctx, lemma);
        for post_ts in post_ts_vec.iter() {
            parts.push(post_ts);
        }

        ts.append(Translation::combine(&parts, Default::default()));
        Box::new(ts)
    }

    fn do_generate(&self, ctx: &TranslationContext, lemma: bool) -> Box<dyn languages::TranslationSet> {
        use ConstituentJsonSpecific as Spec;

        match &self.specific {
            Spec::GermanNP(_) | Spec::GermanVP(_) | Spec::GermanAP(_) | Spec::GermanPar(_) => {
                self.do_generate_german(ctx, lemma)
            }
            Spec::IrishCopP(_) | Spec::IrishNP(_) | Spec::IrishPP(_) | Spec::IrishVP(_) | Spec::IrishAP(_) | Spec::IrishPar(_) => {
                self.do_generate_irish(ctx, lemma)
            },
            Spec::EnglishNP(_) | Spec::EnglishVP(_) | Spec::EnglishPar(_) => {
                Box::new(TranslationSet::<super::irish::Irish>::new())
            }
        }
    }

    pub fn generate(&self, ctx: &TranslationContext) -> Box<dyn languages::TranslationSet> {
        self.do_generate(ctx, false)
    }

    pub fn lemma(&self, ctx: &TranslationContext) -> Vec<Arc<dyn languages::Translation>> {
        self.do_generate(ctx, true).translations().collect()
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct ConstituentJson {
    #[serde(flatten)]
    pub specific: ConstituentJsonSpecific,
    #[serde(default)]
    pub post: Vec<ConstituentJson>,
}
