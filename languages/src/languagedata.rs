use enum_dispatch::enum_dispatch;
use serde::Deserialize;
use serde_json::Value;
use std::collections::{HashMap, HashSet};
use std::sync::Arc;

use crate::ParseError;
use super::{ConceptDefinition, Course, DialectSet, Language, LanguageCode, Lesson, Phrase, TranslationContext};
use super::english::EnglishWord;
use super::german::GermanWord;
use super::irish::external::IrishWord;

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields, rename_all="kebab-case")]
pub enum AudioJson<L: Language> {
    Short(String),
    Full {
        url: String,
        dialects: HashSet<L::Dialect>,
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Audio<L: Language> {
    pub url: String,
    pub dialects: DialectSet<L>,
}

impl<L: Language> From<AudioJson<L>> for Audio<L> {
    fn from(audio: AudioJson<L>) -> Self {
        match audio {
            AudioJson::Short(url) => Self {
                url,
                dialects: DialectSet::all(),
            },
            AudioJson::Full { url, dialects } => Self {
                url,
                dialects: DialectSet::from(dialects),
            },
        }
    }
}

#[enum_dispatch]
pub trait Word {
    fn id(&self) -> String;
    fn lemma(&self) -> Vec<String>;
    fn language(&self) -> LanguageCode;
}

impl<T: Word> Word for Arc<T> {
    fn lemma(&self) -> Vec<String> {
        self.as_ref().lemma()
    }
    fn id(&self) -> String {
        self.as_ref().id()
    }
    fn language(&self) -> LanguageCode {
        self.as_ref().language()
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(tag="language", rename_all="kebab-case")]
#[enum_dispatch(Word)]
pub enum DictWord {
    De(GermanWord),
    En(EnglishWord),
    Ga(IrishWord),
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct LanguageDataJson {
    words: Vec<Arc<DictWord>>,
    concepts: Vec<Arc<ConceptDefinition>>,
    phrases: Vec<Arc<Phrase>>,
    lessons: Vec<Arc<Lesson>>,
    courses: Vec<Arc<Course>>,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(try_from="LanguageDataJson")]
pub struct LanguageData {
    pub json: LanguageDataJson,
    pub words: HashMap<String, Arc<DictWord>>,
    pub word_ids: HashMap<String, Arc<DictWord>>,
    pub concepts: HashMap<String, Arc<ConceptDefinition>>,
}

impl LanguageData {
    // FIXME WordNotFound is not a parse error
    pub fn get_word(&self, key: &str) -> Result<Arc<DictWord>, ParseError> {
        self.words
            .get(key)
            .ok_or_else(|| ParseError::WordNotFound(key.to_string()))
            .map(Arc::clone)
    }

    pub fn get_concept(&self, key: &str) -> Option<Arc<ConceptDefinition>> {
        self.concepts
            .get(key)
            .map(Arc::clone)
    }

    pub fn word_by_id(&self, id: &str) -> Option<Arc<DictWord>> {
        self.word_ids.get(id).map(Arc::clone)
    }

    pub fn words(&self) -> impl Iterator<Item=&Arc<DictWord>> {
        self.json.words.iter()
    }

    pub fn concepts(&self) -> impl Iterator<Item=&Arc<ConceptDefinition>> {
        self.json.concepts.iter()
    }

    pub fn phrases(&self) -> impl Iterator<Item=&Arc<Phrase>> {
        self.json.phrases.iter()
    }

    pub fn lessons(&self) -> impl Iterator<Item=&Arc<Lesson>> {
        self.json.lessons.iter()
    }

    pub fn courses(&self) -> impl Iterator<Item=&Arc<Course>> {
        self.json.courses.iter()
    }

    pub fn apply_concepts(&self, data: Value) -> Result<Vec<Value>, ParseError> {
        super::concept::apply_concepts(data, &self, None)
    }

    pub fn apply_concepts_lang<L: Language>(&self, data: Value) -> Result<Vec<Value>, ParseError> {
        super::concept::apply_concepts(data, &self, Some(L::CODE))
    }
}

impl TryFrom<LanguageDataJson> for LanguageData {
    type Error = ParseError;

    fn try_from(json: LanguageDataJson) -> Result<Self, ParseError> {
        let mut ld = Self {
            json,
            words: HashMap::new(),
            word_ids: HashMap::new(),
            concepts: HashMap::new(),
        };

        for word in ld.json.words.iter() {
            let language = word.language().to_string();
            if ld.word_ids.insert(word.id(), word.clone()).is_some() {
                // TODO Enable error once English has real IDs
                //return Err(ParseError::NameAlreadyUsed(word.id()));
            }

            for lemma in word.lemma() {
                let full_alias = format!("@{}:{}", language, lemma);
                // TODO Catch duplicates
                ld.words.insert(full_alias, word.clone());
                // TODO Catch duplicates
                ld.words.insert(lemma, word.clone());
            }
        }

        let mut ctx = TranslationContext::new(ld);

        for concept in ctx.ld.json.concepts.iter() {
            for alias in concept.aliases(&ctx) {
                // TODO Catch duplicates
                ctx.ld.concepts.insert(alias, concept.clone());
            }
        }

        Ok(*ctx.ld)
    }
}
