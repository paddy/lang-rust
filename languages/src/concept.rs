use itertools::Itertools;
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use std::collections::HashMap;

use crate::ParseError;
use super::{LanguageData, ConstituentJson, TranslationContext};

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all="kebab-case")]
pub enum LanguageCode {
    De,
    En,
    Ga,
}

impl ToString for LanguageCode {
    fn to_string(&self) -> String {
        match serde_json::value::to_value(self).unwrap() {
            Value::String(s) => s,
            _ => panic!("enum didn't serialize into a string"),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct ConceptExpansion {
    language: LanguageCode,
    ambiguity: Option<Vec<String>>,
    param: Option<HashMap<String, Value>>,
    data: Value,
}

fn init_true() -> bool {
    true
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct ConceptDefinition {
    alias: Option<String>,
    #[serde(default="init_true")]
    dictionary: bool,
    translations: Vec<ConceptExpansion>,
}

impl ConceptDefinition {
    pub fn lemma(
        &self,
        ctx: &TranslationContext,
    ) -> Result<impl Iterator<Item=String> + '_ , ParseError> {
        let mut result = Vec::new();

        for translation in &self.translations {
            let json = apply_concepts_param(
                translation.data.clone(),
                &ctx.ld,
                Some(translation.language),
                translation.param.as_ref().unwrap_or(&HashMap::new()),
            )?;

            for json in json {
                let json_data: ConstituentJson = serde_json::from_value(json).unwrap();
                /*let json_data = serde_json::from_value(json.clone());
                if let Err(_) = json_data {
                    println!("{}", json);
                }
                let json_data: ConstituentJson = json_data.unwrap();
                */
                for t in json_data.lemma(ctx) {
                    result.push(format!("{}", t));
                }
            }
        }

        Ok(result.into_iter())
    }

    pub fn aliases(&self, ctx: &TranslationContext) -> Box<dyn Iterator<Item=String> + '_> {
        let result = self.alias.iter().map(|s| s.clone());

        if self.in_dictionary() {
            // FIXME Error handling
            Box::new(result.chain(self.lemma(&ctx).unwrap()))
        } else {
            Box::new(result)
        }
    }

    pub fn in_dictionary(&self) -> bool {
        self.dictionary
    }
}

fn apply_concepts_to_fields(
    map: &Map<String, Value>,
    param: &HashMap<String, Value>,
    ld: &LanguageData,
    lang: Option<LanguageCode>,
    remove_param: &Option<HashMap<String, Value>>,
) -> Result<Vec<Map<String, Value>>, ParseError> {
    let mut base = Map::new();
    let mut variants: Vec<Vec<(String, Value)>> = Vec::new();

    if map.contains_key("concept") {
        let data = apply_concepts_param(Value::Object(map.clone()), &ld, lang, &param)?;
        return Ok(data.into_iter().map(|x| match x {
            Value::Object(obj) => obj,
            _ => panic!(),
        }).collect());
    }

    for (k, v) in map {
        if remove_param.as_ref().map_or(false, |x| x.contains_key(k)) {
            continue;
        }

        match v {
            Value::Object(obj) => {
                let v = apply_concepts_param(Value::Object(obj.clone()), &ld, lang, &param)?;
                variants.push(v.into_iter().map(|x| (k.clone(), x)).collect());
            },
            Value::Array(list) => {
                if list.is_empty() {
                    base.insert(k.clone(), v.clone());
                    continue;
                }

                let mut new_list_variants = Vec::new();
                for elem in list {
                    let v = apply_concepts_param(elem.clone(), &ld, lang, &param)?;
                    new_list_variants.push(v);
                }
                let v = new_list_variants
                    .into_iter()
                    .multi_cartesian_product()
                    .map(|x| (k.clone(), Value::Array(x)))
                    .collect();
                variants.push(v);
            }
            _ => {
                base.insert(k.clone(), v.clone());
            }
        }
    }

    let mut new_maps = Vec::new();
    if variants.is_empty() {
        new_maps.push(base);
    } else {
        for variant in variants.into_iter().multi_cartesian_product() {
            let mut map = base.clone();
            for (k, v) in variant {
                map.insert(k, v);
            }
            new_maps.push(map);
        }
    }

    Ok(new_maps)
}

fn apply_param(
    map: &mut Value,
    param: &HashMap<String, Value>,
) -> Result<(), ParseError> {
    match map {
        Value::String(s) => {
            if s.starts_with('$') {
                let param_name = &s[1..];
                let mut param_val = param.get(param_name)
                    .ok_or_else(|| ParseError::ParameterNotFound(s.clone()))?
                    .clone();
                apply_param(&mut param_val, param)?;
                *map = param_val;
            }
        }
        Value::Object(obj) => {
            for (_, v) in obj.iter_mut() {
                apply_param(v, param)?;
            }

            let include = obj.remove("include");
            if let Some(Value::Object(mut include)) = include {
                obj.append(&mut include);
            }
        }
        Value::Array(list) => {
            for elem in list.iter_mut() {
                apply_param(elem, param)?;
            }
        }
        _ => ()
    }

    Ok(())
}

pub fn apply_concepts_param(
    mut data: Value,
    ld: &LanguageData,
    lang: Option<LanguageCode>,
    param: &HashMap<String, Value>,
) -> Result<Vec<Value>, ParseError> {
    apply_param(&mut data, param)?;
    let mut map = match data {
        Value::Object(map) => map,
        _ => return Ok(vec![data]),
    };

    let concept = map.remove("concept");

    let mut result = Vec::new();

    if let Some(concept) = concept {
        match concept {
            Value::String(concept_name) => {
                let c = match ld.get_concept(&concept_name) {
                    Some(c) => c,
                    None => Err(ParseError::ConceptNotFound(concept_name))?
                };

                for t in &c.translations {
                    if lang.map(|lang| t.language != lang).unwrap_or(false) {
                        continue;
                    }
                    let lang = lang.or(Some(t.language));

                    let mut cur_param = param.clone();
                    if let Some(param) = &t.param {
                        for (k, v) in param {
                            cur_param.insert(k.clone(), map.get(k).unwrap_or(v).clone());
                        }
                    }
                    let new_maps = apply_concepts_to_fields(&map, &cur_param, ld, lang, &t.param)?;

                    let mut cdata = t.data.clone();
                    apply_param(&mut cdata, &cur_param)?;
                    let cdata = if let Value::Object(cdata) = cdata {
                        cdata
                    } else {
                        return Err(ParseError::InvalidType("data".to_owned(), "object".to_owned()));
                    };
                    let c_new_maps = apply_concepts_to_fields(&cdata, &cur_param, ld, lang, &None)?;

                    for map in &new_maps {
                        for cmap in &c_new_maps {
                            let mut cmap = cmap.clone();
                            for (k, v) in map.iter() {
                                cmap.insert(k.clone(), v.clone());
                            }
                            result.push(Value::Object(cmap));
                        }
                    }
                }
            }
            _ => return Err(ParseError::InvalidType("concept".to_owned(), "string".to_owned())),
        };
    } else {
        let new_maps = apply_concepts_to_fields(&map, param, ld, lang, &None)?;
        for map in new_maps {
            result.push(Value::Object(map));
        }
    }

    Ok(result)
}

pub fn apply_concepts(
    data: Value,
    ld: &LanguageData,
    lang: Option<LanguageCode>,
) -> Result<Vec<Value>, ParseError> {
    apply_concepts_param(data, ld, lang, &HashMap::new())
}
