use serde::Deserialize;
use serde_json::Value;

use super::LanguageCode;

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct LessonSectionText {
    pub title: String,
    pub text: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct LessonSectionWords {
    pub title: Option<String>,
    pub words: Vec<String>,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct LessonSectionExercise {
    #[serde(default)]
    pub phrases: Vec<String>,
    #[serde(default)]
    pub phrases_random: u32,
    #[serde(default)]
    pub concepts: Vec<String>,
    #[serde(default)]
    pub concepts_random: u32,
    #[serde(default)]
    pub translate_to_target: u32,
    #[serde(default)]
    pub translate_from_target: u32,
    #[serde(default)]
    pub concept_to_target: u32,
    #[serde(default)]
    pub concept_from_target: u32,
    #[serde(default)]
    pub correct_translation: u32,
    #[serde(default)]
    pub exercises: Vec<Value>,
    #[serde(default)]
    pub focus_skill: u32,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(tag="type", rename_all="kebab-case")]
pub enum LessonSection {
    Text(LessonSectionText),
    Words(LessonSectionWords),
    Exercise(LessonSectionExercise),
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct Lesson {
    pub name: String,
    pub alias: String,
    pub source_lang: LanguageCode,
    pub target_lang: LanguageCode,
    pub sections: Vec<LessonSection>,
}
