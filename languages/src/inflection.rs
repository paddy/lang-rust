use crate::ParseError;
use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq)]
pub struct Inflection<T> {
    // FIXME This should be private
    pub forms: HashMap<String, Vec<T>>,
    verified: bool,
}

impl<T: Clone> Inflection<T> {
    pub fn new() -> Self {
        Self {
            forms: HashMap::new(),
            verified: false,
        }
    }

    pub fn add<S: Into<String>>(
        &mut self,
        form_id: S,
        forms: &mut HashMap<String, Vec<T>>
    ) -> Result<(), ParseError> {
        let form_id = form_id.into();
        match forms.remove(&form_id) {
            Some(defs) => {
                self.forms.insert(form_id, defs);
                Ok(())
            }
            None => Err(ParseError::MissingFeature(form_id))
        }
    }

    pub fn add_from<S: Into<String>>(
        &mut self,
        form_id: S,
        forms: &mut HashMap<String, Vec<T>>,
        src_form_id: &str,
        guess: impl Fn(T) -> Vec<T>
    ) {
        let mut form_id = form_id.into();
        if let Some(defs) = forms.remove(&form_id) {
            self.forms.insert(form_id, defs);
            return;
        }

        let src_defs = self.forms.get(src_form_id).unwrap_or_else(|| {
            panic!("Could not find form '{}'", src_form_id);
        });
        let mut new = Vec::new();
        for def in src_defs {
            new.append(&mut guess(def.clone()));
        }

        form_id.push('+');
        if let Some(mut defs) = forms.remove(&form_id) {
            new.append(&mut defs);
        }
        form_id.pop();

        self.forms.insert(form_id, new);
    }

    pub fn add_or_copy<S: Into<String>>(
        &mut self,
        form_id: S,
        forms: &mut HashMap<String, Vec<T>>,
        src_form_id: &str
    ) {
        self.add_from(form_id, forms, src_form_id, |x| vec![x])
    }

    pub fn get_id_form(
        &self,
        src_form_id: &str,
    ) -> Result<&T, ParseError> {
        let src_forms = match self.forms.get(src_form_id) {
            Some(x) => x,
            None => return Err(ParseError::InvalidKey(src_form_id.to_owned())),
        };
        if src_forms.len() != 1 {
            return Err(ParseError::MissingFeature("id".to_owned()));
        }
        Ok(&src_forms[0])
    }

    pub fn verify(&mut self, forms: HashMap<String, Vec<T>>) -> Result<(), ParseError> {
        self.verified = true;

        if !forms.is_empty() {
            let key = forms.keys().next().unwrap().to_owned();
            return Err(ParseError::InvalidKey(key));
        }

        Ok(())
    }

    pub fn iter<S: AsRef<str>>(&self, form_id: S) -> impl Iterator<Item = &T> {
        self.forms.get(form_id.as_ref()).unwrap_or_else(|| {
            panic!("Could not find form '{}'", form_id.as_ref());
        }).iter()
    }
}

impl<T> Drop for Inflection<T> {
    fn drop(&mut self) {
        if !self.verified {
            panic!("Inflection was not verified");
        }
    }
}
