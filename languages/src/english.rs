use crate::Word;
use crate::LanguageCode;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct EnglishWord {
}

impl Word for EnglishWord {
    fn lemma(&self) -> Vec<String> {
        vec![]
    }

    fn id(&self) -> String {
        "english_word".to_owned()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::En
    }
}

