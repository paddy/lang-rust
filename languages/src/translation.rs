use crate::{self as languages, DialectSet, Language, LanguageCode};
use itertools::Itertools;
use std::any::Any;
use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::Arc;

#[derive(Clone)]
pub struct TranslationPart {
    pub text: String,
    pub contract_with_prev: bool,
    pub contract_with_next: bool,
    word: Option<String>,
    tags: HashSet<String>,
}

impl languages::TranslationPart for TranslationPart {
    fn text(&self) -> &str {
        &self.text
    }

    fn word(&self) -> Option<&str> {
        self.word.as_ref().map(|x| x.as_str())
    }
}

pub struct Translation<L: Language> {
    pub parts: Vec<TranslationPart>,
    pub agreement: L::AgreementInfo,
    pub dialects: DialectSet<L>,
    pub preferred_in_dialects: HashMap<L::Dialect, u32>,
    pub dialect_features: HashMap<String, DialectSet<L>>,
    tags: HashSet<String>,
}

impl<L: Language> languages::Translation for Translation<L> {
    fn language(&self) -> LanguageCode {
        L::CODE
    }

    fn text(&self) -> String {
        format!("{}", self)
    }

    fn parts(&self) -> Box<dyn Iterator<Item = &dyn languages::TranslationPart> + '_> {
        Box::new(self.parts.iter().map(|x| x as &dyn languages::TranslationPart))
    }

    fn has_dialect(&self, dialect: &dyn languages::AnyDialect) -> bool {
        let Some(dialect) = dialect.as_any().downcast_ref::<L::Dialect>() else {
            return false
        };

        self.dialects.contains(*dialect)
    }

    fn dialect_hints(&self) -> Vec<String> {
        Self::dialect_hints(&self)
    }

    fn tags(&self) -> HashSet<String> {
        let mut res = self.tags.clone();
        for p in &self.parts {
            res.extend(p.tags.iter().cloned());
        }
        res
    }

    fn words(&self) -> HashSet<String> {
        let mut res = HashSet::new();
        for p in &self.parts {
            if let Some(word) = &p.word {
                res.insert(word.clone());
            }
        }
        res
    }

    fn boxed_clone(&self) -> Box<dyn super::Translation> {
        Box::new(self.clone())
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

// Implement manually because derive(Clone) would require L: Clone
impl<L: Language> Clone for Translation<L> {
    fn clone(&self) -> Self {
        Self {
            parts: self.parts.clone(),
            agreement: self.agreement.clone(),
            dialects: self.dialects.clone(),
            preferred_in_dialects: self.preferred_in_dialects.clone(),
            dialect_features: self.dialect_features.clone(),
            tags: self.tags.clone(),
        }
    }
}

impl<L: Language> Translation<L> {
    pub fn from_string_full<F>(
        s: &str,
        word: Option<String>,
        contract_with_prev: bool,
        contract_with_next: bool,
        agreement: L::AgreementInfo,
        dialects: &DialectSet<L>,
        dialect_hint: F,
        preferred_in_dialects: &DialectSet<L>,
        ending_preferred_in_dialects: &DialectSet<L>,
    ) -> Self
        where F: FnOnce(&str) -> String
    {
        let mut preferred_in_dialects_map: HashMap<L::Dialect, _> = HashMap::new();
        for d in preferred_in_dialects {
            preferred_in_dialects_map.insert(*d, 1);
        }
        for d in ending_preferred_in_dialects {
            *preferred_in_dialects_map.entry(*d).or_insert(0) += 1;
        }
        let mut dialect_features = HashMap::new();
        if !dialects.contains_all() {
            dialect_features.insert(dialect_hint(&s), dialects.clone());
        }
        Self {
            parts: vec![TranslationPart {
                text: s.to_owned(),
                contract_with_prev,
                contract_with_next,
                tags: HashSet::new(),
                word,
            }],
            agreement,
            dialects: dialects.clone(),
            preferred_in_dialects: preferred_in_dialects_map,
            dialect_features: dialect_features,
            tags: HashSet::new(),
        }
    }

    pub fn from_string_contraction<F>(
        s: &str,
        word: Option<String>,
        contract_with_prev: bool,
        contract_with_next: bool,
        agreement: L::AgreementInfo,
        dialects: &DialectSet<L>,
        dialect_hint: F,
    ) -> Self
        where F: FnOnce(&str) -> String
    {
        Self::from_string_full(s, word, contract_with_prev, contract_with_next, agreement, dialects, dialect_hint, &DialectSet::empty(), &DialectSet::empty())
    }

    pub fn from_string<F>(
        s: &str,
        word: String,
        agreement: L::AgreementInfo,
        dialects: &DialectSet<L>,
        dialect_hint: F,
    ) -> Self
        where F: FnOnce(&str) -> String
    {
        Self::from_string_full(s, Some(word), false, false, agreement, dialects, dialect_hint, &DialectSet::empty(), &DialectSet::empty())
    }

    pub fn from_dialect_form(
        s: &str,
        word: String,
        agreement: L::AgreementInfo,
        dialects: &DialectSet<L>,
    ) -> Self {
        Self::from_string_full(
            s,
            Some(word),
            false,
            false,
            agreement,
            dialects,
            |s: &str| format!("The form '{}'", s),
            &DialectSet::empty(),
            &DialectSet::empty()
        )
    }

    pub fn from_dialect_string(
        s: &str,
        dialects: &DialectSet<L>,
    ) -> Self {
        Self::from_string_full(
            s,
            None,
            false,
            false,
            L::AgreementInfo::default(),
            dialects,
            |s: &str| format!("The form '{}'", s),
            &DialectSet::empty(),
            &DialectSet::empty()
        )
    }

    pub fn from_string_unreferenced(s: &str, agreement: L::AgreementInfo) -> Self {
        Self::from_string_full(
            s,
            None,
            false,
            false,
            agreement,
            &DialectSet::all(),
            |s: &str| format!("The form '{}'", s),
            &DialectSet::empty(),
            &DialectSet::empty()
        )
    }

    pub fn combine_full<T: TranslationGenerator<L>>(
        translations: &[T],
        agreement: L::AgreementInfo,
        capitalise: bool,
    ) -> TranslationSet<L> {
        let mut res = vec![];
        let variants = translations
            .into_iter()
            .map(TranslationGenerator::generate)
            .multi_cartesian_product();

        for v in variants {
            let mut parts = Vec::new();
            let mut dialects = DialectSet::all();
            let mut preferred_in_dialects = HashMap::new();
            let mut dialect_features = HashMap::new();
            let mut tags = HashSet::new();

            for t in v {
                for p in &t.parts {
                    parts.push(p.clone());
                }
                dialects &= &t.dialects;
                for (d, cnt) in &t.preferred_in_dialects {
                    *preferred_in_dialects.entry(*d).or_insert(0) += cnt;
                }
                for (k, v) in &t.dialect_features {
                    if let Some(entry) = dialect_features.get_mut(k) {
                        *entry &= v;
                    } else {
                        dialect_features.insert(k.clone(), v.clone());
                    }
                }
                tags.extend(t.tags.iter().cloned());
            }

            if capitalise {
                if let Some(p) = parts.get_mut(0) {
                    // FIXME Be less lazy and more efficient
                    if !p.text.is_empty() {
                        let c = p.text.remove(0);
                        p.text.insert_str(0, &c.to_uppercase().to_string());
                    }
                }
            }

            res.push(Translation {
                parts,
                agreement,
                dialects,
                preferred_in_dialects,
                dialect_features,
                tags: tags.into_iter().collect(),
            });
        }

        TranslationSet::from_iter(res)
    }

    pub fn combine<T: TranslationGenerator<L>>(
        translations: &[T],
        agreement: L::AgreementInfo,
    ) -> TranslationSet<L> {
        Self::combine_full(translations, agreement, false)
    }

    pub fn add_word_tag(&mut self, tag: String) {
        self.parts[0].tags.insert(tag);
    }

    pub fn add_expression_tag(&mut self, tag: String) {
        self.tags.insert(tag);
    }

    pub fn add_dialect_constraint<F>(&mut self, dialects: &DialectSet<L>, hint: F)
        where F: FnOnce(&Translation<L>) -> String
    {
        if dialects.contains_all() {
            return;
        }
        self.dialects &= dialects;
        self.dialect_features.insert(hint(&self), dialects.clone());
    }

    pub fn dialect_hints(&self) -> Vec<String> {
        let mut v = Vec::new();
        for (feature, dialects) in &self.dialect_features {
            if !dialects.contains_all() {
                v.push(format!("{} is typical for {}", feature, dialects.to_string()));
            }
        }
        v
    }
}

impl<L: Language> TranslationGenerator<L> for Arc<Translation<L>> {
    fn generate(&self) -> TranslationSet<L> {
        TranslationSet {
            translations: vec![self.clone()],
        }
    }
}

impl<L: Language> std::fmt::Display for Translation<L> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let parts: &Vec<TranslationPart> = self.parts.as_ref();
        let mut texts: Vec<&str> = Vec::new();
        let mut add_space = false;

        for p in parts.into_iter() {
            if p.text.is_empty() {
                continue;
            }
            if add_space && !p.contract_with_prev {
                texts.push(" ");
            }
            texts.push(&p.text);
            add_space = !p.contract_with_next;
        }
        texts.join("").fmt(f)
    }
}

impl<L: Language> std::fmt::Debug for Translation<L> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(self, f)
    }
}

#[derive(Clone)]
pub struct TranslationSet<L: Language> {
    translations: Vec<Arc<Translation<L>>>,
}

impl<L: Language> languages::TranslationSet for TranslationSet<L> {
    fn language(&self) -> LanguageCode {
        L::CODE
    }

    fn translations(&self) -> Box<dyn Iterator<Item = Arc<dyn languages::Translation>> + '_> {
        Box::new(self.translations.iter().map(|x| x.clone() as _))
    }

    fn best(&self, dialect: &dyn languages::AnyDialect, most_unique: bool) -> Option<Arc<dyn languages::Translation>> {
        let dialect = dialect.as_any().downcast_ref::<L::Dialect>()?;
        self.best(*dialect, most_unique).map(|x| x as _)
    }

    fn append(&mut self, other: &dyn languages::TranslationSet) {
        if let Some(other) = other.as_any().downcast_ref::<Self>() {
            self.append_ref(other);
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl<L: Language> TranslationSet<L> {
    pub fn new() -> Self {
        Self {
            translations: Vec::new(),
        }
    }

    pub fn append(&mut self, other: TranslationSet<L>) {
        self.append_ref(&other);
    }

    pub fn append_ref(&mut self, other: &TranslationSet<L>) {
        // FIXME If the same string+agreement exists already, merge the translations
        for t in &other.translations {
            self.translations.push(t.clone());
        }
    }

    pub fn map<F>(mut self, f: F) -> Self
        where F: Fn(&mut Translation<L>)
    {
        for t in &mut self.translations {
            f(Arc::make_mut(t));
        }
        self
    }

    pub fn best(&self, dialect: L::Dialect, most_unique: bool) -> Option<Arc<Translation<L>>> {
        let mut best = None;
        let mut best_dialect_count = 0;
        let mut preferred_count = 0;

        for t in &self.translations {
            if !t.dialects.contains(dialect) {
                continue;
            }

            let cur_preferred_count = *t.preferred_in_dialects.get(&dialect).unwrap_or(&0);
            let is_better = if preferred_count < cur_preferred_count {
                true
            } else if preferred_count > cur_preferred_count {
                false
            } else if best.is_none() {
                true
            } else if most_unique {
                 t.dialects.len() < best_dialect_count
            } else {
                 t.dialects.len() > best_dialect_count
            };

            if is_better {
                best = Some(t.clone());
                preferred_count = cur_preferred_count;
                best_dialect_count = t.dialects.len();
            }
        }

        best
    }
}

pub trait IterOrNone {
    type Item;
    fn into_iter_or_none(self) -> std::vec::IntoIter<Option<Self::Item>>;
}

impl<L: Language> IterOrNone for Option<TranslationSet<L>> {
    type Item = Arc<Translation<L>>;
    fn into_iter_or_none(self) -> std::vec::IntoIter<Option<Self::Item>> {
        let res = if let Some(ts) = self {
            ts.into_iter().map(|t| Some(t)).collect()
        } else {
            vec![None]
        };
        res.into_iter()
    }
}

impl<L: Language, F> IterOrNone for Option<(TranslationSet<L>, Vec<F>)> {
    type Item = (Arc<Translation<L>>, F);
    fn into_iter_or_none(self) -> std::vec::IntoIter<Option<Self::Item>> {
        let res = if let Some((ts, forms)) = self {
            ts.into_iter().zip(forms.into_iter()).map(|t| Some(t)).collect()
        } else {
            vec![None]
        };
        res.into_iter()
    }
}

impl<F> IterOrNone for Option<Vec<F>> {
    type Item = F;
    fn into_iter_or_none(self) -> std::vec::IntoIter<Option<Self::Item>> {
        let res = if let Some(forms) = self {
            forms.into_iter().map(|t| Some(t)).collect()
        } else {
            vec![None]
        };
        res.into_iter()
    }
}

impl<L: Language> From<Translation<L>> for TranslationSet<L> {
    fn from(t: Translation<L>) -> Self {
        Self {
            translations: vec![Arc::new(t)],
        }
    }
}

impl<L: Language> FromIterator<Translation<L>> for TranslationSet<L> {
    fn from_iter<I: IntoIterator<Item = Translation<L>>>(it: I) -> Self {
        Self {
            translations: it.into_iter().map(|t| Arc::new(t)).collect(),
        }
    }
}

impl<L: Language> TranslationGenerator<L> for TranslationSet<L> {
    fn generate(&self) -> TranslationSet<L> {
        Self {
            translations: self.translations.clone(),
        }
    }
}

impl<L: Language> IntoIterator for TranslationSet<L> {
    type Item = Arc<Translation<L>>;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.translations.into_iter()
    }
}

pub trait TranslationGenerator<L: Language> {
    fn generate(&self) -> TranslationSet<L>;
}

impl<L: Language> TranslationGenerator<L> for &dyn TranslationGenerator<L> {
    fn generate(&self) -> TranslationSet<L> {
        (*self).generate()
    }
}
