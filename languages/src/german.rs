use crate::{self as languages, Language, LanguageCode, Word};
use crate::translation::Translation;
use maplit::hashset;
use serde::{Deserialize, Serialize};
use std::any::Any;
use std::collections::HashSet;
use std::sync::Arc;

pub mod adjective;
pub mod categories;
pub mod noun;
pub mod particle;
pub mod pronoun;
pub mod verb;

pub mod np;
pub mod vp;

// TODO Remove pub
pub use adjective::{AP, APJson, Adjective};
pub use noun::Noun;
pub use np::{NP, NPJson};
pub use particle::{Par, ParJson, Particle};
pub use pronoun::Pronoun;
pub use verb::Verb;
pub use vp::{VP, VPJson};

pub struct German;
impl Language for German {
    type Dialect = Dialect;
    type AgreementInfo = categories::AgreementInfo;
    const CODE: LanguageCode = LanguageCode::De;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="SCREAMING_SNAKE_CASE")]
pub enum Dialect {
    DeSued,
    DeNord,
    At,
    Ch,
}

impl languages::Dialect<German> for Dialect {
    fn all() -> HashSet<Self> {
        hashset!{
            Dialect::DeSued,
            Dialect::DeNord,
            Dialect::At,
            Dialect::Ch,
        }
    }

    fn subdialects(&self) -> HashSet<Self> {
        HashSet::new()
    }

    fn name(&self) -> &str {
        match self {
            Dialect::DeSued => "Süddt.",
            Dialect::DeNord => "Norddt.",
            Dialect::At => "Österreich",
            Dialect::Ch => "Schweiz",
        }
    }
}

impl languages::AnyDialect for Dialect {
    fn language(&self) -> LanguageCode {
        German::CODE
    }

    fn name(&self) -> &str {
        languages::Dialect::<German>::name(self)
    }

    fn str_translation(&self, text: &str) -> Box<dyn languages::Translation> {
        Box::new(Translation::<German>::from_dialect_string(text, &[*self][..].into()))
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(tag="type")]
pub enum GermanWord {
    GermanNoun(Arc<Noun>),
    GermanVerb(Arc<Verb>),
    GermanAdjective(Arc<Adjective>),
    GermanParticle(Arc<Particle>),
    GermanPronoun(Arc<Pronoun>),
}

impl Word for GermanWord {
    fn lemma(&self) -> Vec<String> {
        match self {
            GermanWord::GermanAdjective(adjective) => adjective.lemma(),
            GermanWord::GermanNoun(noun) => noun.lemma(),
            GermanWord::GermanParticle(particle) => particle.lemma(),
            GermanWord::GermanPronoun(pronoun) => pronoun.lemma(),
            GermanWord::GermanVerb(verb) => verb.lemma(),
        }
    }

    fn id(&self) -> String {
        match self {
            GermanWord::GermanAdjective(adjective) => adjective.id(),
            GermanWord::GermanNoun(noun) => noun.id(),
            GermanWord::GermanParticle(particle) => particle.id(),
            GermanWord::GermanPronoun(pronoun) => pronoun.id(),
            GermanWord::GermanVerb(verb) => verb.id(),
        }
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::De
    }
}
