use itertools::Itertools;
use serde::Deserialize;
use serde_json::Value;
use std::collections::{HashMap, HashSet};
use std::sync::Arc;

use crate::ParseError;
use super::{ConstituentJson, LanguageCode, Translation, TranslationContext, TranslationSet};

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct PhraseExpansion {
    language: Vec<LanguageCode>,
    data: Value,
    var: Option<HashMap<String, Vec<Value>>>,
}

fn var_sets(vars: Option<&HashMap<String, Vec<Value>>>) -> Vec<HashMap<String, Value>> {
    let mut vec: Vec<Vec<(String, Value)>> = Vec::new();

    if let Some(vars) = vars {
        for (k, variants) in vars {
            vec.push(variants.iter().map(|v| (k.clone(), v.clone())).collect());
        }
    }

    if vec.is_empty() {
        return vec![HashMap::new()];
    }

    let sets = vec
        .into_iter()
        .multi_cartesian_product()
        .map(|v| v.into_iter().collect::<HashMap<String, Value>>())
        .collect();

    sets
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum PhraseJson {
    Short(Vec<PhraseExpansion>),
    Full {
        phrase: Vec<PhraseExpansion>,
        param: HashMap<String, Vec<Value>>,
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(from="PhraseJson", deny_unknown_fields)]
pub struct Phrase {
    phrase: Vec<PhraseExpansion>,
    param: HashMap<String, Vec<Value>>,
}

impl From<PhraseJson> for Phrase {
    fn from(json: PhraseJson) -> Self {
        match json {
            PhraseJson::Short(phrase) => Self {
                phrase,
                param: HashMap::new(),
            },
            PhraseJson::Full { phrase, param } => Self {
                phrase,
                param
            },
        }
    }
}

impl Phrase {
    pub fn variants(self: &Arc<Self>) -> Vec<PhraseVariant> {
        var_sets(Some(&self.param))
            .into_iter()
            .map(|param| PhraseVariant {
                phrase: self.clone(),
                param,
            })
            .collect()
    }

    pub fn has_language(&self, lang: LanguageCode) -> bool {
        for exp in &self.phrase {
            if exp.language.contains(&lang) {
                return true;
            }
        }
        false
    }

    pub fn languages(&self) -> HashSet<LanguageCode> {
        let mut res = HashSet::new();
        for exp in &self.phrase {
            res.extend(&exp.language);
        }
        res
    }
}

pub struct PhraseVariant {
    phrase: Arc<Phrase>,
    param: HashMap<String, Value>,
}

impl PhraseVariant {
    fn translation_sets(
        &self,
        ctx: &TranslationContext,
        lang: Option<LanguageCode>,
    ) -> Result<Vec<Box<dyn TranslationSet>>, ParseError> {
        let mut result = Vec::new();

        for exp in &self.phrase.phrase {
            for exp_lang in &exp.language {
                if let Some(lang) = lang {
                    if lang != *exp_lang {
                        continue;
                    }
                }

                for mut vars in var_sets(exp.var.as_ref()) {
                    if let Some(name) = self.param.keys().find(|k| vars.contains_key(*k)) {
                        return Err(ParseError::NameAlreadyUsed(name.clone()));
                    }
                    vars.extend(self.param.iter().map(|(k, v)| (k.clone(), v.clone())));

                    let data = super::concept::apply_concepts_param(
                        exp.data.clone(),
                        &ctx.ld,
                        Some(*exp_lang),
                        &vars
                    )?;

                    for d in data {
                        let phrase: ConstituentJson = serde_json::from_value(d).unwrap();
                        result.push(phrase.generate(&ctx));
                    }
                }
            }
        }

        Ok(result)
    }

    fn do_translations(
        &self,
        ctx: &TranslationContext,
        lang: Option<LanguageCode>,
    ) -> Result<Vec<Box<dyn Translation>>, ParseError> {
        let mut translations = Vec::new();

        for ts in self.translation_sets(ctx, lang)? {
            for t in ts.translations() {
                translations.push(t.boxed_clone());
            }
        }

        Ok(translations)
    }

    pub fn translations(&self, ctx: &TranslationContext) -> Result<Vec<Box<dyn Translation>>, ParseError> {
        self.do_translations(ctx, None)
    }

    pub fn translations_lang(&self, ctx: &TranslationContext, lang: LanguageCode) -> Result<Vec<Box<dyn Translation>>, ParseError> {
        self.do_translations(ctx, Some(lang))
    }

    pub fn best_translation(&self, ctx: &TranslationContext, dialect: &dyn super::AnyDialect, most_unique: bool) -> Option<Arc<dyn Translation>> {
        let mut ts_iter = self.translation_sets(ctx, Some(dialect.language())).ok()?.into_iter();
        let mut merged = ts_iter.next()?;
        for other in ts_iter {
            merged.append(&*other);
        }
        merged.best(dialect, most_unique)
    }
}
