use crate::{ParseError, Word};
use crate::{Inflection, LanguageCode};
use crate::translation::{Translation, TranslationSet};
use std::collections::HashMap;
use super::has_slender_cons_ending;
use super::{DialectSet, Irish};
use super::categories::{AgreementInfo, Case, Definiteness, Gender, Number};

#[derive(Debug, Clone, PartialEq)]
pub struct NounForm {
    pub text: String,
    pub gender: Gender,
    pub dialects: DialectSet<Irish>,
}

impl NounForm {
    pub fn new(text: String, gender: Option<Gender>, dialects: DialectSet<Irish>) -> Self {
        let gender = match gender {
            Some(x) => x,
            None => if has_slender_cons_ending(&text) { Gender::Fem } else { Gender::Masc },
        };
        Self {
            text,
            gender,
            dialects,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Noun {
    id: String,
    infl: Inflection<NounForm>,
    dialects: DialectSet<Irish>,
}

impl Noun {
    fn form_id(&self, c: Case, n: Number) -> String {
        format!("{}-{}", c.id(), n.id())
    }

    pub fn inflect(&self, c: Case, n: Number, def: Definiteness) -> TranslationSet<Irish> {
        let form_case = match c {
            Case::Genitive => c,
            _ => Case::Nominative,
        };
        let id = self.form_id(form_case, n);
        self.infl
            .iter(&id)
            .map(|x| {
                let i = AgreementInfo {
                    case: c,
                    number: n,
                    gender: x.gender,
                    ..Default::default()
                };
                let mut t = Translation::from_dialect_form(&x.text, self.id(), i, &x.dialects);
                t.add_dialect_constraint(&self.dialects, |t| format!("The word '{}'", t));
                t.add_word_tag(format!(
                    "noun-{}-{}-{}-{}",
                    def.id(),
                    c.id(),
                    n.id(),
                    x.gender.id()
                ));
                t
            })
            .collect()
    }

    pub fn new(
        mut forms: HashMap<String, Vec<NounForm>>,
        dialects: DialectSet<Irish>,
    ) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("nom-sg".to_string(), &mut forms)?;
        infl.add("gen-sg".to_string(), &mut forms)?;
        infl.add("nom-pl".to_string(), &mut forms)?;
        infl.add("gen-pl".to_string(), &mut forms)?;

        infl.verify(forms)?;

        Ok(Self {
            id: infl.get_id_form("nom-sg")?.text.clone(),
            infl,
            dialects,
        })
    }
}

impl Word for Noun {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("nom-sg")
            .map(|x| x.text.clone())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::Ga
    }
}
