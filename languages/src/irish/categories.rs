use serde::{Deserialize, Serialize};
use super::Mutation;

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Number {
    Singular,
    Plural,
}
impl Number {
    pub fn id(&self) -> &str {
        match self {
            Self::Singular => "sg",
            Self::Plural => "pl",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        vec![Self::Singular, Self::Plural].into_iter()
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Person {
    First,
    Second,
    Third,
    Autonomous,
    Relative,
}

impl Person {
    pub fn id(&self) -> &str {
        match self {
            Self::First => "1",
            Self::Second => "2",
            Self::Third => "3",
            Self::Autonomous => "sb",
            Self::Relative => "rel",
        }
    }

    pub fn has_number(&self) -> bool {
        match self {
            Self::First | Self::Second | Self::Third => true,
            Self::Autonomous | Self::Relative => false,
        }
    }

    pub fn values(has_number: bool) -> impl Iterator<Item=Self> {
        if has_number {
            vec![Self::First, Self::Second, Self::Third].into_iter()
        } else {
            vec![Self::Autonomous, Self::Relative].into_iter()
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Gender {
    Masc,
    Fem,
}

impl Gender {
    pub fn id(&self) -> &str {
        match self {
            Self::Masc=> "m",
            Self::Fem=> "f",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        use Gender::*;
        vec![
            Masc,
            Fem,
        ].into_iter()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Case {
    Nominative,
    Genitive,
    Dative,
    Vocative,
}

impl Case {
    pub fn id(&self) -> &str {
        match self {
            Self::Nominative=> "nom",
            Self::Genitive => "gen",
            Self::Dative => "dat",
            Self::Vocative => "voc",
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        use Case::*;
        vec![
            Nominative,
            Genitive,
            Dative,
            Vocative,
        ].into_iter()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Definiteness {
    /*
     * Bare and indefinite are the same in Irish, but provide both for
     * convenience (can make the same concept definition work for multiple
     * languages
     */
    Bare,
    Indefinite,
    Definite,
}

impl Definiteness {
    pub fn id(&self) -> &str {
        match self {
            Self::Bare => "bare",
            Self::Indefinite => "indef",
            Self::Definite => "def",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum Tense {
    Present,
    HabitualPresent,
    Past,
    Future,
    Conditional,
    HabitualPast,
    Imperative,
    PresentSubjunctive,
    PastSubjunctive,
}

impl Tense {
    pub fn id(&self) -> &str {
        match self {
            Self::Present => "pres",
            Self::HabitualPresent => "hab-pres",
            Self::Past => "past",
            Self::Future => "fut",
            Self::Conditional => "cond",
            Self::HabitualPast => "hab-past",
            Self::Imperative => "imp",
            Self::PresentSubjunctive => "pres-subj",
            Self::PastSubjunctive => "past-subj",
        }
    }

    pub fn ext_id(&self) -> String {
        match self {
            Self::Present => "present",
            _ => self.id(),
        }.to_owned()
    }

    pub fn has_dependent_forms(&self) -> bool {
        match self {
            Self::Present |
            Self::HabitualPresent |
            Self::Past |
            Self::Future |
            Self::Conditional |
            Self::HabitualPast |
            Self::PastSubjunctive => true,

            Self::Imperative |
            Self::PresentSubjunctive => false,
        }
    }

    pub fn values() -> impl Iterator<Item=Self> {
        vec![
            Self::Present,
            Self::HabitualPresent,
            Self::Past,
            Self::Future,
            Self::Conditional,
            Self::HabitualPast,
            Self::Imperative,
            Self::PresentSubjunctive,
            Self::PastSubjunctive,
        ].into_iter()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Conjugation {
    First,
    Second,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct AgreementInfo {
    pub person: Person,
    pub case: Case,
    pub number: Number,
    pub gender: Gender,
    pub analytic_verb: bool,
    pub dat_mutation: Mutation,
}

impl Default for AgreementInfo {
    fn default() -> Self {
        AgreementInfo {
            person: Person::Third,
            case: Case::Nominative,
            number: Number::Singular,
            gender: Gender::Masc,
            analytic_verb: false,
            dat_mutation: Mutation::None,
        }
    }
}
