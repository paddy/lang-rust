use crate::{ParseError, Word};
use crate::{Inflection, LanguageCode};
use crate::translation::{IterOrNone, Translation, TranslationGenerator, TranslationSet};
use crate::strext::StrExt;
use lazy_static::lazy_static;
use maplit::hashmap;
use std::collections::HashMap;
use std::sync::Arc;
use super::categories::{AgreementInfo, Conjugation, Number, Person, Tense};
use super::{Dialect, DialectSet, Irish, Mutation};
use super::{add_ending, depalatalise, has_slender_ending, has_vocalic_start, is_vowel};
use super::verbalparticle::{VerbalParticle, VerbalParticleForm};

#[derive(Debug, Clone, PartialEq)]
pub struct IrishVerbForm {
    pub analytic: bool,
    pub past_particles: bool,
    pub no_analytic_ending: bool,
    pub use_tense_marker: bool,
    bare_igh: bool,
    pub with_ending: Option<bool>,
    pub dependent: Option<bool>,
    pub text: String,
    pub text_with_t_ending: Option<String>,
    pub text_with_ni: Option<String>,
    pub dialects: DialectSet<Irish>,
    pub preferred_in_dialects: DialectSet<Irish>,
    pub dep_preferred_in_dialects: Option<DialectSet<Irish>>,
    pub ending_preferred_in_dialects: DialectSet<Irish>,
    pub resists_lenition: bool,
    pub contains_do: bool,
    pub except: Vec<String>,
    contains_rel_particle: bool,
    conjugation: Option<Conjugation>,
}

impl IrishVerbForm {
    pub fn new(
        analytic: bool,
        no_analytic_ending: bool,
        use_tense_marker: bool,
        past_particles: bool,
        resists_lenition: bool,
        contains_do: bool,
        dependent: Option<bool>,
        text: String,
        text_with_t_ending: Option<String>,
        text_with_ni: Option<String>,
        dialects: DialectSet<Irish>,
        preferred_in_dialects: DialectSet<Irish>,
        dep_preferred_in_dialects: Option<DialectSet<Irish>>,
        with_ending: Option<bool>,
        except: Vec<String>,
        conjugation: Option<Conjugation>,
    ) -> Self {
        Self {
            analytic,
            no_analytic_ending,
            bare_igh: false,
            use_tense_marker,
            past_particles,
            resists_lenition,
            contains_do,
            dependent,
            text,
            text_with_t_ending,
            text_with_ni,
            dialects,
            preferred_in_dialects,
            dep_preferred_in_dialects,
            ending_preferred_in_dialects: DialectSet::empty(),
            with_ending,
            except,
            contains_rel_particle: true,
            conjugation,
        }
    }
}

impl IrishVerbForm {
    fn get_text(&self) -> String {
        self.text.clone()
    }
}

struct IrishVerbEnding {
    text: String,
    tense_marker: Option<&'static str>,
    analytic: bool,
    resists_lenition: bool,
    ignore_no_analytic_ending: bool,
    after_vowel: Option<bool>,
    dialects: DialectSet<Irish>,
    preferred_in_dialects: DialectSet<Irish>,
}

impl IrishVerbEnding {
    fn new(text: &str, analytic: bool, dialects: &[Dialect]) -> Self {
        Self {
            text: text.to_string(),
            tense_marker: None,
            analytic,
            resists_lenition: false,
            ignore_no_analytic_ending: false,
            after_vowel: None,
            dialects: dialects.into(),
            preferred_in_dialects: DialectSet::empty(),
        }
    }

    fn after_vowel(mut self, value: bool) -> Self {
        self.after_vowel = Some(value);
        self
    }

    fn resists_lenition(mut self) -> Self {
        self.resists_lenition = true;
        self
    }

    fn tense_marker(mut self, tense_marker: &'static str) -> Self {
        self.tense_marker = Some(tense_marker);
        self
    }

    fn ignore_no_analytic_ending(mut self) -> Self {
        self.ignore_no_analytic_ending = true;
        self
    }

    fn preferred(mut self, dialects: &[Dialect]) -> Self {
        self.preferred_in_dialects = dialects.into();
        self
    }
}

const CONNACHT_ULSTER: &[Dialect] = &[Dialect::Connacht, Dialect::Ulster];
const CONNACHT_ULSTER_SCHOOL: &[Dialect] = &[Dialect::Connacht, Dialect::Ulster, Dialect::SchoolCO];

lazy_static! {
    static ref ENDINGS_FIRST: HashMap<&'static str, Vec<IrishVerbEnding>> = hashmap!{
        "pres-1sg" => vec![ IrishVerbEnding::new("im", false, &[]) ],
        "pres-2sg" => vec![ IrishVerbEnding::new("ann", true, &[]) ],
        "pres-3sg" => vec![ IrishVerbEnding::new("ann", true, &[]) ],
        "pres-1pl" => vec![ IrishVerbEnding::new("ann", true, CONNACHT_ULSTER),
                            IrishVerbEnding::new("imid", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("imíd", false, &[Dialect::MunsterD]) ],
        "pres-2pl" => vec![ IrishVerbEnding::new("ann", true, &[]) ],
        "pres-3pl" => vec![ IrishVerbEnding::new("id", false, &[Dialect::MunsterD]),
                            IrishVerbEnding::new("id", true, &[Dialect::MunsterD]).ignore_no_analytic_ending(),
                            IrishVerbEnding::new("ann", true, &[]) ],
        "pres-sb"  => vec![ IrishVerbEnding::new("tar", false, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("tar", false, &[Dialect::UlsterD]).after_vowel(false),
                            IrishVerbEnding::new("thar", false, &[Dialect::UlsterD]).after_vowel(true) ],
        "pres-rel" => vec![ IrishVerbEnding::new("as", true, &[Dialect::Ulster]).ignore_no_analytic_ending(),
                            IrishVerbEnding::new("anns", true, &[Dialect::ConnachtD]),
                            IrishVerbEnding::new("ann", true, &[]) ],

        "past-1sg" => vec![ IrishVerbEnding::new("", true, CONNACHT_ULSTER_SCHOOL),
                            IrishVerbEnding::new("as", false, &[Dialect::Munster]) ],
        "past-2sg" => vec![ IrishVerbEnding::new("", true, CONNACHT_ULSTER_SCHOOL),
                            IrishVerbEnding::new("is", false, &[Dialect::Munster]) ],
        "past-3sg" => vec![ IrishVerbEnding::new("", true, &[]) ],
        "past-1pl" => vec![ IrishVerbEnding::new("", true, CONNACHT_ULSTER),
                            IrishVerbEnding::new("amar", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("amair", false, &[Dialect::MunsterD]) ],
        "past-2pl" => vec![ IrishVerbEnding::new("", true, &[Dialect::Connacht, Dialect::Ulster, Dialect::MunsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("abhair", false, &[Dialect::MunsterD]) ],
        "past-3pl" => vec![ IrishVerbEnding::new("", true, CONNACHT_ULSTER_SCHOOL),
                            IrishVerbEnding::new("adar", false, &[Dialect::Munster, Dialect::ConnachtD]).preferred(&[Dialect::ConnachtD]) ],
        "past-sb"  => vec![ IrishVerbEnding::new("adh", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::Ulster, Dialect::SchoolCO]).resists_lenition(),
                            IrishVerbEnding::new("adh", false, &[Dialect::MunsterD]) ],

        "fut-1sg" => vec![ IrishVerbEnding::new("idh", true, &[Dialect::Connacht, Dialect::Ulster, Dialect::MunsterCO, Dialect::SchoolCO]).tense_marker("f"),
                           IrishVerbEnding::new("ad", false, &[Dialect::MunsterD]).tense_marker("f") ],
        "fut-2sg" => vec![ IrishVerbEnding::new("idh", true, &[Dialect::Connacht, Dialect::Ulster, Dialect::MunsterCO, Dialect::SchoolCO]).tense_marker("f"),
                           IrishVerbEnding::new("ir", false, &[Dialect::MunsterD]).tense_marker("f") ],
        "fut-3sg" => vec![ IrishVerbEnding::new("idh", true, &[]).tense_marker("f") ],
        "fut-1pl" => vec![ IrishVerbEnding::new("idh", true, CONNACHT_ULSTER).tense_marker("f"),
                           IrishVerbEnding::new("imid", false, &[Dialect::MunsterCO, Dialect::SchoolCO]).tense_marker("f"),
                           IrishVerbEnding::new("am", false, &[Dialect::MunsterD]).tense_marker("f"),
                           IrishVerbEnding::new("imíd", false, &[Dialect::MunsterD]).tense_marker("f").preferred(&[Dialect::MunsterD]) ],
        "fut-2pl" => vec![ IrishVerbEnding::new("idh", true, &[]).tense_marker("f") ],
        "fut-3pl" => vec![ IrishVerbEnding::new("idh", true, &[Dialect::Connacht, Dialect::Ulster, Dialect::MunsterCO, Dialect::SchoolCO]).tense_marker("f"),
                           IrishVerbEnding::new("id", false, &[Dialect::MunsterD]).tense_marker("f").preferred(&[Dialect::MunsterD]),
                           IrishVerbEnding::new("id", true, &[Dialect::MunsterD]).tense_marker("f") ],
        "fut-sb"  => vec![ IrishVerbEnding::new("far", false, &[])],
        "fut-rel" => vec![ IrishVerbEnding::new("as", true, &[Dialect::Connacht, Dialect::Ulster]).tense_marker("f"),
                           IrishVerbEnding::new("idh", true, &[]).tense_marker("f") ],

        "cond-1sg" => vec![ IrishVerbEnding::new("inn", false, &[]).tense_marker("f"), ],
        "cond-2sg" => vec![ IrishVerbEnding::new("fá", false, &[]), ],
        "cond-3sg" => vec![ IrishVerbEnding::new("adh", true, &[]).tense_marker("f") ],
        "cond-1pl" => vec![ IrishVerbEnding::new("adh", true, CONNACHT_ULSTER).tense_marker("f"),
                            IrishVerbEnding::new("imis", false, &[Dialect::MunsterCO, Dialect::SchoolCO]).tense_marker("f"),
                            IrishVerbEnding::new("imist", false, &[Dialect::UlsterD]).tense_marker("f").preferred(&[Dialect::UlsterD]),
                            IrishVerbEnding::new("imís", false, &[Dialect::MunsterD]).tense_marker("f"),
                            IrishVerbEnding::new("imíst", false, &[Dialect::MunsterD]).tense_marker("f").preferred(&[Dialect::MunsterD]),
                            IrishVerbEnding::new("as", true, &[Dialect::UlsterD]).tense_marker("f") ],
        "cond-2pl" => vec![ IrishVerbEnding::new("adh", true, &[]).tense_marker("f") ],
        "cond-3pl" => vec![ IrishVerbEnding::new("adh", true, &[Dialect::ConnachtD, Dialect::UlsterD]).tense_marker("f"),
                            IrishVerbEnding::new("idís", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]).tense_marker("f").preferred(&[Dialect::ConnachtD]),
                            IrishVerbEnding::new("idíst", false, &[Dialect::MunsterD]).tense_marker("f").preferred(&[Dialect::MunsterD]) ],
        "cond-sb" => vec![ IrishVerbEnding::new("fí", false, &[])],

        "hab-past-1sg" => vec![ IrishVerbEnding::new("inn", false, &[]), ],
        "hab-past-2sg" => vec![ IrishVerbEnding::new("tá", false, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                IrishVerbEnding::new("tá", false, &[Dialect::UlsterD]).after_vowel(false),
                                IrishVerbEnding::new("thá", false, &[Dialect::UlsterD]).after_vowel(true) ],
        "hab-past-3sg" => vec![ IrishVerbEnding::new("adh", true, &[]) ],
        "hab-past-1pl" => vec![ IrishVerbEnding::new("imist", false, &[Dialect::UlsterD]).preferred(&[Dialect::Ulster]),
                                IrishVerbEnding::new("imis", false, &[Dialect::MunsterCO, Dialect::SchoolCO, Dialect::Ulster]),
                                IrishVerbEnding::new("amais", false, &[Dialect::UlsterD]),
                                IrishVerbEnding::new("adh", true, CONNACHT_ULSTER),
                                IrishVerbEnding::new("imíst", false, &[Dialect::MunsterD]) ],
        "hab-past-2pl" => vec![ IrishVerbEnding::new("adh", true, &[]) ],
        "hab-past-3pl" => vec![ IrishVerbEnding::new("idís", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]).preferred(&[Dialect::ConnachtD]),
                                IrishVerbEnding::new("adh", true, &[Dialect::ConnachtD, Dialect::UlsterD]),
                                IrishVerbEnding::new("idíst", false, &[Dialect::MunsterD]).preferred(&[Dialect::MunsterD]) ],
        "hab-past-sb"  => vec![ IrishVerbEnding::new("tí", false, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                IrishVerbEnding::new("tí", false, &[Dialect::UlsterD]).after_vowel(false),
                                IrishVerbEnding::new("thí", false, &[Dialect::UlsterD]).after_vowel(true) ],

        "imp-1sg" => vec![ IrishVerbEnding::new("im", false, &[]) ],
        "imp-2sg" => vec![ IrishVerbEnding::new("", false, &[]) ],
        "imp-3sg" => vec![ IrishVerbEnding::new("adh", true, &[]) ],
        "imp-1pl" => vec![ IrishVerbEnding::new("imis", false, &[Dialect::MunsterCO, Dialect::ConnachtCO, Dialect::Ulster, Dialect::SchoolCO]),
                           IrishVerbEnding::new("adh", true, &[Dialect::ConnachtD]),
                           IrishVerbEnding::new("imist", false, &[Dialect::UlsterD]).preferred(&[Dialect::Ulster]),
                           IrishVerbEnding::new("imíst", false, &[Dialect::MunsterD]) ],
        "imp-2pl" => vec![ IrishVerbEnding::new("idh", false, &[Dialect::ConnachtD]).preferred(&[Dialect::ConnachtD]),
                           IrishVerbEnding::new("igí", false, &[]),
                           IrishVerbEnding::new("íg", false, &[Dialect::MunsterD]).preferred(&[Dialect::MunsterD]) ],
        "imp-3pl" => vec![ IrishVerbEnding::new("idíst", false, &[Dialect::MunsterD]).preferred(&[Dialect::MunsterD]),
                           IrishVerbEnding::new("idís", false, &[Dialect::MunsterD, Dialect::UlsterCO, Dialect::Connacht, Dialect::SchoolCO]).preferred(&[Dialect::Connacht]),
                           IrishVerbEnding::new("adh", true, &[Dialect::UlsterD, Dialect::ConnachtD]) ],
        "imp-sb"  => vec![ IrishVerbEnding::new("tar", false, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("tar", false, &[Dialect::UlsterD]).after_vowel(false),
                           IrishVerbEnding::new("thar", false, &[Dialect::UlsterD]).after_vowel(true) ],

        "pres-subj-1sg" => vec![ IrishVerbEnding::new("@", true, &[Dialect::Connacht, Dialect::MunsterCO, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("idh", true, &[Dialect::UlsterD]),
                                 IrishVerbEnding::new("ad", false, &[Dialect::MunsterD]) ],
        "pres-subj-2sg" => vec![ IrishVerbEnding::new("@", true, &[Dialect::Connacht, Dialect::MunsterCO, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("idh", true, &[Dialect::UlsterD]),
                                 IrishVerbEnding::new("ir", false, &[Dialect::MunsterD]) ],
        "pres-subj-3sg" => vec![ IrishVerbEnding::new("@", true, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("idh", true, &[Dialect::UlsterD]) ],
        "pres-subj-1pl" => vec![ IrishVerbEnding::new("imid", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("imíd", false, &[Dialect::MunsterD]),
                                 IrishVerbEnding::new("@", true, &[Dialect::Connacht, Dialect::UlsterCO]),
                                 IrishVerbEnding::new("idh", true, &[Dialect::UlsterD]) ],
        "pres-subj-2pl" => vec![ IrishVerbEnding::new("@", true, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("idh", true, &[Dialect::UlsterD]) ],
        "pres-subj-3pl" => vec![ IrishVerbEnding::new("@", true, &[Dialect::Connacht, Dialect::MunsterCO, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("idh", true, &[Dialect::UlsterD]),
                                 IrishVerbEnding::new("id", false, &[Dialect::MunsterD]) ],
        "pres-subj-sb"  => vec![ IrishVerbEnding::new("tar", false, &[]) ],

        "past-subj-1sg" => vec![ IrishVerbEnding::new("inn", false, &[]) ],
        "past-subj-2sg" => vec![ IrishVerbEnding::new("tá", false, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("tá", false, &[Dialect::UlsterD]).after_vowel(false),
                                 IrishVerbEnding::new("thá", false, &[Dialect::UlsterD]).after_vowel(true) ],
        "past-subj-3sg" => vec![ IrishVerbEnding::new("adh", true, &[]) ],
        "past-subj-1pl" => vec![ IrishVerbEnding::new("adh", true, CONNACHT_ULSTER),
                                 IrishVerbEnding::new("imis", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("imist", false, &[Dialect::UlsterD]).preferred(&[Dialect::UlsterD]),
                                 IrishVerbEnding::new("imíst", false, &[Dialect::MunsterD]) ],
        "past-subj-2pl" => vec![ IrishVerbEnding::new("adh", true, &[]) ],
        "past-subj-3pl" => vec![ IrishVerbEnding::new("idís", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]).preferred(&[Dialect::ConnachtD]),
                                 IrishVerbEnding::new("adh", true, &[Dialect::ConnachtD, Dialect::UlsterD]),
                                 IrishVerbEnding::new("idíst", false, &[Dialect::MunsterD]) ],
        "past-subj-sb"  => vec![ IrishVerbEnding::new("tí", false, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("tí", false, &[Dialect::UlsterD]).after_vowel(false),
                                 IrishVerbEnding::new("thí", false, &[Dialect::UlsterD]).after_vowel(true) ],
    };
    static ref ENDINGS_SECOND: HashMap<&'static str, Vec<IrishVerbEnding>> = hashmap!{
        "pres-1sg" => vec![ IrishVerbEnding::new("%ím", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%im", false, &[Dialect::UlsterD]) ],
        "pres-2sg" => vec![ IrishVerbEnding::new("%íonn", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%ann", true, &[Dialect::UlsterD]) ],
        "pres-3sg" => vec![ IrishVerbEnding::new("%íonn", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%ann", true, &[Dialect::UlsterD]) ],
        "pres-1pl" => vec![ IrishVerbEnding::new("%íonn", true, &[Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann]),
                            IrishVerbEnding::new("%ann", true, &[Dialect::UlsterD]),
                            IrishVerbEnding::new("%ímid", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%ímíd", false, &[Dialect::MunsterD]) ],
        "pres-2pl" => vec![ IrishVerbEnding::new("%íonn", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%ann", true, &[Dialect::UlsterD]) ],
        "pres-3pl" => vec![ IrishVerbEnding::new("%íonn", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%ann", true, &[Dialect::UlsterD]),
                            IrishVerbEnding::new("%íd", false, &[Dialect::MunsterD]),
                            IrishVerbEnding::new("%íd", true, &[Dialect::MunsterD]) ],
        "pres-sb"  => vec![ IrishVerbEnding::new("%ítear", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%ithear", false, &[Dialect::Teileann]),
                            IrishVerbEnding::new("%tar", false, &[Dialect::UlsterD]).after_vowel(false),
                            IrishVerbEnding::new("%thar", false, &[Dialect::UlsterD]).after_vowel(true),
                            IrishVerbEnding::new("%íotar", false, &[Dialect::MunsterD]) ],
        "pres-rel" => vec![ IrishVerbEnding::new("%íos", true, &[Dialect::UlsterCO, Dialect::Teileann]),
                            IrishVerbEnding::new("%as", true, &[Dialect::UlsterD]),
                            IrishVerbEnding::new("%íonns", true, &[Dialect::ConnachtD]),
                            IrishVerbEnding::new("%íonn", true, &[]) ],

        "past-1sg" => vec![ IrishVerbEnding::new("", true, CONNACHT_ULSTER_SCHOOL),
                            IrishVerbEnding::new("%íos", false, &[Dialect::Munster]) ],
        "past-2sg" => vec![ IrishVerbEnding::new("", true, CONNACHT_ULSTER_SCHOOL),
                            IrishVerbEnding::new("%ís", false, &[Dialect::Munster]) ],
        "past-3sg" => vec![ IrishVerbEnding::new("", true, &[]) ],
        "past-1pl" => vec![ IrishVerbEnding::new("", true, CONNACHT_ULSTER),
                            IrishVerbEnding::new("%íomar", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%íomair", false, &[Dialect::MunsterD]) ],
        "past-2pl" => vec![ IrishVerbEnding::new("", true, &[Dialect::Connacht, Dialect::Ulster, Dialect::MunsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%íobhair", false, &[Dialect::MunsterD]) ],
        "past-3pl" => vec![ IrishVerbEnding::new("", true, CONNACHT_ULSTER_SCHOOL),
                            IrishVerbEnding::new("%íodar", false, &[Dialect::Munster, Dialect::ConnachtD]) ],
        "past-sb"  => vec![ IrishVerbEnding::new("%íodh", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]).resists_lenition(),
                            IrishVerbEnding::new("%adh", false, &[Dialect::UlsterD]).resists_lenition(),
                            IrishVerbEnding::new("%íodh", false, &[Dialect::MunsterD]) ],

        "fut-1sg" => vec![ IrishVerbEnding::new("%óidh", true, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%óchaidh", true, &[Dialect::UlsterD]),
                           IrishVerbEnding::new("%ód", false, &[Dialect::MunsterD]) ],
        "fut-2sg" => vec![ IrishVerbEnding::new("%óidh", true, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%óchaidh", true, &[Dialect::UlsterD]),
                           IrishVerbEnding::new("%óir", false, &[Dialect::MunsterD]) ],
        "fut-3sg" => vec![ IrishVerbEnding::new("%óidh", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%óchaidh", true, &[Dialect::UlsterD]) ],
        "fut-1pl" => vec![ IrishVerbEnding::new("%óidh", true, &[Dialect::Connacht, Dialect::UlsterCO]),
                           IrishVerbEnding::new("%óchaidh", true, &[Dialect::UlsterD]),
                           IrishVerbEnding::new("%óimid", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%óm", false, &[Dialect::MunsterD]),
                           IrishVerbEnding::new("%óimíd", false, &[Dialect::MunsterD]) ],
        "fut-2pl" => vec![ IrishVerbEnding::new("%óidh", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%óchaidh", true, &[Dialect::UlsterD]) ],
        "fut-3pl" => vec![ IrishVerbEnding::new("%óidh", true, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%óchaidh", true, &[Dialect::UlsterD]),
                           IrishVerbEnding::new("%óid", false, &[Dialect::MunsterD]),
                           IrishVerbEnding::new("%óid", true, &[Dialect::MunsterD]) ],
        "fut-sb"  => vec![ IrishVerbEnding::new("%ófar", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%óchar", false, &[Dialect::UlsterD])],
        "fut-rel" => vec![ IrishVerbEnding::new("%ós", true, &[Dialect::Connacht, Dialect::UlsterCO]),
                           IrishVerbEnding::new("%óchas", true, &[Dialect::UlsterD]),
                           IrishVerbEnding::new("%óidh", true, &[]) ],

        "cond-1sg" => vec![ IrishVerbEnding::new("%óinn", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%óchainn", false, &[Dialect::UlsterD]), ],
        "cond-2sg" => vec![ IrishVerbEnding::new("%ófá", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%óchá", false, &[Dialect::UlsterD]), ],
        "cond-3sg" => vec![ IrishVerbEnding::new("%ódh", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%óchadh", true, &[Dialect::UlsterD]), ],
        "cond-1pl" => vec![ IrishVerbEnding::new("%ódh", true, &[Dialect::Connacht, Dialect::UlsterCO]),
                            IrishVerbEnding::new("%óchaimist", false, &[Dialect::UlsterD]).preferred(&[Dialect::UlsterD]),
                            IrishVerbEnding::new("%óchadh", true, &[Dialect::UlsterD]),
                            IrishVerbEnding::new("%óimis", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%óimíst", false, &[Dialect::MunsterD]) ],
        "cond-2pl" => vec![ IrishVerbEnding::new("%ódh", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%óchadh", true, &[Dialect::UlsterD]), ],
        "cond-3pl" => vec![ IrishVerbEnding::new("%óchadh", true, &[Dialect::UlsterD]),
                            IrishVerbEnding::new("%óidís", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]).preferred(&[Dialect::Connacht]),
                            IrishVerbEnding::new("%ódh", true, &[Dialect::ConnachtD]),
                            IrishVerbEnding::new("%óidíst", false, &[Dialect::MunsterD]) ],
        "cond-sb"  => vec![ IrishVerbEnding::new("%ófaí", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]),
                            IrishVerbEnding::new("%óchaí", false, &[Dialect::UlsterD]) ],

        "hab-past-1sg" => vec![ IrishVerbEnding::new("%ínn", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                IrishVerbEnding::new("%inn", false, &[Dialect::UlsterD]) ],
        "hab-past-2sg" => vec![ IrishVerbEnding::new("%íteá", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                IrishVerbEnding::new("%athá", false, &[Dialect::UlsterD]),
                                IrishVerbEnding::new("%íotá", false, &[Dialect::MunsterD]) ],
        "hab-past-3sg" => vec![ IrishVerbEnding::new("%íodh", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                IrishVerbEnding::new("%adh", true, &[Dialect::UlsterD]) ],
        "hab-past-1pl" => vec![ IrishVerbEnding::new("%ímist", false, &[Dialect::Teileann]),
                                IrishVerbEnding::new("%imist", false, &[Dialect::UlsterD]),
                                IrishVerbEnding::new("%ímis", false, &[Dialect::MunsterCO, Dialect::Ulster, Dialect::SchoolCO]),
                                IrishVerbEnding::new("%íodh", true, CONNACHT_ULSTER),
                                IrishVerbEnding::new("%íomais", false, &[Dialect::UlsterD]),
                                IrishVerbEnding::new("%ímíst", false, &[Dialect::MunsterD]) ],
        "hab-past-2pl" => vec![ IrishVerbEnding::new("%íodh", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                IrishVerbEnding::new("%adh", true, &[Dialect::UlsterD]) ],
        "hab-past-3pl" => vec![ IrishVerbEnding::new("%íodh", true, &[Dialect::ConnachtD, Dialect::Teileann]),
                                IrishVerbEnding::new("%adh", true, &[Dialect::UlsterD]),
                                IrishVerbEnding::new("%ídís", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]).preferred(&[Dialect::Connacht]),
                                IrishVerbEnding::new("%ídíst", false, &[Dialect::MunsterD]) ],
        "hab-past-sb"  => vec![ IrishVerbEnding::new("%ítí", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                IrishVerbEnding::new("%ití", false, &[Dialect::UlsterD]) ],

        "imp-1sg" => vec![ IrishVerbEnding::new("%ím", false, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%im", false, &[Dialect::UlsterD]) ],
        "imp-2sg" => vec![ IrishVerbEnding::new("", false, &[]) ],
        "imp-3sg" => vec![ IrishVerbEnding::new("%íodh", true, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%adh", true, &[Dialect::UlsterD]) ],
        "imp-1pl" => vec![ IrishVerbEnding::new("%ímis", false, &[Dialect::MunsterCO, Dialect::ConnachtCO, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%íodh", true, &[Dialect::ConnachtD]),
                           IrishVerbEnding::new("%ímist", false, &[Dialect::Teileann]),
                           IrishVerbEnding::new("%imist", false, &[Dialect::UlsterD]),
                           IrishVerbEnding::new("%ímíst", false, &[Dialect::MunsterD]) ],
        "imp-2pl" => vec![ IrishVerbEnding::new("%ígí", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%igí", false, &[Dialect::UlsterD]),
                           IrishVerbEnding::new("%íg", false, &[Dialect::MunsterD]).preferred(&[Dialect::MunsterD]) ],
        "imp-3pl" => vec![ IrishVerbEnding::new("%ídís", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]).preferred(&[Dialect::Connacht]),
                           IrishVerbEnding::new("%ídíst", false, &[Dialect::MunsterD]),
                           IrishVerbEnding::new("%íodh", true, &[Dialect::Teileann, Dialect::ConnachtD]),
                           IrishVerbEnding::new("%adh", true, &[Dialect::UlsterD]) ],
        "imp-sb"  => vec![ IrishVerbEnding::new("%ítear", false, &[Dialect::Connacht, Dialect::MunsterCO, Dialect::UlsterCO, Dialect::SchoolCO]),
                           IrishVerbEnding::new("%íotar", false, &[Dialect::MunsterD]),
                           IrishVerbEnding::new("%ithear", false, &[Dialect::Teileann]),
                           IrishVerbEnding::new("%tar", false, &[Dialect::UlsterD]).after_vowel(false),
                           IrishVerbEnding::new("%thar", false, &[Dialect::UlsterD]).after_vowel(true) ],

        "pres-subj-1sg" => vec![ IrishVerbEnding::new("%í", true, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%idh", true, &[Dialect::UlsterD]) ],
        "pres-subj-2sg" => vec![ IrishVerbEnding::new("%í", true, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%idh", true, &[Dialect::UlsterD]),
                                 IrishVerbEnding::new("%ír", false, &[Dialect::MunsterD]) ],
        "pres-subj-3sg" => vec![ IrishVerbEnding::new("%í", true, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%idh", true, &[Dialect::UlsterD]) ],
        "pres-subj-1pl" => vec![ IrishVerbEnding::new("%ímid", false, &[Dialect::MunsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%ímíd", false, &[Dialect::MunsterD]),
                                 IrishVerbEnding::new("%í", true, &[Dialect::Connacht, Dialect::UlsterCO]),
                                 IrishVerbEnding::new("%idh", true, &[Dialect::UlsterD]) ],
        "pres-subj-2pl" => vec![ IrishVerbEnding::new("%í", true, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%idh", true, &[Dialect::UlsterD]) ],
        "pres-subj-3pl" => vec![ IrishVerbEnding::new("%í", true, &[Dialect::Connacht, Dialect::Munster, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%idh", true, &[Dialect::UlsterD]),
                                 IrishVerbEnding::new("%íd", false, &[Dialect::MunsterD]) ],
        "pres-subj-sb"  => vec![ IrishVerbEnding::new("%ítear", false, &[Dialect::Connacht, Dialect::MunsterCO, Dialect::UlsterCO, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%íotar", false, &[Dialect::MunsterD]),
                                 IrishVerbEnding::new("%ithear", false, &[Dialect::Teileann]),
                                 IrishVerbEnding::new("%tar", false, &[Dialect::UlsterD]).after_vowel(false),
                                 IrishVerbEnding::new("%thar", false, &[Dialect::UlsterD]).after_vowel(true) ],

        "past-subj-1sg" => vec![ IrishVerbEnding::new("%ínn", false, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%inn", false, &[Dialect::UlsterD]) ],
        "past-subj-2sg" => vec![ IrishVerbEnding::new("%íteá", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%athá", false, &[Dialect::UlsterD]),
                                 IrishVerbEnding::new("%íotá", false, &[Dialect::MunsterD]) ],
        "past-subj-3sg" => vec![ IrishVerbEnding::new("%íodh", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%adh", true, &[Dialect::UlsterD]) ],
        "past-subj-1pl" => vec![ IrishVerbEnding::new("%íodh", true, &[Dialect::Connacht]),
                                 IrishVerbEnding::new("%íomais", false, &[Dialect::UlsterD]),
                                 IrishVerbEnding::new("%imist", false, &[Dialect::UlsterD]).preferred(&[Dialect::UlsterD]),
                                 IrishVerbEnding::new("%ímist", false, &[Dialect::Teileann]),
                                 IrishVerbEnding::new("%ímis", false, &[Dialect::MunsterCO, Dialect::Ulster, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%ímíst", false, &[Dialect::MunsterD]) ],
        "past-subj-2pl" => vec![ IrishVerbEnding::new("%íodh", true, &[Dialect::Munster, Dialect::Connacht, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%adh", true, &[Dialect::UlsterD]) ],
        "past-subj-3pl" => vec![ IrishVerbEnding::new("%ídís", false, &[Dialect::MunsterCO, Dialect::Connacht, Dialect::UlsterCO, Dialect::SchoolCO]).preferred(&[Dialect::Connacht]),
                                 IrishVerbEnding::new("%íodh", true, &[Dialect::ConnachtD, Dialect::Teileann]),
                                 IrishVerbEnding::new("%adh", true, &[Dialect::UlsterD]),
                                 IrishVerbEnding::new("%ídíst", false, &[Dialect::MunsterD]) ],
        "past-subj-sb"  => vec![ IrishVerbEnding::new("%ítí", false, &[Dialect::Connacht, Dialect::MunsterCO, Dialect::UlsterCO, Dialect::Teileann, Dialect::SchoolCO]),
                                 IrishVerbEnding::new("%íotaí", false, &[Dialect::MunsterD]),
                                 IrishVerbEnding::new("%ití", false, &[Dialect::UlsterD]) ],
    };
}

#[derive(Debug, Clone, PartialEq)]
pub struct IrishVerb {
    id: String,
    infl: Inflection<IrishVerbForm>,
    has_habitual_present: bool,
}

impl IrishVerb {
    pub fn new(
        id: Option<String>,
        conjugation: Option<Conjugation>,
        mut forms: HashMap<String, Vec<IrishVerbForm>>
    ) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("root".to_string(), &mut forms)?;
        let conjugation = conjugation.unwrap_or_else(|| Self::guess_conjugation(&infl));

        infl.add_from("vn", &mut forms, "root", |x| Self::guess_vn(x, conjugation));
        infl.add_from("va", &mut forms, "root", |x| Self::guess_va(x, conjugation));
        infl.add_from("stem", &mut forms, "root", |x| Self::guess_stem(x, conjugation));

        infl.add_from("present", &mut forms, "stem", |x| Self::copy_form(x, "present"));

        let mut has_habitual_present = true;
        infl.add("hab-pres", &mut forms).unwrap_or_else(|_| has_habitual_present = false);

        infl.add_from("past", &mut forms, "stem", |x| Self::copy_form(x, "past"));

        infl.add_from("fut", &mut forms, "stem", |x| Self::copy_form(x, "fut"));
        infl.add_from("cond", &mut forms, "fut", |x| Self::copy_form(x, "cond"));
        infl.add_from("hab-past", &mut forms, "stem", |x| Self::copy_form(x, "hab-past"));

        infl.add_from("imp", &mut forms, "stem", |x| Self::copy_form(x, "imp"));
        infl.add_from("pres-subj", &mut forms, "stem", |x| Self::copy_form(x, "pres-subj"));
        infl.add_from("past-subj", &mut forms, "stem", |x| Self::copy_form(x, "past-subj"));

        for t in Tense::values() {
            if t == Tense::HabitualPresent && !has_habitual_present {
                continue;
            }
            for n in Number::values() {
                for p in Person::values(true) {
                    let indep_id = Self::form_id(t, n, p);
                    let id = Self::form_id(t, n, p);
                    Self::add_form(
                        &mut infl,
                        &mut forms,
                        indep_id,
                        id,
                        &t.ext_id(),
                        conjugation
                    );
                }
            }
            for p in Person::values(false) {
                let indep_id = Self::form_id(t, Number::Singular, p);
                let id = Self::form_id(t, Number::Singular, p);
                Self::add_form(
                    &mut infl,
                    &mut forms,
                    indep_id,
                    id,
                    &t.ext_id(),
                    conjugation
                );
            }
        }

        infl.verify(forms)?;

        let id = match id {
            Some(x) => x,
            None => infl.get_id_form("root")?.get_text(),
        };

        Ok(Self {
            id,
            infl,
            has_habitual_present,
        })
    }

    fn add_form(
        infl: &mut Inflection<IrishVerbForm>,
        forms: &mut HashMap<String, Vec<IrishVerbForm>>,
        mut indep_id: String, id: String, base_id: &str,
        default_conjugation: Conjugation)
    {
        if indep_id.starts_with("hab-pres-") {
            indep_id.replace_range(..4, "");
        }

        infl.add_from(
            &id,
            forms,
            base_id,
            |form| Self::guess_form(&indep_id, &id, default_conjugation, form)
        );
    }

    fn copy_form(form: IrishVerbForm, id: &str) -> Vec<IrishVerbForm> {
        if form.except.iter().any(|x| x == id) {
            vec![]
        } else {
            vec![form]
        }
    }

    fn form_id(t: Tense, n: Number, p: Person) -> String {
        if p.has_number() {
            format!("{}-{}{}", t.id(), p.id(), n.id())
        } else {
            format!("{}-{}", t.id(), p.id())
        }
    }

    fn guess_form(
        indep_id: &str,
        id: &str,
        default_conjugation: Conjugation,
        mut stem: IrishVerbForm,
    ) -> Vec<IrishVerbForm> {
        if stem.except.iter().any(|x| x == id) {
            return vec![];
        }

        let endings_paradigm = &match stem.conjugation.unwrap_or(default_conjugation) {
            Conjugation::First => &*ENDINGS_FIRST,
            Conjugation::Second => &*ENDINGS_SECOND,
        };
        let endings = endings_paradigm.get(indep_id);

        let endings = if let Some(endings) = endings {
            endings
        } else if id.ends_with("-rel") {
            let mut id_3sg = id[..id.len() - 4].to_string();
            id_3sg.push_str("-3sg");
            stem.contains_rel_particle = false;
            endings_paradigm.get(id_3sg.as_str()).unwrap()
        } else if id.ends_with("-rel-dep") {
            let mut id_3sg = id[..id.len() - 8].to_string();
            id_3sg.push_str("-3sg");
            stem.contains_rel_particle = false;
            endings_paradigm.get(id_3sg.as_str()).unwrap()
        } else {
            panic!("Ending missing for '{}'", id);
        };

        let mut result = Vec::with_capacity(endings.len());
        for ending in endings {
            let dialects = &stem.dialects & &ending.dialects;
            if dialects.is_empty() {
                continue;
            }

            let apply_ending = !(stem.no_analytic_ending && ending.analytic && !ending.ignore_no_analytic_ending && id.starts_with("pres"));

            let is_t_form = ending.text.starts_with('t') || ending.text.starts_with("%t");
            let stem_text = if is_t_form && stem.text_with_t_ending.is_some() {
                stem.text_with_t_ending.as_ref().unwrap()
            } else {
                &stem.text
            };

            if let Some(with_ending) = stem.with_ending {
                let has_ending = !ending.text.is_empty() && apply_ending;
                if with_ending != has_ending {
                    continue;
                }
            }
            if let Some(after_vowel) = ending.after_vowel {
                if let Some(c) = stem_text.chars().rev().next() {
                    if is_vowel(c) != after_vowel {
                        continue;
                    }
                }
            }

            // FIXME
            let mut form = stem.clone();
            if apply_ending {
                let tense_marker = if form.use_tense_marker { ending.tense_marker } else { None };
                form.text = add_ending(&stem_text, &ending.text, tense_marker);

                if let Some(text_with_ni) = form.text_with_ni {
                    form.text_with_ni = Some(add_ending(&text_with_ni, &ending.text, tense_marker));
                }
            }
            if ending.text.len() == 0 && form.bare_igh {
                form.text.push_str("igh");
            }
            form.contains_rel_particle = false;
            form.resists_lenition |= ending.resists_lenition;
            form.analytic = ending.analytic;
            form.dialects = dialects;
            form.ending_preferred_in_dialects = ending.preferred_in_dialects.clone();

            result.push(form);
        }
        result
    }

    fn guess_conjugation(infl: &Inflection<IrishVerbForm>) -> Conjugation {
        if let Some(form) = infl.iter("root").next() {
            if form.text.ends_with("áil") || form.text.count_syllables(is_vowel) == 1 {
                return Conjugation::First;
            }
        }

        Conjugation::Second
    }

    fn has_syncope(text: &String) -> bool {
        if text.count_syllables(is_vowel) < 2 {
            false
        } else if text.ends_with_multi(&["lis", "nis", "ris", "lais", "nais", "rais"]) {
            true
        } else if text.ends_with_multi(&["ail", "ain", "air"]) {
            !is_vowel(text.chars().rev().nth(3).unwrap_or('x'))
        } else if text.ends_with_multi(&["il", "in", "ir"]) {
            !is_vowel(text.chars().rev().nth(2).unwrap_or('x'))
        } else {
            false
        }
    }

    fn guess_stem(mut form: IrishVerbForm, default_conjugation: Conjugation) -> Vec<IrishVerbForm> {
        let conjugation = form.conjugation.unwrap_or(default_conjugation);

        if Self::has_syncope(&form.text) {
            let mut without_ending = form.clone();
            without_ending.with_ending = Some(false);

            if form.text_with_t_ending == None && conjugation == Conjugation::Second {
                form.text_with_t_ending = Some(depalatalise(&form.text));
            }

            let last = form.text.pop().unwrap();
            form.text.pop();
            if form.text.chars().last() == Some('a') {
                form.text.pop();
            }
            form.text.push(last);
            if conjugation == Conjugation::Second {
                if !has_slender_ending(&form.text) {
                    form.text.push('a');
                }
                form.text.push_str("igh");
            }
            form.with_ending = Some(true);
            return vec![form, without_ending];
        } else if form.text.ends_with("áil") {
            let mut without_ending = form.clone();
            without_ending.with_ending = Some(false);

            form.text_with_t_ending = Some(form.text.clone());
            form.text.pop();
            form.text.pop();
            form.text.push_str("l");

            form.with_ending = Some(true);
            return vec![form, without_ending];
        } else if form.text.ends_with("igh") {
            if "áéíóú".contains(form.text.chars().rev().nth(3).unwrap_or('x')) {
                let mut ulster_form = form.clone();
                let ulster_dialects = DialectSet::<Irish>::from(&[Dialect::Ulster][..]);
                ulster_form.dialects &= &ulster_dialects;
                form.dialects -= &ulster_dialects;
                form.bare_igh = true;
                form.text.pop();
                form.text.pop();
                form.text.pop();
                return vec![form, ulster_form];
            }

            if form.text_with_t_ending == None {
                let mut text_with_t_ending = form.text.clone();
                text_with_t_ending.pop();
                text_with_t_ending.pop();
                form.text_with_t_ending = Some(text_with_t_ending);
            }
        } else if conjugation == Conjugation::Second {
            let mut text_with_t_ending = form.text.clone();
            text_with_t_ending.push('i');
            form.text_with_t_ending = Some(text_with_t_ending);
        }

        vec![form]
    }

    fn guess_vn(mut form: IrishVerbForm, default_conjugation: Conjugation) -> Vec<IrishVerbForm> {
        if form.except.iter().any(|x| x == "vn") {
            return vec![];
        }

        if Self::has_syncope(&form.text) {
            form.text.push('t');
        } else if form.text.ends_with("áil") {
            /* remains unchanged */
        } else if form.text.ends_with_multi(&["áigh, éigh, óigh, úigh"]) {
            form.text.pop();
            form.text.pop();
            form.text.pop();
        } else if form.text.ends_with("igh") {
            match form.conjugation.unwrap_or(default_conjugation) {
                Conjugation::First => form.text.push_str("%í"),
                Conjugation::Second => form.text = add_ending(&form.text, "%ú", None),
            }
        } else {
            match form.conjugation.unwrap_or(default_conjugation) {
                Conjugation::First => form.text.push_str("adh"),
                Conjugation::Second => (),
            }
        }

        vec![form]
    }

    fn guess_va(mut form: IrishVerbForm, default_conjugation: Conjugation) -> Vec<IrishVerbForm> {
        if form.except.iter().any(|x| x == "va") {
            return vec![];
        }

        let (use_th, is_slender);
        match form.conjugation.unwrap_or(default_conjugation) {
            Conjugation::First => {
                use_th = form.text.ends_with_multi(&["b", "bh", "c", "f", "g", "m", "mh", "p", "r"]);
                is_slender = has_slender_ending(&form.text);
            },
            Conjugation::Second => {
                use_th = form.text.ends_with_multi(&["igh", "im", "ing", "ir"]);
                is_slender = !form.text.ends_with_multi(&["in", "im", "ir"]);
                if !is_slender {
                    form.text = depalatalise(&form.text);
                } else if form.text.ends_with("igh") {
                    form.text.pop();
                    form.text.pop();
                }
            },
        };

        let ending = match (use_th, is_slender) {
            (false, false) => "ta",
            (false, true) => "%te",
            (true, false) => "tha",
            (true, true) => "%the",
        };
        form.text = add_ending(&form.text, ending, None);

        vec![form]
    }

    // FIXME Move to JSON file, make it accessible here
    fn create_do() -> Arc<VerbalParticle> {
        let mut forms: HashMap<String, Vec<VerbalParticleForm>> = HashMap::new();
        let form = vec![
            VerbalParticleForm {
                text: "".to_owned(),
                text_before_vowel: Some("d'".to_owned()),
                dialects: [Dialect::SchoolCO, Dialect::Connacht, Dialect::Ulster].as_ref().into(),
                mutation: Mutation::Lenition,
            },
            VerbalParticleForm {
                text: "(do)".to_owned(),
                text_before_vowel: Some("dh'".to_owned()),
                dialects: [Dialect::MunsterD].as_ref().into(),
                mutation: Mutation::Lenition,
            },
        ];
        forms.insert("present".to_owned(), form.clone());
        forms.insert("past".to_owned(), form);

        let form = vec![
            VerbalParticleForm {
                text: "".to_owned(),
                text_before_vowel: Some("d'".to_owned()),
                dialects: [Dialect::SchoolCO, Dialect::Connacht, Dialect::Ulster].as_ref().into(),
                mutation: Mutation::Lenition,
            },
            VerbalParticleForm {
                text: "(do)".to_owned(),
                text_before_vowel: None,
                dialects: [Dialect::MunsterD].as_ref().into(),
                mutation: Mutation::None,
            },
        ];
        forms.insert("present-sb".to_owned(), form);

        let form = vec![
            VerbalParticleForm {
                text: "".to_owned(),
                text_before_vowel: None,
                dialects: [Dialect::SchoolCO, Dialect::MunsterCO, Dialect::ConnachtCO, Dialect::UlsterCO].as_ref().into(),
                mutation: Mutation::None,
            },
            VerbalParticleForm {
                text: "".to_owned(),
                text_before_vowel: None,
                dialects: [Dialect::ConnachtD, Dialect::UlsterD].as_ref().into(),
                mutation: Mutation::HPrefix,
            },
            VerbalParticleForm {
                text: "(do)".to_owned(),
                text_before_vowel: Some("dh'".to_owned()),
                dialects: [Dialect::MunsterD].as_ref().into(),
                // XXX Should this be HPrefix?
                mutation: Mutation::LenitionExceptF,
            },
        ];
        forms.insert("past-sb".to_owned(), form);

        let form = vec![
            VerbalParticleForm {
                text: "".to_owned(),
                text_before_vowel: Some("d'".to_owned()),
                dialects: [Dialect::SchoolCO, Dialect::Connacht, Dialect::Ulster].as_ref().into(),
                mutation: Mutation::Lenition,
            },
            VerbalParticleForm {
                text: "".to_owned(),
                text_before_vowel: Some("dh'".to_owned()),
                dialects: [Dialect::MunsterD].as_ref().into(),
                mutation: Mutation::Lenition,
            },
        ];
        forms.insert("present-rel".to_owned(), form.clone());
        forms.insert("past-rel".to_owned(), form);

        Arc::new(VerbalParticle::new(forms, false).unwrap())
    }

    pub fn inflect(
        &self,
        mut t: Tense,
        mut i: AgreementInfo,
        particle: &Option<Arc<VerbalParticle>>,
    ) -> TranslationSet<Irish> {
        if t == Tense::HabitualPresent && !self.has_habitual_present {
            t = Tense::Present;
        }

        let (dependent, ni);
        if let Some(p) = particle {
            dependent = p.dependent_form && t.has_dependent_forms();
            ni = p.lemma().iter().any(|x| x == "ní");
        } else {
            dependent = false;
            ni = false;
        }

        let id = Self::form_id(t, i.number, i.person);

        let mut need_do_particle = match t {
            Tense::Past | Tense::HabitualPast | Tense::Conditional => true,
            _ => false,
        };
        if let Some(p) = particle {
            if p.dependent_form {
                need_do_particle = false;
            }
        }

        let do_particle;
        let do_particle_forms = if need_do_particle {
            do_particle = Some(Self::create_do());
            do_particle.as_ref().map(|x| x.inflect(t, i))
        } else {
            do_particle = None;
            None
        };

        let mut ts = TranslationSet::new();

        for v in self.infl.iter(&id) {

            let do_particle_it: Box<dyn Iterator<Item=_>> = if !v.contains_do && do_particle_forms.is_some() {
                Box::new(do_particle_forms.as_ref().unwrap().iter().map(|x| Some(x)))
            } else {
                Box::new(vec![None].into_iter())
            };

            for do_p in do_particle_it {
                if let Some(v_dep) = v.dependent {
                    if v_dep != dependent {
                        continue;
                    }
                }

                let verb_form = if ni && v.text_with_ni.is_some() {
                    v.text_with_ni.as_ref().unwrap()
                } else {
                    &v.text
                };

                let particle_forms = if let Some(p) = particle {
                    if p.is_direct_relative_particle() && v.contains_rel_particle {
                        None
                    } else {
                        let particle_tense = if !v.past_particles && t == Tense::Past {
                            Tense::Present
                        } else {
                            t
                        };
                        Some(p.inflect(particle_tense, i))
                    }
                } else {
                    None
                };

                for p in particle_forms.into_iter_or_none() {
                    let mutated: String;
                    let text = if let Some(f) = do_p {
                        let mutation = if v.resists_lenition && f.mutation.is_lenition() {
                            Mutation::None
                        } else {
                            f.mutation
                        };
                        mutated = mutation.apply(&verb_form);
                        &mutated
                    } else if let Some(f) = p {
                        let mutation = if v.resists_lenition && f.mutation.is_lenition() {
                            Mutation::None
                        } else {
                            f.mutation
                        };
                        mutated = mutation.apply(&verb_form);
                        &mutated
                    } else {
                        &verb_form
                    };

                    let preferred_in_dialects = if dependent && v.dep_preferred_in_dialects.is_some() {
                        v.dep_preferred_in_dialects.as_ref().unwrap()
                    } else {
                        &v.preferred_in_dialects
                    };

                    i.analytic_verb = v.analytic;
                    let verb_t = Arc::new({
                        let mut t = Translation::from_string_full(
                            text,
                            Some(self.id.clone()),
                            false,
                            false,
                            i,
                            &v.dialects,
                            |s: &str| format!("The form '{}'", s),
                            preferred_in_dialects,
                            &v.ending_preferred_in_dialects,
                        );
                        t.add_word_tag(format!("verb-{}", id.clone()));
                        t
                    });

                    let mut parts: Vec<&dyn TranslationGenerator<Irish>> = Vec::new();
                    let (particle_t, do_particle_t);

                    if !(ni && v.text_with_ni.is_some()) {
                        if let Some(f) = &p {
                            particle_t = f.translation(particle.as_ref().unwrap(), has_vocalic_start(&text), i);
                            parts.push(&particle_t);
                        }
                    }
                    if let Some(f) = &do_p {
                        do_particle_t = f.translation(do_particle.as_ref().unwrap(), has_vocalic_start(&text), i);
                        parts.push(&do_particle_t);
                    }
                    parts.push(&verb_t);
                    ts.append(Translation::combine(&parts, verb_t.agreement));
                }
            }
        }

        ts
    }

    pub fn vn(&self) -> TranslationSet<Irish> {
        let i = AgreementInfo {
            analytic_verb: true,
            ..Default::default()
        };
        self.infl
            .iter("vn")
            .map(|x| {
                let mut t = Translation::from_string_full(
                    &x.text,
                    Some(self.id.clone()),
                    false,
                    false,
                    i,
                    &x.dialects,
                    |s: &str| format!("The form '{}'", s),
                    &x.preferred_in_dialects,
                    &x.ending_preferred_in_dialects,
                );
                t.add_word_tag("verb-vn".to_owned());
                t
            })
            .collect()
    }

    pub fn va(&self) -> TranslationSet<Irish> {
        let i = AgreementInfo::default();
        self.infl
            .iter("va")
            .map(|x| Translation::from_dialect_form(&x.text, self.id(), i, &x.dialects))
            .collect()
    }

    pub fn has_habitual_present(&self) -> bool {
        self.has_habitual_present
    }
}

impl Word for IrishVerb {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("root")
            .map(|x| x.get_text())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::Ga
    }
}
