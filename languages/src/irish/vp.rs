use crate::{ConstituentJson, TranslationContext};
use crate::translation::{IterOrNone, Translation, TranslationGenerator, TranslationSet};
use serde::Deserialize;
use std::sync::Arc;
use super::{DialectSet, Irish, Mutation};
use super::categories::{AgreementInfo, Number, Person, Tense};
use super::np::NP;
use super::pp::PP;
use super::verb::IrishVerb;
use super::verbalparticle::VerbalParticle;

#[derive(Debug, Clone, Copy, PartialEq, Deserialize)]
#[serde(rename_all="UPPERCASE")]
pub enum VPType {
    Dictionary,
    Statement,
    #[serde(rename="VN_A")]
    VerbalNounA,
    #[serde(rename="VN_GEN")]
    VerbalNounGen,
}

impl VPType {
    fn is_sentence(&self) -> bool {
        match self {
            Self::Statement => true,
            _ => false,
        }
    }

    fn tag(&self) -> &str {
        match self {
            VPType::Dictionary      => "vp-dict",
            VPType::Statement       => "vp-decl",
            /*VPType::Question        => "vp-question",
            VPType::Relative        => "vp-relative",*/
            VPType::VerbalNounA     => "vp-vn-a",
            VPType::VerbalNounGen   => "vp-vn-genitive",
        }
    }
}

pub struct VP {
    particle: Option<Arc<VerbalParticle>>,
    verb: Arc<IrishVerb>,
    tense: Tense,
    subject: Option<NP>,
    object: Option<NP>,
    comp: Option<Box<VP>>,
    adverbial: Vec<Arc<PP>>,
    agreement: AgreementInfo,
    vptype: Option<VPType>,
    neg: bool,
    dictionary: bool,
    is_sentence: Option<bool>,
    post: Vec<ConstituentJson>,
}

impl VP {
    pub fn new(verb: Arc<IrishVerb>, tense: Tense, neg: bool) -> Self {
        Self {
            particle: None,
            verb,
            tense,
            subject: None,
            object: None,
            comp: None,
            adverbial: Vec::new(),
            agreement: AgreementInfo::default(),
            vptype: None,
            neg,
            dictionary: false,
            is_sentence: None,
            post: Vec::new(),
        }
    }

    pub fn subject(mut self, subject: NP) -> Self {
        self.subject = Some(subject);
        self
    }

    pub fn object(mut self, object: NP) -> Self {
        self.object = Some(object);
        self
    }

    pub fn comp(mut self, mut comp: VP) -> Self {
        if comp.vptype.is_none() {
            comp.vptype = Some(VPType::VerbalNounA);
        }
        self.comp = Some(Box::new(comp));
        self
    }

    pub fn particle(mut self, particle: Arc<VerbalParticle>) -> Self {
        self.particle = Some(particle);
        self
    }

    pub fn agreement(mut self, i: AgreementInfo) -> Self {
        self.agreement = i;
        self
    }

    pub fn adverbial(mut self, adverbial: PP) -> Self {
        self.adverbial.push(Arc::new(adverbial));
        self
    }

    pub fn dictionary(mut self) -> Self {
        self.dictionary = true;
        self
    }

    pub fn sentence(mut self, is_sentence: bool) -> Self {
        self.is_sentence = Some(is_sentence);
        self
    }

    fn is_sentence(&self) -> bool {
        if let Some(is_sentence) = self.is_sentence {
            is_sentence
        } else {
            self.get_vptype().is_sentence()
        }
    }

    pub fn vptype(mut self, vptype: VPType) -> Self {
        self.vptype = Some(vptype);
        self
    }

    pub fn post(mut self, post: Vec<ConstituentJson>) -> Self {
        self.post = post;
        self
    }

    pub fn get_agreement(&self) -> AgreementInfo {
        self.agreement
    }

    pub fn get_vptype(&self) -> VPType {
        if let Some(vptype) = self.vptype {
            return vptype;
        }

        let implicit_subj = match self.agreement.person {
            Person::Autonomous | Person::Relative => true,
            _ => false,
        };

        if (self.subject.is_some() || implicit_subj) && !self.dictionary {
            VPType::Statement
        } else if self.subject.is_some() || self.object.is_some() {
            VPType::VerbalNounA
        } else {
            VPType::Dictionary
        }
    }

    pub fn generate(&self, ctx: &TranslationContext) -> TranslationSet<Irish> {
        let vptype = self.get_vptype();
        let subject = self.subject.as_ref().map(|x| x.generate(ctx));
        let object = self.object.as_ref().map(|x| x.generate(ctx));
        let comp = self.comp.as_ref().map(|x| x.generate(ctx));
        let adverbial = self.adverbial.iter().map(|x| x.generate(ctx)).collect::<Vec<_>>();

        let particle = if self.particle.is_some() {
            assert!(!self.neg);
            self.particle.clone()
        } else if self.neg {
            Some(VerbalParticle::from_str(&ctx.ld, "ní").unwrap())
        } else {
            None
        };

        let mut res = TranslationSet::new();
        for s in subject.into_iter_or_none() {
            let agreement = if let Some(s) = &s {
                s.agreement
            } else {
                self.agreement
            };

            let verb = match vptype {
                VPType::Statement => self.verb.inflect(self.tense, agreement, &particle),
                VPType::Dictionary => {
                    self.verb.inflect(
                        Tense::Imperative,
                        AgreementInfo {
                            person: Person::Second,
                            number: Number::Singular,
                            ..Default::default()
                        },
                        &particle,
                    )
                },
                VPType::VerbalNounA | VPType::VerbalNounGen => self.verb.vn(),
            };

            for mut v in verb {
                let mut parts: Vec<&dyn TranslationGenerator<Irish>> = Vec::new();
                if vptype != VPType::VerbalNounA {
                    parts.push(&v);
                } else {
                    let t = Arc::make_mut(&mut v);
                    if let Some(tp) = t.parts.get_mut(0) {
                        tp.text = Mutation::Lenition.apply(&tp.text);
                    }
                }

                if v.agreement.analytic_verb {
                    if let Some(s) = &s {
                        parts.push(s);
                    }
                } else if self.subject.as_ref().map_or(false, |x| !x.allow_synthetic_form()) {
                    continue;
                }
                if let Some(o) = &object {
                    parts.push(o);
                };

                let a_part;
                if vptype == VPType::VerbalNounA {
                    a_part = Arc::new(Translation::from_string_unreferenced("a", v.agreement));
                    parts.push(&a_part);
                    parts.push(&v);
                }
                for a in &adverbial {
                    parts.push(a);
                }
                if let Some(c) = &comp {
                    parts.push(c);
                };

                let post_ts_vec;
                if !self.post.is_empty() {
                    post_ts_vec = ConstituentJson::generate_post(&self.post, ctx, self.dictionary);
                    for post_ts in post_ts_vec.iter() {
                        parts.push(post_ts);
                    }
                }

                let punctuation;
                if vptype == VPType::Statement && self.is_sentence() {
                    punctuation = Arc::new(Translation::from_string_contraction(
                        ".",
                        None,
                        true,
                        false,
                        AgreementInfo::default(),
                        &DialectSet::all(),
                        |_| unreachable!(),
                    ));
                    parts.push(&punctuation);
                }

                res.append(Translation::combine_full(&parts, v.agreement, self.is_sentence()));
            }
        }

        res = res.map(|t| {
            t.add_expression_tag(vptype.tag().to_owned());
            if self.neg {
                t.add_expression_tag("vp-neg".to_owned());
            }
        });

        res
    }
}
