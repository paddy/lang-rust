use crate::{ParseError, Word};
use crate::{Inflection, LanguageCode};
use crate::translation::{Translation, TranslationGenerator, TranslationSet};
use std::collections::HashMap;
use super::categories::{AgreementInfo, Gender, Number, Person};
use super::{Dialect, DialectSet, Irish, Mutation};

#[derive(Debug, Clone, PartialEq)]
pub struct PrepositionForm {
    pub text: String,
    pub analytic: bool,
    pub dialects: DialectSet<Irish>,
    pub mutation: Mutation,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Preposition {
    id: String,
    infl: Inflection<PrepositionForm>,
}

impl Preposition {
    pub fn new(
        mut forms: HashMap<String, Vec<PrepositionForm>>,
    ) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("word".to_string(), &mut forms)?;
        for form in ["1sg", "2sg", "3sg-m", "3sg-f", "1pl", "2pl", "3pl", "art-sg"] {
            infl.add_or_copy(form, &mut forms, "word");
        }
        infl.add_from("art-sg", &mut forms, "word", Self::guess_art_sg);

        infl.verify(forms)?;

        Ok(Self {
            id: infl.get_id_form("word")?.text.clone(),
            infl,
        })
    }

    fn guess_art_sg(form: PrepositionForm) -> Vec<PrepositionForm> {
        let mut form_lenition = form.clone();
        let mut form_eclipsis = form;

        form_lenition.dialects &= &[Dialect::Ulster].as_ref().into();
        form_lenition.mutation = Mutation::LenitionTs;

        form_eclipsis.dialects &= &[Dialect::Connacht, Dialect::Munster, Dialect::SchoolCO].as_ref().into();
        form_eclipsis.mutation = Mutation::Eclipsis;

        vec![
            form_lenition,
            form_eclipsis,
        ]
    }

    pub fn inflect(
        &self,
        article: bool,
        allow_synthetic: bool,
        mut i: AgreementInfo,
    ) -> TranslationSet<Irish> {
        let id_str;
        let id = if article {
            if i.number == Number::Singular { "art-sg" } else { "word" }
        } else if !allow_synthetic {
            "word"
        } else if i.number == Number::Singular && i.person == Person::Third {
            match i.gender {
                Gender::Masc => "3sg-m",
                Gender::Fem => "3sg-f",
            }
        } else {
            id_str = format!("{}{}", i.person.id(), i.number.id());
            &id_str
        };

        self.infl
            .iter(&id)
            .map(|x| {
                if id == "art-sg" {
                    i.dat_mutation = x.mutation;
                    Translation::from_string(&x.text, self.id(), i, &x.dialects, |s| {
                        format!("{:?} after '{} an'", x.mutation, s)
                    })
                } else {
                    Translation::from_dialect_form(&x.text, self.id(), i, &x.dialects)
                }
            })
            .collect()
    }
}

impl TranslationGenerator<Irish> for Preposition {
    fn generate(&self) -> TranslationSet<Irish> {
        self.inflect(false, false, AgreementInfo::default())
    }
}

impl Word for Preposition {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("word")
            .map(|x| x.text.clone())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::Ga
    }
}
