use crate::{ParseError, Word};
use crate::LanguageCode;
use crate::translation::{Translation, TranslationGenerator, TranslationSet};
use std::sync::Arc;
use super::categories::AgreementInfo;
use super::{DialectSet, Irish};

#[derive(Debug, Clone, PartialEq)]
pub struct Particle {
    word: String,
}

impl Particle {
    pub fn new(word: String) -> Result<Self, ParseError> {
        Ok(Self {
            word
        })
    }
}

impl Word for Particle {
    fn id(&self) -> String {
        self.word.clone()
    }

    fn lemma(&self) -> Vec<String> {
        vec![self.word.clone()]
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::Ga
    }
}

pub struct Par {
    particle: Arc<Particle>,
}

impl Par {
    pub fn new(particle: Arc<Particle>) -> Self {
        Self {
            particle,
        }
    }
}

impl TranslationGenerator<Irish> for Par {
    fn generate(&self) -> TranslationSet<Irish> {
        Translation::from_dialect_form(
            &self.particle.word,
            self.particle.id(),
            AgreementInfo::default(),
            &DialectSet::all(),
        ).into()
    }
}
