use crate::{ParseError, Word};
use crate::{Inflection, LanguageCode};
use crate::translation::{Translation, TranslationSet};
use std::collections::HashMap;
use super::{DialectSet, Irish};
use super::categories::{AgreementInfo, Case, Number, Gender, Person};

#[derive(Debug, Clone, PartialEq)]
pub struct PronounForm {
    pub text: String,
    pub dialects: DialectSet<Irish>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Pronoun {
    id: String,
    infl: Inflection<PronounForm>,
    person: Person,
    number: Number,
    gender: Gender,
}

impl Pronoun {
    fn form_id(&self, subject_form: bool, copula_form: bool, emphatic: bool) -> String {
        let is_2sg = self.person == Person::Second && self.number == Number::Singular;

        let id = if !subject_form || (copula_form && !is_2sg) { "obj" } else { "subj" };
        let mut id = id.to_owned();

        if emphatic {
            id.push_str("-emph");
        }

        id
    }

    pub fn inflect(
        &self,
        subject_form: bool,
        copula_form: bool,
        emphatic: bool
    ) -> TranslationSet<Irish> {
        let id = self.form_id(subject_form, copula_form, emphatic);
        let i = AgreementInfo {
            person: self.person,
            number: self.number,
            gender: self.gender,
            case: Case::Nominative,
            ..Default::default()
        };
        self.infl
            .iter(&id)
            .map(|x| Translation::from_dialect_form(&x.text, self.id(), i, &x.dialects))
            .collect()
    }

    pub fn new(
        person: Person, number: Number, gender: Option<Gender>,
        mut forms: HashMap<String, Vec<PronounForm>>
    ) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("subj".to_string(), &mut forms)?;
        infl.add("subj-emph".to_string(), &mut forms)?;

        infl.add_or_copy("obj", &mut forms, "subj");
        infl.add_or_copy("obj-emph", &mut forms, "subj-emph");

        infl.verify(forms)?;

        Ok(Self {
            id: infl.get_id_form("subj")?.text.clone(),
            infl,
            person,
            number,
            gender: gender.unwrap_or(Gender::Masc),
        })
    }

    pub fn agreement(&self) -> AgreementInfo {
        AgreementInfo {
            person: self.person,
            number: self.number,
            gender: self.gender,
            case: Case::Nominative,
            ..Default::default()
        }
    }
}

impl Word for Pronoun {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("subj")
            .map(|x| x.text.clone())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::Ga
    }
}
