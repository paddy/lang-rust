use crate::{ParseError, Word};
use crate::{Inflection, LanguageCode};
use crate::translation::{Translation, TranslationGenerator, TranslationSet};
use crate::strext::StrExt;
use std::collections::HashMap;
use std::sync::Arc;
use super::{depalatalise, has_slender_ending, is_vowel, lenite, palatalise, syncopate};
use super::{DialectSet, Irish};
use super::categories::{AgreementInfo, Case, Gender, Number};

#[derive(Debug, Clone, PartialEq)]
pub enum Declension {
    First,
    Second,
    Third,
}

#[derive(Debug, Clone, PartialEq)]
pub struct AdjectiveForm {
    pub text: String,
    pub dialects: DialectSet<Irish>,
    pub declension: Declension,
}

impl AdjectiveForm {
    pub fn new(text: String, dialects: DialectSet<Irish>) -> Self {
        let last_letter = text.chars().rev().nth(0).unwrap_or('x');
        let declension = if text.ends_with_multi(&["úil", "ir"]) {
            Declension::Second
        } else if is_vowel(last_letter) {
            Declension::Third
        } else {
            Declension::First
        };

        Self {
            text,
            dialects,
            declension,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Adjective {
    id: String,
    infl: Inflection<AdjectiveForm>,
    dialects: DialectSet<Irish>,
}

impl Adjective {
    fn form_id(c: Case, n: Number, g: Gender) -> String {
        format!("{}-{}-{}", c.id(), n.id(), g.id())
    }

    pub fn new(
        mut forms: HashMap<String, Vec<AdjectiveForm>>,
        dialects: DialectSet<Irish>,
    ) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("root".to_string(), &mut forms)?;

        for c in Case::values() {
            for n in Number::values() {
                if c == Case::Genitive && n == Number::Plural {
                    continue
                }
                for g in Gender::values() {
                    let id = Self::form_id(c, n, g);
                    infl.add_from(
                        &id,
                        &mut forms,
                        "root",
                        |form| Self::guess_form(c, n, g, form)
                    );
                }
            }
        }

        infl.verify(forms)?;

        Ok(Self {
            id: infl.get_id_form("root")?.text.clone(),
            infl,
            dialects,
        })
    }

    fn guess_form(
        case: Case,
        number: Number,
        gender: Gender,
        mut form: AdjectiveForm
    ) -> Vec<AdjectiveForm> {
        if case == Case::Genitive && number == Number::Singular {
            match form.declension {
                Declension::First => {
                    if gender == Gender::Fem {
                        form.text = syncopate(&form.text);
                    }
                    if !has_slender_ending(&form.text) {
                        form.text = palatalise(&form.text);
                    }
                    if gender == Gender::Fem {
                        form.text.push('e');
                    }
                }
                Declension::Second => {
                    if gender == Gender::Fem {
                        form.text = depalatalise(&syncopate(&form.text));
                        form.text.push('a');
                    }
                }
                Declension::Third => (),
            };
            if form.text.ends_with("ighe") {
                form.text.pop();
                form.text.pop();
                form.text.pop();
                form.text.pop();
                form.text.push_str("í");
            }
        } else if case == Case::Nominative && number == Number::Plural {
            match form.declension {
                Declension::First => {
                    form.text = syncopate(&form.text);
                    if has_slender_ending(&form.text) {
                        form.text.push('e');
                    } else {
                        form.text.push('a');
                    }
                }
                Declension::Second => {
                    form.text = depalatalise(&syncopate(&form.text));
                    form.text.push('a');
                }
                Declension::Third => (),
            };
        }

        vec![form]
    }

    pub fn inflect(
        &self,
        mut case: Case,
        mut number: Number,
        gender: Gender,
        definite: bool,
        strong_plural: bool,
        slender_ending: bool,
    ) -> TranslationSet<Irish> {
        if case == Case::Genitive && number == Number::Plural {
            case = Case::Nominative;
            if !strong_plural {
                number = Number::Singular;
            }
        }

        let id = Self::form_id(case, number, gender);
        let tag = if case == Case::Dative && number == Number::Singular {
            format!("adj-{}-{}", if definite { "def" } else { "indef" }, id)
        } else  {
            format!("adj-{}", id)
        };

        let i = AgreementInfo {
            number,
            gender,
            ..Default::default()
        };
        self.infl
            .iter(&id)
            .map(|x| {
                // TODO dative (only after article)
                // TODO plural after slender consonant
                let lenited;
                let text = if ["nom-sg-f", "gen-sg-m"].contains(&id.as_str()) {
                    lenited = lenite(&x.text);
                    &lenited
                } else if id.starts_with("nom-pl-") && slender_ending {
                    lenited = lenite(&x.text);
                    &lenited
                } else {
                    &x.text
                };

                let mut t = Translation::from_dialect_form(text, self.id(), i, &x.dialects);
                t.add_dialect_constraint(&self.dialects, |t| format!("The word '{}'", t));
                t.add_word_tag(tag.clone());
                t
            })
            .collect()
    }
}

impl Word for Adjective {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("root")
            .map(|x| x.text.clone())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::Ga
    }
}

pub struct AP {
    adjective: Arc<Adjective>,
}

impl AP {
    pub fn new(adjective: Arc<Adjective>) -> Self {
        Self {
            adjective,
        }
    }

    pub fn inflect(
        &self,
        case: Case,
        number: Number,
        gender: Gender,
        definite: bool,
        strong_plural: bool,
        slender_ending: bool,
    ) -> TranslationSet<Irish> {
        self.adjective.inflect(case, number, gender, definite, strong_plural, slender_ending)
    }
}

impl TranslationGenerator<Irish> for AP {
    fn generate(&self) -> TranslationSet<Irish> {
        self.adjective.inflect(
            Case::Nominative,
            Number::Singular,
            Gender::Masc,
            false,
            false,
            false,
        )
    }
}
