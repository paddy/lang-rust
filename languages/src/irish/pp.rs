use crate::TranslationContext;
use crate::translation::{TranslationGenerator, TranslationSet, Translation};
use std::sync::Arc;
use super::Irish;
use super::categories::AgreementInfo;
use super::np::NP;
use super::preposition::Preposition;

pub struct PP {
    prep: Arc<Preposition>,
    object: NP,
}

impl PP {
    pub fn new(prep: Arc<Preposition>, object: NP) -> Self {
        Self {
            prep,
            object,
        }
    }

    pub fn generate(&self, ctx: &TranslationContext) -> TranslationSet<Irish> {
        if self.object.allow_synthetic_form() {
            return self.prep.inflect(false, true, self.object.agreement());
        }

        let prep_ts = self.prep.inflect(self.object.has_article(), false, self.object.agreement());
        let mut translations = TranslationSet::new();

        for p in prep_ts {
            let obj_ts = self.object.generate_with_prep(ctx, p.agreement.dat_mutation);
            for t in obj_ts {
                let parts: [&dyn TranslationGenerator<Irish>; 2] = [
                    &p,
                    &t,
                ];
                translations.append(Translation::combine(&parts, AgreementInfo::default()));
            }
        }

        translations
    }
}

