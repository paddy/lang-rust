use crate::{ParseError, Word};
use crate::{DictWord, Inflection, LanguageCode, LanguageData};
use crate::translation::Translation;
use std::collections::HashMap;
use std::sync::Arc;
use super::categories::{AgreementInfo, Person, Tense};
use super::{DialectSet, Irish, Mutation};
use super::external::IrishWord;

#[derive(Debug, Clone, PartialEq)]
pub struct VerbalParticleForm {
    pub text: String,
    pub text_before_vowel: Option<String>,
    pub dialects: DialectSet<Irish>,
    pub mutation: Mutation,
}

impl VerbalParticleForm {
    fn get_text(&self) -> String {
        self.text.clone()
    }

    pub fn translation(&self, word: &VerbalParticle, before_vowel: bool, i: AgreementInfo) -> Arc<Translation<Irish>> {
        let text = if before_vowel && self.text_before_vowel.is_some() {
            self.text_before_vowel.as_ref().unwrap()
        } else {
            &self.text
        };
        Arc::new(Translation::from_string_contraction(
            text,
            Some(word.id()),
            false,
            text.ends_with("'"),
            i,
            &self.dialects,
            |s: &str| format!("The form '{}'", s)
        ))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct VerbalParticle {
    id: String,
    infl: Inflection<VerbalParticleForm>,
    pub dependent_form: bool,
}

impl VerbalParticle {
    pub fn new(
        mut forms: HashMap<String, Vec<VerbalParticleForm>>,
        dependent_form: bool,
    ) -> Result<Self, ParseError> {
        let mut infl = Inflection::new();

        infl.add("present".to_string(), &mut forms)?;
        infl.add_or_copy("present-sb", &mut forms, "present");
        infl.add_or_copy("present-rel", &mut forms, "present");
        infl.add_or_copy("past", &mut forms, "present");
        infl.add_or_copy("past-sb", &mut forms, "past");
        infl.add_or_copy("past-rel", &mut forms, "past");

        infl.verify(forms)?;

        Ok(Self {
            id: infl.get_id_form("present")?.get_text(),
            infl,
            dependent_form,
        })
    }

    pub fn from_str(ld: &LanguageData, s: &str) -> Option<Arc<VerbalParticle>> {
        match &*ld.get_word(s).ok()? {
            DictWord::Ga(IrishWord::IrishVerbalParticle(p)) => Some(p.clone()),
            _ => None
        }
    }

    pub fn inflect(
        &self,
        t: Tense,
        i: AgreementInfo,
    ) -> Vec<&VerbalParticleForm> {
        let id = match t {
            Tense::Past => match i.person {
                Person::Autonomous => "past-sb",
                Person::Relative => "past-rel",
                _ =>  "past",
            },
            _ => match i.person {
                Person::Autonomous => "present-sb",
                Person::Relative => "present-rel",
                _ => "present",
            },
        };

        self.infl.iter(&id).collect()
    }

    pub fn is_direct_relative_particle(&self) -> bool {
        // FIXME
        self.lemma().iter().any(|x| x == "a")
    }
}

impl Word for VerbalParticle {
    fn id(&self) -> String {
        self.id.clone()
    }

    fn lemma(&self) -> Vec<String> {
        self.infl
            .iter("present")
            .map(|x| x.get_text())
            .collect()
    }

    fn language(&self) -> LanguageCode {
        LanguageCode::Ga
    }
}
