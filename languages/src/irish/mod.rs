use crate::{self as languages, DialectSet, Language, LanguageCode};
use crate::translation::Translation;
use crate::strext::StrExt;
use maplit::hashset;
use serde::{Deserialize, Serialize};
use std::any::Any;
use std::collections::HashSet;

pub mod categories;
pub mod external;

pub mod adjective;
pub mod noun;
pub mod particle;
pub mod preposition;
pub mod pronoun;
pub mod verb;
pub mod verbalparticle;

pub mod copp;
pub mod np;
pub mod vp;
pub mod pp;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Irish;
impl Language for Irish {
    type Dialect = Dialect;
    type AgreementInfo = categories::AgreementInfo;
    const CODE: LanguageCode = LanguageCode::Ga;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="SCREAMING_SNAKE_CASE")]
pub enum Dialect {
    Munster,
    Connacht,
    Ulster,
    #[serde(rename="MUNSTER_CO")]
    MunsterCO,
    #[serde(rename="CONNACHT_CO")]
    ConnachtCO,
    #[serde(rename="ULSTER_CO")]
    UlsterCO,
    #[serde(rename="SCHOOL_CO")]
    SchoolCO,
    MunsterD,
    ConnachtD,
    UlsterD,
    Teileann,
}

impl languages::Dialect<Irish> for Dialect {
    fn subdialects(&self) -> HashSet<Self> {
        match self {
            Dialect::Munster => hashset! { Dialect::MunsterCO, Dialect::MunsterD },
            Dialect::Connacht => hashset! { Dialect::ConnachtCO, Dialect::ConnachtD },
            Dialect::Ulster =>  hashset! { Dialect::UlsterCO, Dialect::UlsterD },
            Dialect::UlsterD => hashset! { Dialect::Teileann },
            _ => HashSet::new(),
        }
    }

    fn all() -> HashSet<Self> {
        hashset!{
            Dialect::Munster,
            Dialect::Connacht,
            Dialect::Ulster,
            Dialect::MunsterCO,
            Dialect::ConnachtCO,
            Dialect::UlsterCO,
            Dialect::SchoolCO,
            Dialect::MunsterD,
            Dialect::ConnachtD,
            Dialect::UlsterD,
            Dialect::Teileann,
        }
    }

    fn name(&self) -> &str {
        match self {
            Dialect::Munster => "Munster",
            Dialect::Connacht => "Connacht",
            Dialect::Ulster => "Ulster",
            Dialect::MunsterCO => "Munster (standard)",
            Dialect::ConnachtCO => "Connacht (standard)",
            Dialect::UlsterCO => "Ulster (standard)",
            Dialect::SchoolCO => "School Irish",
            Dialect::MunsterD => "Munster (dialect)",
            Dialect::ConnachtD => "Connacht (dialect)",
            Dialect::UlsterD => "Ulster (dialect)",
            Dialect::Teileann => "Teileann",
        }
    }
}

impl languages::AnyDialect for Dialect {
    fn language(&self) -> LanguageCode {
        Irish::CODE
    }

    fn name(&self) -> &str {
        languages::Dialect::<Irish>::name(self)
    }

    fn str_translation(&self, text: &str) -> Box<dyn languages::Translation> {
        Box::new(Translation::<Irish>::from_dialect_string(text, &[*self][..].into()))
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all="SCREAMING_SNAKE_CASE")]
pub enum Mutation {
    None,
    Lenition,
    LenitionTs,
    LenitionExceptF,
    Eclipsis,
    EclipsisWithoutVowels,
    HPrefix,
    TPrefix,
}

impl Mutation {
    pub fn apply(&self, word: &str) -> String {
        match self {
            Mutation::None => word.to_owned(),
            Mutation::Lenition => lenite(word),
            Mutation::LenitionTs => {
                if word.starts_with("s") {
                    let mut result = word.to_owned();
                    result.insert(0, 't');
                    result
                } else {
                    lenite(word)
                }
            },
            Mutation::LenitionExceptF => {
                if word.starts_with("f") { word.to_owned() } else { lenite(word) }
            },
            Mutation::Eclipsis => eclipse(word, true),
            Mutation::EclipsisWithoutVowels => eclipse(word, false),
            Mutation::HPrefix => {
                let mut result = word.to_owned();
                if is_vowel(word.chars().next().unwrap_or('x')) {
                    result.insert(0, 'h');
                }
                result
            }
            Mutation::TPrefix => {
                let mut result = word.to_owned();
                let c = word.chars().next().unwrap_or('x');
                if is_vowel(c) {
                    if c.is_uppercase() {
                        result.insert(0, 't');
                    } else {
                        result.insert_str(0, "t-");
                    }
                }
                result
            }
        }
    }

    pub fn update_translation(&self, t: &mut Translation<Irish>) {
        if let Some(part) = t.parts.get_mut(0) {
            part.text = self.apply(&part.text);
        }
    }

    pub fn is_lenition(&self) -> bool {
        match self {
            Mutation::Lenition | Mutation::LenitionExceptF => true,
            _ => false,
        }
    }
}

fn is_vowel(c: char) -> bool {
    "aeiouáéíóúAEIOUÁÉÍÓÚ".contains(c)
}

fn split_last_vowel_group(word: &str) -> (&str, &str, &str) {
    let mut it = word.char_indices().rev();
    let mut last_vowel = None;
    let mut prev = word.len();
    let mut after_last_vowel = prev;

    while let Some((i, c)) = it.next() {
        if is_vowel(c) {
            after_last_vowel = prev;
            last_vowel = Some(i);
            break;
        }
        prev = i;
    }

    let last_vowel = if let Some(i) = last_vowel {
        i
    } else {
        return ("", "", word);
    };

    let mut first_vowel = last_vowel;
    prev = last_vowel;
    while let Some((i, c)) = it.next() {
        if !is_vowel(c) {
            first_vowel = prev;
            break;
        }
        prev = i;
    }

    let (first_part, end) = word.split_at(after_last_vowel);
    let (start, mid) = first_part.split_at(first_vowel);

    (start, mid, end)
}

fn syncopate(word: &str) -> String {
    if word.count_syllables(is_vowel) < 2 {
        return word.to_owned()
    }

    let (c1, v, c2) = split_last_vowel_group(word);
    if v.chars().any(|x| "áéíóú".contains(x)) || !["l", "n", "r", "s"].contains(&c2) {
        return word.to_owned();
    }

    if c2 == "s" && !c1.ends_with_multi(&["l", "n", "r"]) {
        return word.to_owned();
    }

    c1.to_owned() + c2
}

fn palatalise(word: &str) -> String {
    let (c1, v, c2) = split_last_vowel_group(word);

    if word.count_syllables(is_vowel) > 1 {
        if word.ends_with("each") {
            return c1.to_owned() + "igh";
        } else if word.ends_with("ach") {
            return c1.to_owned() + "aigh";
        } else if word.ends_with("íoch") {
            return c1.to_owned() + "ígh";
        }
    }

    let mut slender_marker = "";
    let v = match v {
        "ea" => "i",
        "éa" => "éi",
        "ia" => "éi",
        "io" => "i",
        "ío" => "í",
        "iu" => "i",
        ""   => "",
        _ => {
            if !has_slender_ending(v) {
                slender_marker = "i";
            }
            v
        }
    };

    c1.to_owned() + v + slender_marker + c2
}

fn depalatalise(word: &str) -> String {
    let (c1, v, c2) = split_last_vowel_group(word);

    let v = match v {
        "ei" => "ea",
        "éi" => "éa",
        "i"  => "ea",
        "io" => "ea",
        "í"  => "ío",
        "ui" => "o",
        ""   => "",
        _ => v.strip_suffix("i").unwrap_or(v),
    };

    c1.to_owned() + v + c2
}

fn has_slender_start_it<I: Iterator<Item = char>>(it: I) -> bool {
    for c in it {
        if "eiéíEIÉÍ".contains(c) {
            return true;
        } else if "aouáóúAOUÁOÚ".contains(c) {
            return false;
        }
    }
    false
}

fn has_slender_start(s: &str) -> bool {
    has_slender_start_it(s.chars())
}

fn has_slender_ending(s: &str) -> bool {
    has_slender_start_it(s.chars().rev())
}

fn has_slender_cons_ending(s: &str) -> bool {
    let last_letter = s.chars().rev().nth(0).unwrap_or('x');
    !is_vowel(last_letter) && has_slender_ending(s)
}

fn has_vocalic_start(s: &str) -> bool {
    let mut it = s.chars();

    if let Some(c) = it.next() {
        if is_vowel(c) {
            return true;
        } else if c == 'f' && it.next() == Some('h') {
            if let Some(c) = it.next() {
                return is_vowel(c) || c == 'r' || c == 'l';
            }
        }
    }
    false
}

fn lenite(word: &str) -> String {
    let mut lenited = String::new();
    let mut chars = word.chars();

    if let Some(c) = chars.next() {
        lenited.push(c);
        let next = chars.next();
        if let Some(n) = next {
            if n != 'h' && ("bcdfgmpt".contains(c) || (c == 's' && !"cfmpt".contains(n))) {
                lenited.push('h');
            }
            lenited.push(n);
        }
    }
    lenited.push_str(chars.as_str());
    lenited
}

fn eclipse(word: &str, vowels: bool) -> String {
    let mut eclipsed = String::new();
    let mut chars = word.chars();

    if let Some(c) = chars.next() {
        match c {
            'b' => eclipsed.push('m'),
            'c' => eclipsed.push('g'),
            'd' => eclipsed.push('n'),
            'f' => eclipsed.push_str("bh"),
            'g' => eclipsed.push('n'),
            'p' => eclipsed.push('b'),
            't' => eclipsed.push('d'),
            _ => if vowels && is_vowel(c) { eclipsed.push_str("n-") },
        };
        eclipsed.push(c);
        if let Some(c) = chars.next() {
            // Eclipsing overrides lenition
            if c != 'h' {
                eclipsed.push(c);
            }
        }
    }
    eclipsed.push_str(chars.as_str());
    eclipsed
}


fn add_ending(mut word: &str, mut ending: &str, tense_marker: Option<&str>) -> String {
    if ending.is_empty() {
        return word.to_string();
    }

    if ending.starts_with('%') {
        ending = &ending[1..];
        if word.ends_with("igh") {
            word = &word[..word.len() - 3]
        }
    }

    let mut ending = if let Some(t) = tense_marker {
        t.to_string() + ending
    } else {
        ending.to_string()
    };

    // -t/-th + -t- leaves a single -t-
    if ending.starts_with('t') {
        if word.ends_with('t') {
            word = &word[..word.len() - 1];
        } else if word.ends_with("th") {
            word = &word[..word.len() - 2];
        }
    }

    // -bh/-mb + -th- results in -f-
    if ending.starts_with("th") && word.ends_with_multi(&["mh", "bh"]) {
        word = &word[..word.len() - 2];
        ending = "f".to_string() + &ending[2..];
    }

    let word_slender = has_slender_ending(word);
    let ending_slender = has_slender_start(&ending);

    let ending_first_letter = ending.chars().nth(0).unwrap_or('x');
    let word_last_letter = word.chars().rev().nth(0).unwrap_or('x');

    if is_vowel(ending_first_letter) {
        if is_vowel(word_last_letter) {
            if "áéíóú".contains(ending_first_letter) && !"áéíóú".contains(word_last_letter) && word_slender == ending_slender {
                let mut chars = word.chars();
                chars.next_back();
                word = chars.as_str();
            } else if word_slender == ending_slender && !"eé".contains(word_last_letter) {
                ending.remove(0);
            } else if "ií".contains(word_last_letter) && ending_first_letter == 'a' {
                ending = "o".to_string() + &ending[1..];
            }
        } else if word_slender != ending_slender {
            // e + ó = eo
            if ending_first_letter == 'ó' {
                ending = 'o'.to_string() + &ending["ó".len()..];
            }
            ending = if ending_slender {
                "a"
            } else if "uú".contains(ending_first_letter) {
                "i"
            } else {
                "e"
            }.to_string() + &ending;
        }
    } else if ending == "@" {
        ending.clear();
        if !is_vowel(word_last_letter) {
            ending.push(if word_slender { 'e' } else { 'a' });
        }
    } else {
        let mut vowel_offset = 0;
        let mut it = ending.char_indices();
        while let Some((i, c)) = it.next() {
            if is_vowel(c) {
                vowel_offset = i;
                break;
            }
        }
        assert!(vowel_offset != 0);

        if word_slender != ending_slender {
            ending.insert(vowel_offset, if ending_slender { 'a' } else { 'e' });
        }
        if "eé".contains(word_last_letter) {
            ending.insert(0, 'i');
        }
    }

    word.to_string() + &ending
}
