use crate::TranslationContext;
use crate::translation::{Translation, TranslationGenerator, TranslationSet};
use std::sync::Arc;
use super::{DialectSet, Irish, Mutation};
use super::categories::{AgreementInfo, Case, Definiteness, Gender, Number, Person};
use super::adjective::AP;
use super::noun::Noun;
use super::pronoun::Pronoun;
use super::vp::VP;

enum NPHead {
    Noun {
        noun: Arc<Noun>,
        case: Case,
        number: Number,
        definite: Definiteness,
        attributes: Vec<AP>,
    },
    NP {
        np: Arc<NP>,
        attributes: Vec<AP>,
    },
    Pronoun {
        pronoun: Arc<Pronoun>,
        emphatic: bool,
    },
    VP {
        vp: Arc<VP>,
    },
}

pub struct NP {
    noun: NPHead,
    dialects: DialectSet<Irish>,
}

impl NP {
    pub fn from_pronoun(pronoun: Arc<Pronoun>, emphatic: bool) -> Self {
        Self {
            noun: NPHead::Pronoun {
                pronoun,
                emphatic,
            },
            dialects: DialectSet::all(),
        }
    }

    pub fn from_np(np: Arc<NP>, attributes: Vec<AP>) -> Self {
        Self {
            noun: NPHead::NP { np, attributes },
            dialects: DialectSet::all(),
        }
    }

    pub fn from_noun(
        noun: Arc<Noun>,
        c: Case,
        n: Number,
        d: Definiteness,
        attributes: Vec<AP>
    ) -> Self {
        Self {
            noun: NPHead::Noun {
                noun,
                case: c,
                number: n,
                definite: d,
                attributes,
            },
            dialects: DialectSet::all(),
        }
    }

    pub fn from_vp(vp: VP) -> Self {
        Self {
            noun: NPHead::VP { vp: Arc::new(vp) },
            dialects: DialectSet::all(),
        }
    }

    pub fn set_dialects(&mut self, dialects: DialectSet<Irish>) {
        self.dialects = dialects;
    }

    pub fn allow_synthetic_form(&self) -> bool {
        match self.noun {
            NPHead::Pronoun { .. } => true,
            _ => false,
        }
    }

    pub fn is_pronoun(&self) -> bool {
        match self.noun {
            NPHead::Pronoun { .. } => true,
            _ => false,
        }
    }

    pub fn is_definite(&self) -> bool {
        self.has_article()
    }

    pub fn has_article(&self) -> bool {
        match self.noun {
            NPHead::Noun { definite, .. } => definite == Definiteness::Definite,
            NPHead::NP { ref np, .. } => np.has_article(),
            _ => false,
        }
    }

    pub fn agreement(&self) -> AgreementInfo {
        match &self.noun {
            NPHead::Noun { number, .. } => AgreementInfo {
                person: Person::Third,
                number: *number,
                ..Default::default()
            },
            NPHead::NP { np, .. } => np.agreement(),
            NPHead::Pronoun { pronoun, .. } => pronoun.agreement(),
            NPHead::VP { vp, .. } => vp.get_agreement(),
        }
    }

    fn add_article(
        &self,
        noun: &mut Arc<Translation<Irish>>,
        case: Case,
        agreement: AgreementInfo,
    ) -> TranslationSet<Irish> {
        // Tuples are: (lenition/ts, eclipsis, t-prefix)
        let article = match agreement.number {
            Number::Singular => match case {
                Case::Nominative => match agreement.gender {
                    Gender::Masc => ("an", Mutation::TPrefix),
                    Gender::Fem =>  ("an", Mutation::LenitionTs),
                },
                Case::Genitive => match agreement.gender {
                    Gender::Masc => ("an", Mutation::LenitionTs),
                    Gender::Fem =>  ("na", Mutation::HPrefix),
                },
                // TODO Gender
                Case::Dative => ("an", agreement.dat_mutation),
                Case::Vocative => panic!("There is no vocative article"),
            },
            Number::Plural => match case {
                Case::Nominative => match agreement.gender {
                    Gender::Masc => ("na", Mutation::HPrefix),
                    Gender::Fem =>  ("na", Mutation::HPrefix),
                },
                Case::Genitive => match agreement.gender {
                    Gender::Masc => ("na", Mutation::Eclipsis),
                    Gender::Fem =>  ("na", Mutation::Eclipsis),
                },
                Case::Dative => match agreement.gender {
                    Gender::Masc => ("na", Mutation::HPrefix),
                    Gender::Fem =>  ("na", Mutation::HPrefix),
                },
                Case::Vocative => panic!("There is no vocative article"),
            },
        };

        // Mutate the noun
        let noun = Arc::make_mut(noun);
        let noun_text = &mut noun.parts[0].text;
        *noun_text = article.1.apply(&noun_text);

        let mut t = Translation::from_string_unreferenced(
            article.0,
            agreement,
        );
        t.add_word_tag(format!(
            "art-{}-{}-{}",
            case.id(),
            agreement.number.id(),
            agreement.gender.id(),
        ));
        t.into()
    }

    fn generate_common(
        &self,
        ctx: &TranslationContext,
        dat_mutation: Mutation,
        subj_form: bool,
        cop_form: bool,
    ) -> TranslationSet<Irish> {
        let empty_attributes;
        let (np_head, add_article, attributes) = match &self.noun {
            NPHead::Noun { noun, case, number, definite, attributes } => {
                (noun.inflect(*case, *number, *definite), self.has_article(), attributes)
            }
            NPHead::NP { np, attributes } => {
                (np.generate_common(ctx, dat_mutation, subj_form, cop_form), false, attributes)
            }
            NPHead::Pronoun { pronoun, emphatic } => {
                empty_attributes = Vec::new();
                (pronoun.inflect(true, cop_form, *emphatic), false, &empty_attributes)
            }
            NPHead::VP { vp } => {
                empty_attributes = Vec::new();
                (vp.generate(ctx), false, &empty_attributes)
            }
        };

        let mut ts = TranslationSet::new();

        for mut noun in np_head {
            let mut parts: Vec<&dyn TranslationGenerator<Irish>> = Vec::new();
            let mut agreement = noun.agreement;
            let article;
            if add_article {
                agreement.dat_mutation = dat_mutation;
                article = self.add_article(&mut noun, agreement.case, agreement);
                parts.push(&article);
            }
            parts.push(&noun);

            let attributes: Vec<_> = attributes.iter().map(|x| {
                x.inflect(
                    agreement.case,
                    agreement.number,
                    agreement.gender,
                    self.has_article(),
                    false, // FIXME strong_plural
                    false, // FIXME slender_ending
                )
            }).collect();
            for a in &attributes {
                parts.push(a);
            }

            ts.append(Translation::combine(&parts, agreement));
        }

        ts.map(|t| t.add_dialect_constraint(&self.dialects, |t| format!("The expression '{}'", t)))
    }

    pub fn generate_copula(&self, ctx: &TranslationContext, subj: bool) -> TranslationSet<Irish> {
        self.generate_common(ctx, Mutation::None, subj, true)
    }

    pub fn generate_with_prep(
        &self,
        ctx: &TranslationContext,
        dat_mutation: Mutation
    ) -> TranslationSet<Irish> {
        self.generate_common(ctx, dat_mutation, false, false)
    }

    pub fn generate(&self, ctx: &TranslationContext) -> TranslationSet<Irish> {
        self.generate_common(ctx, Mutation::None, true, false)
    }
}
