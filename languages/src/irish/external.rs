use crate::{ParseError, Word};
use crate::{
    Audio,
    AudioJson,
    ConstituentJson,
    ConstituentJsonSpecific,
    DictWord,
    LanguageCode,
    LanguageData
};
use maplit::hashset;
use serde::Deserialize;
use serde_with::{serde_as, TryFromInto, OneOrMany};
use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use super::categories::{Conjugation, Case, Definiteness, Gender, Number, Person, Tense};
use super::{Dialect, DialectSet, Irish, Mutation};
use super::{adjective, copp, noun, np, particle, pp, preposition, pronoun, verb, verbalparticle, vp};

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum VerbForm {
    Short(String),
    #[serde(rename_all="kebab-case")]
    Full {
        analytic: Option<bool>,
        no_analytic_ending: Option<bool>,
        use_tense_marker: Option<bool>,
        dialects: Option<HashSet<Dialect>>,
        preferred_in_dialects: Option<HashSet<Dialect>>,
        dep_preferred_in_dialects: Option<HashSet<Dialect>>,
        past_particles: Option<bool>,
        resists_lenition: Option<bool>,
        contains_do: Option<bool>,
        with_ending: Option<bool>,
        dependent: Option<bool>,
        #[serde(rename="form")]
        text: String,
        #[serde(rename="form-t")]
        text_with_t_ending: Option<String>,
        #[serde(rename="form-ní")]
        text_with_ni: Option<String>,
        except: Option<Vec<String>>,
        conjugation: Option<u8>,
    },
}

impl TryFrom<VerbForm> for verb::IrishVerbForm {
    type Error = ParseError;

    fn try_from(form: VerbForm) -> Result<verb::IrishVerbForm, ParseError> {
        let res = match form {
            VerbForm::Short(text) => verb::IrishVerbForm::new(
                false,
                false,
                true,
                true,
                false,
                false,
                None,
                text,
                None,
                None,
                DialectSet::all(),
                DialectSet::empty(),
                None,
                None,
                Vec::new(),
                None,
            ),
            VerbForm::Full { analytic, no_analytic_ending, use_tense_marker, dialects, preferred_in_dialects, dep_preferred_in_dialects, past_particles, resists_lenition, contains_do, with_ending, dependent, text, text_with_t_ending, text_with_ni, except, conjugation } => {
                let conjugation = match conjugation {
                    None => None,
                    Some(1) => Some(Conjugation::First),
                    Some(2) => Some(Conjugation::Second),
                    Some(x) => return Err(ParseError::InvalidValue("conjugation".to_string(), x.to_string()))
                };
                let preferred_in_dialects = if let Some(preferred_in_dialects) = preferred_in_dialects {
                    DialectSet::from(preferred_in_dialects)
                } else {
                    DialectSet::empty()
                };
                let dep_preferred_in_dialects = if let Some(dep_preferred_in_dialects) = dep_preferred_in_dialects {
                    Some(DialectSet::from(dep_preferred_in_dialects))
                } else {
                    None
                };

                verb::IrishVerbForm::new(
                    analytic.unwrap_or(false),
                    no_analytic_ending.unwrap_or(false),
                    use_tense_marker.unwrap_or(true),
                    past_particles.unwrap_or(true),
                    resists_lenition.unwrap_or(false),
                    contains_do.unwrap_or(false),
                    dependent,
                    text,
                    text_with_t_ending,
                    text_with_ni,
                    DialectSet::from(dialects.unwrap_or_else(|| hashset!{})),
                    preferred_in_dialects,
                    dep_preferred_in_dialects,
                    with_ending,
                    except.unwrap_or_else(|| Vec::new()),
                    conjugation,
                )
            },
        };
        Ok(res)
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct Verb {
    id: Option<String>,
    conjugation: Option<u8>,
    #[serde(default)]
    #[serde_as(as="OneOrMany<TryFromInto<AudioJson<Irish>>>")]
    audio: Vec<Audio<Irish>>,
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<TryFromInto<VerbForm>>>")]
    forms: HashMap<String, Vec<verb::IrishVerbForm>>,
}

impl TryFrom<Verb> for Arc<verb::IrishVerb> {
    type Error = ParseError;

    fn try_from(verb: Verb) -> Result<Arc<verb::IrishVerb>, ParseError> {
        let conjugation = match verb.conjugation {
            None => None,
            Some(1) => Some(Conjugation::First),
            Some(2) => Some(Conjugation::Second),
            Some(x) => return Err(ParseError::InvalidValue("conjugation".to_string(), x.to_string()))
        };
        Ok(Arc::new(verb::IrishVerb::new(verb.id, conjugation, verb.forms)?))
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum VerbalParticleForm {
    Short(String),
    #[serde(rename_all="kebab-case")]
    Full {
        dialects: Option<HashSet<Dialect>>,
        #[serde(rename="form")]
        text: String,
        #[serde(rename="form-before-vowel")]
        text_before_vowel: Option<String>,
        mutation: Option<Mutation>,
    },
}

impl From<VerbalParticleForm> for verbalparticle::VerbalParticleForm {
    fn from(form: VerbalParticleForm) -> verbalparticle::VerbalParticleForm {
        match form {
            VerbalParticleForm::Short(text) => verbalparticle::VerbalParticleForm {
                text,
                text_before_vowel: None,
                dialects: DialectSet::all(),
                mutation: Mutation::None,
            },
            VerbalParticleForm::Full { dialects, text, text_before_vowel, mutation } => verbalparticle::VerbalParticleForm {
                text,
                text_before_vowel,
                dialects: DialectSet::from(dialects.unwrap_or_else(|| hashset!{})),
                mutation: mutation.unwrap_or(Mutation::None),
            }
        }
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct VerbalParticle {
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<TryFromInto<VerbalParticleForm>>>")]
    forms: HashMap<String, Vec<verbalparticle::VerbalParticleForm>>,
    dependent_form: Option<bool>,
}

impl TryFrom<VerbalParticle> for Arc<verbalparticle::VerbalParticle> {
    type Error = ParseError;

    fn try_from(
        particle: VerbalParticle
    ) -> Result<Arc<verbalparticle::VerbalParticle>, ParseError> {
        Ok(Arc::new(verbalparticle::VerbalParticle::new(
            particle.forms,
            particle.dependent_form.unwrap_or(true),
        )?))
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct Particle {
    word: String,
    #[serde(default)]
    #[serde_as(as="OneOrMany<TryFromInto<AudioJson<Irish>>>")]
    audio: Vec<Audio<Irish>>,
}

impl TryFrom<Particle> for Arc<particle::Particle> {
    type Error = ParseError;

    fn try_from(
        particle: Particle
    ) -> Result<Arc<particle::Particle>, ParseError> {
        Ok(Arc::new(particle::Particle::new(particle.word)?))
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum NounForm {
    Short(String),
    #[serde(rename_all="kebab-case")]
    Full {
        dialects: Option<HashSet<Dialect>>,
        #[serde(rename="form")]
        text: String,
        gender: Option<Gender>,
    },
}

impl From<NounForm> for noun::NounForm {
    fn from(form: NounForm) -> noun::NounForm {
        match form {
            NounForm::Short(text) => {
                noun::NounForm::new(
                    text,
                    None,
                    DialectSet::all(),
                )
            },
            NounForm::Full { dialects, text, gender } => {
                noun::NounForm::new(
                    text,
                    gender,
                    DialectSet::from(dialects.unwrap_or_else(|| hashset!{})),
                )
            },
        }
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct Noun {
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<TryFromInto<NounForm>>>")]
    forms: HashMap<String, Vec<noun::NounForm>>,
    dialects: Option<HashSet<Dialect>>,
    #[serde(default)]
    #[serde_as(as="OneOrMany<TryFromInto<AudioJson<Irish>>>")]
    audio: Vec<Audio<Irish>>,
}

impl TryFrom<Noun> for Arc<noun::Noun> {
    type Error = ParseError;

    fn try_from(noun: Noun) -> Result<Arc<noun::Noun>, ParseError> {
        let dialects = if let Some(dialects) = noun.dialects {
            DialectSet::from(dialects)
        } else {
            DialectSet::all()
        };
        Ok(Arc::new(noun::Noun::new(noun.forms, dialects)?))
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum PronounForm {
    Short(String),
    #[serde(rename_all="kebab-case")]
    Full {
        dialects: Option<HashSet<Dialect>>,
        #[serde(rename="form")]
        text: String,
    },
}

impl From<PronounForm> for pronoun::PronounForm {
    fn from(form: PronounForm) -> pronoun::PronounForm {
        match form {
            PronounForm::Short(text) => pronoun::PronounForm {
                text,
                dialects: DialectSet::all(),
            },
            PronounForm::Full { dialects, text } => pronoun::PronounForm {
                text,
                dialects: DialectSet::from(dialects.unwrap_or_else(|| hashset!{})),
            }
        }
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct Pronoun {
    person: Person,
    number: Number,
    gender: Option<Gender>,
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<TryFromInto<PronounForm>>>")]
    forms: HashMap<String, Vec<pronoun::PronounForm>>,
}

impl TryFrom<Pronoun> for Arc<pronoun::Pronoun> {
    type Error = ParseError;

    fn try_from(pronoun: Pronoun) -> Result<Arc<pronoun::Pronoun>, ParseError> {
        Ok(Arc::new(pronoun::Pronoun::new(
            pronoun.person,
            pronoun.number,
            pronoun.gender,
            pronoun.forms,
        )?))
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum PrepositionForm {
    Short(String),
    #[serde(rename_all="kebab-case")]
    Full {
        dialects: Option<HashSet<Dialect>>,
        analytic: Option<bool>,
        #[serde(rename="form")]
        text: String,
    },
}

impl From<PrepositionForm> for preposition::PrepositionForm {
    fn from(form: PrepositionForm) -> preposition::PrepositionForm {
        match form {
            PrepositionForm::Short(text) => preposition::PrepositionForm {
                text,
                analytic: true,
                dialects: DialectSet::all(),
                mutation: Mutation::None,
            },
            PrepositionForm::Full { dialects, analytic, text } => preposition::PrepositionForm {
                text,
                analytic: analytic.unwrap_or(true),
                dialects: DialectSet::from(dialects.unwrap_or_else(|| hashset!{})),
                mutation: Mutation::None,
            }
        }
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct Preposition {
    #[serde(default)]
    #[serde_as(as="OneOrMany<TryFromInto<AudioJson<Irish>>>")]
    audio: Vec<Audio<Irish>>,
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<TryFromInto<PrepositionForm>>>")]
    forms: HashMap<String, Vec<preposition::PrepositionForm>>,
}

impl TryFrom<Preposition> for Arc<preposition::Preposition> {
    type Error = ParseError;

    fn try_from(preposition: Preposition) -> Result<Arc<preposition::Preposition>, ParseError> {
        Ok(Arc::new(preposition::Preposition::new(preposition.forms)?))
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum AdjectiveForm {
    Short(String),
    #[serde(rename_all="kebab-case")]
    Full {
        dialects: Option<HashSet<Dialect>>,
        #[serde(rename="form")]
        text: String,
    },
}

impl From<AdjectiveForm> for adjective::AdjectiveForm {
    fn from(form: AdjectiveForm) -> adjective::AdjectiveForm {
        match form {
            AdjectiveForm::Short(text) => adjective::AdjectiveForm::new(
                text,
                DialectSet::all(),
            ),
            AdjectiveForm::Full { dialects, text } => adjective::AdjectiveForm::new(
                text,
                DialectSet::from(dialects.unwrap_or_else(|| hashset!{})),
            )
        }
    }
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all="kebab-case")]
pub struct Adjective {
    #[serde(default)]
    #[serde_as(as="OneOrMany<TryFromInto<AudioJson<Irish>>>")]
    audio: Vec<Audio<Irish>>,
    #[serde(flatten)]
    #[serde_as(as="HashMap<_, OneOrMany<TryFromInto<AdjectiveForm>>>")]
    forms: HashMap<String, Vec<adjective::AdjectiveForm>>,
    dialects: Option<HashSet<Dialect>>,
}

impl TryFrom<Adjective> for Arc<adjective::Adjective> {
    type Error = ParseError;

    fn try_from(adjective: Adjective) -> Result<Arc<adjective::Adjective>, ParseError> {
        let dialects = if let Some(dialects) = adjective.dialects {
            DialectSet::from(dialects)
        } else {
            DialectSet::all()
        };
        Ok(Arc::new(adjective::Adjective::new(adjective.forms, dialects)?))
    }
}


#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(tag="type")]
pub enum IrishWord {
    IrishAdjective(#[serde_as(as="TryFromInto<Adjective>")] Arc<adjective::Adjective>),
    IrishNoun(#[serde_as(as="TryFromInto<Noun>")] Arc<noun::Noun>),
    IrishParticle(#[serde_as(as="TryFromInto<Particle>")] Arc<particle::Particle>),
    IrishPronoun(#[serde_as(as="TryFromInto<Pronoun>")] Arc<pronoun::Pronoun>),
    IrishPrep(#[serde_as(as="TryFromInto<Preposition>")] Arc<preposition::Preposition>),
    IrishVerb(#[serde_as(as="TryFromInto<Verb>")] Arc<verb::IrishVerb>),
    IrishVerbalParticle(#[serde_as(as="TryFromInto<VerbalParticle>")] Arc<verbalparticle::VerbalParticle>),
}

impl Word for IrishWord {
    // FIXME Use #[enum_dispatch(Word)] as soon as all variants have distinct types
    fn lemma(&self) -> Vec<String> {
        match self {
            IrishWord::IrishAdjective(adjective) => adjective.lemma(),
            IrishWord::IrishNoun(noun) => noun.lemma(),
            IrishWord::IrishPrep(prep) => prep.lemma(),
            IrishWord::IrishPronoun(pronoun) => pronoun.lemma(),
            IrishWord::IrishVerb(verb) => verb.lemma(),
            IrishWord::IrishVerbalParticle(particle) => particle.lemma(),
            IrishWord::IrishParticle(particle) => particle.lemma(),
        }
    }
    fn id(&self) -> String {
        match self {
            IrishWord::IrishAdjective(adjective) => adjective.id(),
            IrishWord::IrishNoun(noun) => noun.id(),
            IrishWord::IrishPrep(prep) => prep.id(),
            IrishWord::IrishPronoun(pronoun) => pronoun.id(),
            IrishWord::IrishVerb(verb) => verb.id(),
            IrishWord::IrishVerbalParticle(particle) => particle.id(),
            IrishWord::IrishParticle(particle) => particle.id(),
        }
    }
    fn language(&self) -> LanguageCode {
        LanguageCode::Ga
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields, rename_all="kebab-case")]
pub enum NP {
    Noun {
        noun: String,
        number: Option<Number>,
        case: Option<Case>,
        dialects: Option<HashSet<Dialect>>,
        definite: Option<Definiteness>,
        #[serde(default)]
        attributes: Vec<ConstituentJson>,
    },
    NP {
        np: Box<ConstituentJson>,
        #[serde(default)]
        attributes: Vec<ConstituentJson>,
        definite: Option<Definiteness>,
        dialects: Option<HashSet<Dialect>>,
    },
    Pronoun {
        pronoun: String,
        #[serde(default)]
        emphatic: bool,
        dialects: Option<HashSet<Dialect>>,
    },
}

impl NP {
    pub fn into_full(
        json: &ConstituentJson,
        l: &LanguageData,
        c: Case,
        def: Option<Definiteness>,
    ) -> Result<np::NP, ParseError> {
        let json = match &json.specific {
            ConstituentJsonSpecific::IrishNP(x) => x,
            ConstituentJsonSpecific::IrishVP(_) => {
                let vp = VP::into(json, l)?;
                match vp.get_vptype() {
                    vp::VPType::VerbalNounA | vp::VPType::VerbalNounGen => (),
                    _ => return Err(ParseError::InvalidValue("vptype".to_string(), "VN_*".to_string())),
                };
                return Ok(np::NP::from_vp(vp));
            }
            _ => return Err(ParseError::InvalidValue("type".to_string(), "IrishNP".to_string())),
        };

        let np_dialects;
        let mut np = match json {
            NP::Noun { noun, number, case, dialects, definite, attributes, .. } => {
                np_dialects = dialects.clone();
                let mut np_attributes = Vec::new();
                for a in attributes {
                    np_attributes.push(AP::into(a, l)?);
                }
                match l.get_word(noun)?.as_ref() {
                    DictWord::Ga(IrishWord::IrishNoun(noun)) => np::NP::from_noun(
                        noun.clone(),
                        case.unwrap_or(c),
                        number.unwrap_or(Number::Singular),
                        def.unwrap_or(definite.unwrap_or(Definiteness::Indefinite)),
                        np_attributes,
                    ),
                    _ => return Err(ParseError::ExpectedWordType("noun".to_string())),
                }
            }
            NP::NP { np, dialects, definite, attributes } => {
                np_dialects = dialects.clone();
                let mut np_attributes = Vec::new();
                for a in attributes {
                    np_attributes.push(AP::into(a, l)?);
                }
                np::NP::from_np(Arc::new(NP::into_full(np, l, c, *definite)?), np_attributes)
            }
            NP::Pronoun { pronoun, dialects, emphatic } => {
                np_dialects = dialects.clone();
                match l.get_word(pronoun)?.as_ref() {
                    DictWord::Ga(IrishWord::IrishPronoun(pronoun)) => {
                        np::NP::from_pronoun(pronoun.clone(), *emphatic)
                    },
                    _ => return Err(ParseError::ExpectedWordType("pronoun".to_string())),
                }
            }
        };

        if let Some(dialects) = np_dialects {
            np.set_dialects(DialectSet::from(dialects));
        }

        Ok(np)
    }

    pub fn into(
        json: &ConstituentJson,
        l: &LanguageData,
        c: Case
    ) -> Result<np::NP, ParseError> {
        Self::into_full(json, l, c, None)
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct PP {
    preposition: String,
    object: Arc<ConstituentJson>,
}

impl PP {
    pub fn into(json: &ConstituentJson, l: &LanguageData) -> Result<pp::PP, ParseError> {
        let json = match &json.specific {
            ConstituentJsonSpecific::IrishPP(x) => x,
            _ => return Err(ParseError::InvalidValue("type".to_string(), "IrishPP".to_string())),
        };

        let preposition = match l.get_word(&json.preposition)?.as_ref() {
            DictWord::Ga(IrishWord::IrishPrep(prep)) => prep.clone(),
            _ => return Err(ParseError::ExpectedWordType("preposition".to_string())),
        };

        // TODO Get the case from the preposition
        let pp = pp::PP::new(preposition, NP::into(&json.object, &l, Case::Dative)?);

        Ok(pp)
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct AP {
    adjective: String,
}

impl AP {
    pub fn into(json: &ConstituentJson, l: &LanguageData) -> Result<adjective::AP, ParseError> {
        let json = match &json.specific {
            ConstituentJsonSpecific::IrishAP(x) => x,
            _ => return Err(ParseError::InvalidValue("type".to_string(), "IrishAP".to_string())),
        };

        let adjective = match l.get_word(&json.adjective)?.as_ref() {
            DictWord::Ga(IrishWord::IrishAdjective(adjective)) => adjective.clone(),
            _ => return Err(ParseError::ExpectedWordType("adjective".to_string())),
        };

        Ok(adjective::AP::new(adjective))
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct VP {
    vptype: Option<vp::VPType>,
    verb: String,
    subject: Option<Arc<ConstituentJson>>,
    object: Option<Arc<ConstituentJson>>,
    comp: Option<Arc<ConstituentJson>>,
    #[serde(default)]
    adverbial: Vec<Arc<ConstituentJson>>,
    #[serde(default)]
    neg: bool,
}

impl VP {
    pub fn into(json_generic: &ConstituentJson, l: &LanguageData) -> Result<vp::VP, ParseError> {
        let json = match &json_generic.specific {
            ConstituentJsonSpecific::IrishVP(x) => x,
            _ => return Err(ParseError::InvalidValue("type".to_string(), "IrishVP".to_string())),
        };

        let verb = match l.get_word(&json.verb)?.as_ref() {
            DictWord::Ga(IrishWord::IrishVerb(verb)) => verb.clone(),
            _ => return Err(ParseError::ExpectedWordType("verb".to_string())),
        };

        let mut vp = vp::VP::new(verb, Tense::Present, json.neg);

        if let Some(vptype) = json.vptype {
            vp = vp.vptype(vptype);
        }

        if let Some(subject) = &json.subject {
            vp = vp.subject(NP::into(&subject, &l, Case::Nominative)?);
        }

        if let Some(object) = &json.object {
            vp = vp.object(NP::into(&object, &l, Case::Nominative)?);
        }

        if let Some(comp) = &json.comp{
            vp = vp.comp(VP::into(&comp, &l)?);
        }

        for adverbial in &json.adverbial {
            vp = vp.adverbial(PP::into(&adverbial, &l)?);
        }

        if !json_generic.post.is_empty() {
            vp = vp.post(json_generic.post.clone());
        }

        Ok(vp)
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct CopP {
    subject: Arc<ConstituentJson>,
    predicate: Option<Arc<ConstituentJson>>,
    #[serde(default)]
    neg: bool,
}

impl CopP {
    pub fn into(json: &ConstituentJson, l: &LanguageData) -> Result<copp::CopP, ParseError> {
        let json = match &json.specific {
            ConstituentJsonSpecific::IrishCopP(x) => x,
            _ => return Err(ParseError::InvalidValue("type".to_string(), "IrishCopP".to_string())),
        };


        let mut copp = copp::CopP::new(
            Tense::Present,
            NP::into(&json.subject, &l, Case::Nominative)?,
            json.neg,
        );

        if let Some(predicate) = &json.predicate {
            match predicate.specific {
                ConstituentJsonSpecific::IrishNP(_) => {
                    copp = copp.predicate_np(NP::into(&predicate, &l, Case::Nominative)?);
                }
                ConstituentJsonSpecific::IrishAP(_) => {
                    copp = copp.predicate_ap(AP::into(&predicate, &l)?);
                }
                _ => return Err(ParseError::InvalidValue("predicate".to_string(), "IrishNP or IrishAP".to_string())),
            }
        }

        Ok(copp)
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct Par {
    particle: String,
}

impl Par {
    pub fn into(json: &ConstituentJson, l: &LanguageData) -> Result<particle::Par, ParseError> {
        let json = match &json.specific {
            ConstituentJsonSpecific::IrishPar(x) => x,
            _ => return Err(ParseError::InvalidValue("type".to_string(), "IrishPar".to_string())),
        };

        let particle = match l.get_word(&json.particle)?.as_ref() {
            DictWord::Ga(IrishWord::IrishParticle(particle)) => particle.clone(),
            _ => return Err(ParseError::ExpectedWordType("particle".to_string())),
        };

        Ok(particle::Par::new(particle))
    }
}
