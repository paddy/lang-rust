use crate::TranslationContext;
use crate::translation::{Translation, TranslationGenerator, TranslationSet};
use std::sync::Arc;
use super::{DialectSet, Irish, Mutation};
use super::categories::{AgreementInfo, Gender, Person, Tense};
use super::adjective::AP;
use super::np::NP;

enum Predicate {
    None,
    NP(NP),
    AP(AP),
}

pub struct CopP {
    tense: Tense,
    subject: NP,
    predicate: Predicate,
    neg: bool,
}

impl CopP {
    pub fn new(tense: Tense, subject: NP, neg: bool) -> Self {
        Self {
            tense,
            subject,
            predicate: Predicate::None,
            neg,
        }
    }

    pub fn predicate_np(mut self, predicate: NP) -> Self {
        self.predicate = Predicate::NP(predicate);
        self
    }

    pub fn predicate_ap(mut self, predicate: AP) -> Self {
        self.predicate = Predicate::AP(predicate);
        self
    }

    pub fn generate(&self, ctx: &TranslationContext) -> TranslationSet<Irish> {
        let subject = self.subject.generate_copula(ctx, true);
        let predicate;
        let mut subpredicate;
        let mut fronted_subject = false;
        let tag;

        match &self.predicate {
            Predicate::NP(p) => {
                predicate = Some(p.generate_copula(ctx, false));
                subpredicate = if p.is_definite() {
                    let agreement = self.subject.agreement();
                    // FIXME Only for third person pronoun subjects, the subpredicate agrees with the
                    // subject. Otherwise (in particular, with nouns), it should agree with the
                    // predicate.
                    // FIXME Get the actual pronoun from the language data
                    // TODO siad
                    if agreement.person != Person::Third {
                        fronted_subject = true;
                        None
                    } else {
                        let form = match agreement.gender {
                            Gender::Masc => "é",
                            Gender::Fem => "í",
                        };

                        Some(Arc::new(Translation::from_string_unreferenced(form, agreement)))
                    }
                } else {
                    None
                };
                tag = Some(if p.is_definite() { "cop-identification" } else { "cop-classification" });
            }
            Predicate::AP(p) => {
                predicate = Some(p.generate());
                subpredicate = None;
                tag = Some("cop-adj");
            }
            Predicate::None => {
                predicate = None;
                subpredicate = None;
                tag = None;
            }
        }

        let subsubject = if subpredicate.is_none() && self.subject.is_definite() {
            // FIXME Get the actual pronoun from the language data
            // TODO siad
            let agreement = self.subject.agreement();
            let form = match agreement.gender {
                Gender::Masc => "é",
                Gender::Fem => "í",
            };

            Some(Arc::new(Translation::from_string_unreferenced(form, agreement)))
        } else {
            None
        };

        if self.neg && subpredicate.is_some() {
            subpredicate.as_mut().map(|s| Mutation::HPrefix.update_translation(Arc::make_mut(s)));
        }

        let punctuation = Arc::new(Translation::from_string_contraction(
            ".",
            None,
            true,
            false,
            AgreementInfo::default(),
            &DialectSet::all(),
            |_| unreachable!(),
        ));

        let mut res = TranslationSet::new();
        for s in subject {
            let mut parts: Vec<&dyn TranslationGenerator<Irish>> = Vec::new();
            let cop_form_str = if self.neg { "ní" } else { "is" };
            let cop_form = Arc::new(
                Translation::from_string_unreferenced(cop_form_str, s.agreement)
            );

            parts.push(&cop_form);
            if fronted_subject {
                parts.push(&s);
            }
            if let Some(sp) = &subpredicate {
                parts.push(sp);
            };
            if let Some(p) = &predicate {
                parts.push(p);
            };
            if let Some(ss) = &subsubject {
                parts.push(ss);
            };
            if !fronted_subject {
                parts.push(&s);
            }
            parts.push(&punctuation);

            res.append(Translation::combine_full(&parts, s.agreement, true));
        }

        if let Some(tag) = tag {
            res = res.map(|t| t.add_expression_tag(tag.to_owned()));
        }

        res
    }
}
