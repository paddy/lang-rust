use serde::Deserialize;
use serde_with::{serde_as, TryFromInto};

use super::LanguageCode;

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="UPPERCASE")]
pub enum SkillType {
    Words,
    Grammar,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum WordJson {
    Short(String),
    #[serde(rename_all="kebab-case")]
    Full(Word),
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct Word {
    pub word: String,
    #[serde(default)]
    pub dialects: Vec<String>,
}

impl From<WordJson> for Word {
    fn from(word: WordJson) -> Self {
        match word {
            WordJson::Short(w) => Self {
                word: w,
                dialects: Vec::new(),
            },
            WordJson::Full(w) => w,
        }
    }
}

fn default_priority() -> u32 {
    50
}

#[serde_as]
#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct Skill {
    pub r#type: SkillType,
    pub name: String,
    pub section: String,
    pub description: Option<String>,
    #[serde(default)]
    #[serde_as(as="Vec<TryFromInto<WordJson>>")]
    pub words: Vec<Word>,
    #[serde(default)]
    pub tags: Vec<String>,
    #[serde(default="default_priority")]
    pub priority: u32,
    pub lesson: Option<String>,
    pub emoji: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct Course {
    pub name: String,
    pub source_lang: LanguageCode,
    pub target_lang: LanguageCode,
    #[serde(default)]
    pub source_dialects: Vec<String>,
    #[serde(default)]
    pub target_dialects: Vec<String>,
    pub lessons: Vec<String>,
    pub skills: Vec<Skill>,
}
